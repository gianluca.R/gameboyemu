﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Instructions;
using System;

namespace GameBoyEmuTests.Emu.Utils
{
    class AccessorMocks
    {
        /// <summary>
        /// Generate a mocked ReadAccessor that will return an AccessorToken with the provided value, accessor and address
        /// </summary>
        public static InstructionAccessors.ReadAccessor<T> GenerateReadAccessor<T>(T readValue, Accessor? readAccessor, UInt16? readAddress)
        {
            return (ram, registers, parameters) =>
            {
                return new AccessorToken<T>(readValue, readAddress, readAccessor);
            };
        }
        public static InstructionAccessors.ReadAccessor<T> GenerateReadAccessor<T>(T readValue)
        {
            return GenerateReadAccessor<T>(readValue, null, null);
        }

        /// <summary>
        /// Generate a mocked WriteAccessor that will call the provided action with the output value.
        /// </summary>
        public static InstructionAccessors.WriteAccessor<T> GenerateWriteAccessor<T>(Action<AccessorToken<T>> outputSet, Accessor? writeAccessor, UInt16? writeAddress)
        {
            return (ram, registers, parameters, accessorToken) =>
            {
                outputSet(accessorToken);

                // I set these but they don't really matter most of the times
                accessorToken.writeAddress = writeAddress;
                accessorToken.writeAccessor = writeAccessor;
            };
        }
        public static InstructionAccessors.WriteAccessor<T> GenerateWriteAccessor<T>(Action<AccessorToken<T>> outputSet)
        {
            return GenerateWriteAccessor<T>(outputSet, null, null);
        }

        public static InstructionAccessors.FlagAccessor GenerateFlagAccessor(bool flagValue, Accessor? readAccessor, UInt16? readAddress)
        {
            return (ram, registers, parameters) =>
            {
                return flagValue;
            };
        }
        public static InstructionAccessors.FlagAccessor GenerateFlagAccessor(bool flagValue)
        {
            return GenerateFlagAccessor(flagValue, null, null);
        }
    }
}
