﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Instructions;
using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;

namespace GameBoyEmuTests.Emu.Utils
{
    class AssertUtils
    {
        public static void AssertFlags(Registers registers, bool flagZ, bool flagN, bool flagH, bool flagC)
        {
            Assert.AreEqual(flagZ, registers.flagZ);
            Assert.AreEqual(flagN, registers.flagN);
            Assert.AreEqual(flagH, registers.flagH);
            Assert.AreEqual(flagC, registers.flagC);
        }

        public static void AssertMethodsAreEqual(MethodInfo method1, MethodInfo method2)
        {
            Assert.AreEqual(method1, method2);

            var body1 = method1.GetMethodBody();
            var body2 = method2.GetMethodBody();

            bool bodiesAreEqual = true;
            var bb1 = body1.GetILAsByteArray();
            var bb2 = body2.GetILAsByteArray();
            if (bb1.Length != bb2.Length) { bodiesAreEqual = false; }
            else
            {
                for (int k = 0; k < bb1.Length; k++)
                {
                    if (bb1[k] != bb2[k])
                    {
                        bodiesAreEqual = false;
                        break;
                    }
                }
            }
            Assert.IsTrue(bodiesAreEqual);
        }

        public static void ExecuteInstructionAndAssertBehaviour(
            RAM ram,
            Registers registers,
            InstructionDescriptor instruction,
            InstructionDescriptor.Action expectedAction,
            string expectedName,
            int expectedClock)
        {
            var method = instruction.action.Method;
            var method2 = expectedAction.Method;

            AssertMethodsAreEqual(method, method2);
            Assert.AreEqual(expectedName, instruction.name);
            Assert.AreEqual(expectedClock, instruction.clock);
            Assert.AreEqual(expectedClock, instruction.clock);

            InstructionOutput instructionOutput = instruction.action(ram, registers, ExecutionParameters.DEFAULT);
            InstructionOutput expectedOutput = expectedAction(ram, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(expectedOutput.readAccessor, instructionOutput.readAccessor); 
            Assert.AreEqual(expectedOutput.writeAccessor, instructionOutput.writeAccessor);
        }
    }
}
