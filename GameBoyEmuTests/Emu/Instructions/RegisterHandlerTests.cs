﻿using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;
using GameBoyEmuTests.Emu.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameBoyEmu.Emu.Instructions.Tests
{
    [TestClass()]
    public class RegisterHandlerTests
    {
        Registers registers;

        [TestInitialize()]
        public void Setup()
        {
            registers = new Registers();

            registers.PC.Full = 0x100;
            registers.AF.Full = 0x1200;
            registers.BC.Full = 0x1212;
            registers.DE.Full = 0x1212;
            registers.HL.Full = 0x1212;
            registers.SP.Full = 0x1212;
        }

        #region SetFlags

        [TestMethod()]
        public void SetFlagsReadOnlyModeTest()
        {
            RegisterHandler.SetFlags(registers, ExecutionParameters.DEBUGGER, true, true, true, true);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void SetFlagsWithOnlyZTest()
        {
            RegisterHandler.SetFlags(registers, ExecutionParameters.DEFAULT, true, null, null, null);
            AssertUtils.AssertFlags(registers, true, false, false, false);
        }

        [TestMethod()]
        public void SetFlagsWithOnlyNTest()
        {
            RegisterHandler.SetFlags(registers, ExecutionParameters.DEFAULT, null, true, null, null);
            AssertUtils.AssertFlags(registers, false, true, false, false);
        }

        [TestMethod()]
        public void SetFlagsWithOnlyHTest()
        {
            RegisterHandler.SetFlags(registers, ExecutionParameters.DEFAULT, null, null, true, null);
            AssertUtils.AssertFlags(registers, false, false, true, false);
        }

        [TestMethod()]
        public void SetFlagsWithOnlyCTest()
        {
            RegisterHandler.SetFlags(registers, ExecutionParameters.DEFAULT, null, null, null, true);
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        [TestMethod()]
        public void SetFlagsWithAllArgumentsTest()
        {
            RegisterHandler.SetFlags(registers, ExecutionParameters.DEFAULT, true, true, true, true);
            AssertUtils.AssertFlags(registers, true, true, true, true);
        }

        #endregion

        #region Set single flags

        [TestMethod()]
        public void SetFlagZReadOnlyTest()
        {
            RegisterHandler.SetFlagZ(registers, ExecutionParameters.DEBUGGER, true);
            Assert.AreEqual(false, registers.flagZ);
        }

        [TestMethod()]
        public void SetFlagZTest()
        {
            RegisterHandler.SetFlagZ(registers, ExecutionParameters.DEFAULT, true);
            Assert.AreEqual(true, registers.flagZ);
        }

        [TestMethod()]
        public void SetFlagNReadOnlyTest()
        {
            RegisterHandler.SetFlagN(registers, ExecutionParameters.DEBUGGER, true);
            Assert.AreEqual(false, registers.flagN);
        }

        [TestMethod()]
        public void SetFlagNTest()
        {
            RegisterHandler.SetFlagN(registers, ExecutionParameters.DEFAULT, true);
            Assert.AreEqual(true, registers.flagN);
        }

        [TestMethod()]
        public void SetFlagHReadOnlyTest()
        {
            RegisterHandler.SetFlagH(registers, ExecutionParameters.DEBUGGER, true);
            Assert.AreEqual(false, registers.flagH);
        }

        [TestMethod()]
        public void SetFlagHTest()
        {
            RegisterHandler.SetFlagH(registers, ExecutionParameters.DEFAULT, true);
            Assert.AreEqual(true, registers.flagH);
        }

        [TestMethod()]
        public void SetFlagCReadOnlyTest()
        {
            RegisterHandler.SetFlagC(registers, ExecutionParameters.DEBUGGER, true);
            Assert.AreEqual(false, registers.flagC);
        }

        [TestMethod()]
        public void SetFlagCTest()
        {
            RegisterHandler.SetFlagC(registers, ExecutionParameters.DEFAULT, true);
            Assert.AreEqual(true, registers.flagC);
        }

        #endregion

        #region Byte registers

        [TestMethod()]
        public void SetAReadOnlyTest()
        {
            RegisterHandler.SetA(registers, ExecutionParameters.DEBUGGER, 0x81);
            Assert.AreEqual(0x12, registers.AF.Left);
        }

        [TestMethod()]
        public void SetATest()
        {
            RegisterHandler.SetA(registers, ExecutionParameters.DEFAULT, 0x81);
            Assert.AreEqual(0x81, registers.AF.Left);
        }

        [TestMethod()]
        public void SetFReadOnlyTest()
        {
            RegisterHandler.SetF(registers, ExecutionParameters.DEBUGGER, 0x81);
            Assert.AreEqual(0x0, registers.AF.Right);
        }

        [TestMethod()]
        public void SetFTest()
        {
            RegisterHandler.SetF(registers, ExecutionParameters.DEFAULT, 0x81);
            Assert.AreEqual(0x81, registers.AF.Right);
        }

        [TestMethod()]
        public void SetBReadOnlyTest()
        {
            RegisterHandler.SetB(registers, ExecutionParameters.DEBUGGER, 0x81);
            Assert.AreEqual(0x12, registers.BC.Left);
        }

        [TestMethod()]
        public void SetBTest()
        {
            RegisterHandler.SetB(registers, ExecutionParameters.DEFAULT, 0x81);
            Assert.AreEqual(0x81, registers.BC.Left);
        }

        [TestMethod()]
        public void SetCReadOnlyTest()
        {
            RegisterHandler.SetC(registers, ExecutionParameters.DEBUGGER, 0x81);
            Assert.AreEqual(0x12, registers.BC.Right);
        }

        [TestMethod()]
        public void SetCTest()
        {
            RegisterHandler.SetC(registers, ExecutionParameters.DEFAULT, 0x81);
            Assert.AreEqual(0x81, registers.BC.Right);
        }

        [TestMethod()]
        public void SetDReadOnlyTest()
        {
            RegisterHandler.SetD(registers, ExecutionParameters.DEBUGGER, 0x81);
            Assert.AreEqual(0x12, registers.DE.Left);
        }

        [TestMethod()]
        public void SetDTest()
        {
            RegisterHandler.SetD(registers, ExecutionParameters.DEFAULT, 0x81);
            Assert.AreEqual(0x81, registers.DE.Left);
        }

        [TestMethod()]
        public void SetEReadOnlyTest()
        {
            RegisterHandler.SetE(registers, ExecutionParameters.DEBUGGER, 0x81);
            Assert.AreEqual(0x12, registers.DE.Right);
        }

        [TestMethod()]
        public void SetETest()
        {
            RegisterHandler.SetE(registers, ExecutionParameters.DEFAULT, 0x81);
            Assert.AreEqual(0x81, registers.DE.Right);
        }

        [TestMethod()]
        public void SetHReadOnlyTest()
        {
            RegisterHandler.SetH(registers, ExecutionParameters.DEBUGGER, 0x81);
            Assert.AreEqual(0x12, registers.HL.Left);
        }

        [TestMethod()]
        public void SetHTest()
        {
            RegisterHandler.SetH(registers, ExecutionParameters.DEFAULT, 0x81);
            Assert.AreEqual(0x81, registers.HL.Left);
        }

        [TestMethod()]
        public void SetLReadOnlyTest()
        {
            RegisterHandler.SetL(registers, ExecutionParameters.DEBUGGER, 0x81);
            Assert.AreEqual(0x12, registers.HL.Right);
        }

        [TestMethod()]
        public void SetLTest()
        {
            RegisterHandler.SetL(registers, ExecutionParameters.DEFAULT, 0x81);
            Assert.AreEqual(0x81, registers.HL.Right);
        }

        #endregion

        #region Short registers

        [TestMethod()]
        public void SetAFReadOnlyTest()
        {
            RegisterHandler.SetAF(registers, ExecutionParameters.DEBUGGER, 0x8181);
            Assert.AreEqual(0x1200, registers.AF.Full);
        }

        [TestMethod()]
        public void SetAFTest()
        {
            RegisterHandler.SetAF(registers, ExecutionParameters.DEFAULT, 0x8181);
            Assert.AreEqual(0x8181, registers.AF.Full);
        }

        [TestMethod()]
        public void SetBCReadOnlyTest()
        {
            RegisterHandler.SetBC(registers, ExecutionParameters.DEBUGGER, 0x8181);
            Assert.AreEqual(0x1212, registers.BC.Full);
        }

        [TestMethod()]
        public void SetBCTest()
        {
            RegisterHandler.SetBC(registers, ExecutionParameters.DEFAULT, 0x8181);
            Assert.AreEqual(0x8181, registers.BC.Full);
        }

        [TestMethod()]
        public void SetDEReadOnlyTest()
        {
            RegisterHandler.SetDE(registers, ExecutionParameters.DEBUGGER, 0x8181);
            Assert.AreEqual(0x1212, registers.DE.Full);
        }

        [TestMethod()]
        public void SetDETest()
        {
            RegisterHandler.SetDE(registers, ExecutionParameters.DEFAULT, 0x8181);
            Assert.AreEqual(0x8181, registers.DE.Full);
        }

        [TestMethod()]
        public void SetHLReadOnlyTest()
        {
            RegisterHandler.SetHL(registers, ExecutionParameters.DEBUGGER, 0x8181);
            Assert.AreEqual(0x1212, registers.HL.Full);
        }

        [TestMethod()]
        public void SetHLTest()
        {
            RegisterHandler.SetHL(registers, ExecutionParameters.DEFAULT, 0x8181);
            Assert.AreEqual(0x8181, registers.HL.Full);
        }

        [TestMethod()]
        public void SetPCReadOnlyTest()
        {
            RegisterHandler.SetPC(registers, ExecutionParameters.DEBUGGER, 0x8181);
            Assert.AreEqual(0x0100, registers.PC.Full);
        }

        [TestMethod()]
        public void SetPCTest()
        {
            RegisterHandler.SetPC(registers, ExecutionParameters.DEFAULT, 0x8181);
            Assert.AreEqual(0x8181, registers.PC.Full);
        }

        [TestMethod()]
        public void SetSPReadOnlyTest()
        {
            RegisterHandler.SetSP(registers, ExecutionParameters.DEBUGGER, 0x8181);
            Assert.AreEqual(0x1212, registers.SP.Full);
        }

        [TestMethod()]
        public void SetSPTest()
        {
            RegisterHandler.SetSP(registers, ExecutionParameters.DEFAULT, 0x8181);
            Assert.AreEqual(0x8181, registers.SP.Full);
        }

        #endregion

        #region Inc/Dec

        [TestMethod()]
        public void IncHLReadOnlyTest()
        {
            RegisterHandler.IncHL(registers, ExecutionParameters.DEBUGGER, 1);
            Assert.AreEqual(0x1212, registers.HL.Full);
        }

        [TestMethod()]
        public void IncHLTest()
        {
            RegisterHandler.IncHL(registers, ExecutionParameters.DEFAULT, 1);
            Assert.AreEqual(0x1213, registers.HL.Full);
        }

        [TestMethod()]
        public void DecHLReadOnlyTest()
        {
            RegisterHandler.DecHL(registers, ExecutionParameters.DEBUGGER, 1);
            Assert.AreEqual(0x1212, registers.HL.Full);
        }

        [TestMethod()]
        public void DecHLTest()
        {
            RegisterHandler.DecHL(registers, ExecutionParameters.DEFAULT, 1);
            Assert.AreEqual(0x1211, registers.HL.Full);
        }

        [TestMethod()]
        public void IncSPReadOnlyTest()
        {
            RegisterHandler.IncSP(registers, ExecutionParameters.DEBUGGER, 1);
            Assert.AreEqual(0x1212, registers.SP.Full);
        }

        [TestMethod()]
        public void IncSPTest()
        {
            RegisterHandler.IncSP(registers, ExecutionParameters.DEFAULT, 1);
            Assert.AreEqual(0x1213, registers.SP.Full);
        }

        [TestMethod()]
        public void DecSPReadOnlyTest()
        {
            RegisterHandler.DecSP(registers, ExecutionParameters.DEBUGGER, 1);
            Assert.AreEqual(0x1212, registers.SP.Full);
        }

        [TestMethod()]
        public void DecSPTest()
        {
            RegisterHandler.DecSP(registers, ExecutionParameters.DEFAULT, 1);
            Assert.AreEqual(0x1211, registers.SP.Full);
        }

        #endregion

        #region IME/Halted

        [TestMethod()]
        public void SetIMEReadOnlyTest()
        {
            registers.IME = false;
            RegisterHandler.SetIME(registers, ExecutionParameters.DEBUGGER, true);
            Assert.AreEqual(false, registers.IME);
        }

        [TestMethod()]
        public void SetIMETest()
        {
            registers.IME = false;
            RegisterHandler.SetIME(registers, ExecutionParameters.DEFAULT, true);
            Assert.AreEqual(true, registers.IME);
        }

        [TestMethod()]
        public void SetHaltedReadOnlyTest()
        {
            registers.Halted = false;
            RegisterHandler.SetHalted(registers, ExecutionParameters.DEBUGGER, true);
            Assert.AreEqual(false, registers.Halted);
        }

        [TestMethod()]
        public void SetHaltedTest()
        {
            registers.Halted = false;
            RegisterHandler.SetHalted(registers, ExecutionParameters.DEFAULT, true);
            Assert.AreEqual(true, registers.Halted);
        }

        #endregion
    }
}