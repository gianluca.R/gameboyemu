﻿using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.Utils;
using GameBoyEmu.Emu.VirtualComponents;
using GameBoyEmuTests.Emu.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameBoyEmu.Emu.Instructions.Subsets.Tests
{
    [TestClass()]
    public class ControlInstructionsTests
    {
        Registers registers;
        RAM ram;

        [TestInitialize()]
        public void Setup()
        {
            registers = new Registers();

            registers.PC.Full = 0x100;
            registers.AF.Full = 0x1200;
            registers.BC.Full = 0x5678;
            registers.DE.Full = 0x9ABC;
            registers.HL.Full = 0xDEF1;
            registers.SP.Full = 0xFFF4;

            ram = new RAM();
        }

        #region DI/EI

        [TestMethod()]
        public void DisableInterruptsTest()
        {
            var di = ControlInstructions.SetIME(false);

            registers.IME = true;
            di(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.IME, false);
        }

        [TestMethod()]
        public void EnableInterruptsTest()
        {
            var ei = ControlInstructions.SetIME(true);

            registers.IME = false;
            ei(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.IME, true);
        }

        #endregion

        #region CCF/SCF

        [TestMethod()]
        public void ComplementCarryFlagWithFlagCNotSetTest()
        {
            var ccf = ControlInstructions.ComplementCarryFlag();

            registers.flagC = false;
            ccf(null, registers, ExecutionParameters.DEFAULT);
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }
        [TestMethod()]
        public void ComplementCarryFlagWithFlagCSetTest()
        {
            var ccf = ControlInstructions.ComplementCarryFlag();

            registers.flagC = true;
            ccf(null, registers, ExecutionParameters.DEFAULT);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }
        [TestMethod()]
        public void SetCarryFlagTest()
        {
            var scf = ControlInstructions.SetCarryFlag();

            registers.flagC = false;
            scf(null, registers, ExecutionParameters.DEFAULT);
            AssertUtils.AssertFlags(registers, false, false, false, true);

            registers.flagC = true;
            scf(null, registers, ExecutionParameters.DEFAULT);
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        #endregion

        #region HALT

        [TestMethod()]
        public void HaltWithInterruptsEnabledTest()
        {
            var halt = ControlInstructions.Halt();
            MemoryRegistersUtils.SetInterruptEnable(ram, 0xFF);
            MemoryRegistersUtils.SetInterruptFlag(ram, 0xFF);

            registers.Halted = false;
            registers.IME = true;
            halt(ram, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.Halted, true);
        }

        #endregion
    }
}