﻿using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;
using GameBoyEmuTests.Emu.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Reflection;
using System.Runtime.Serialization;

namespace GameBoyEmu.Emu.Instructions.Subsets.Tests
{
    [TestClass()]
    public class ArithmeticInstructionsMappingTests
    {
        Registers registers;
        InstructionSet instructionSet;
        RAM ram;

        [TestInitialize()]
        public void Setup()
        {
            registers = new Registers();

            registers.PC.Full = 0x100;
            registers.AF.Full = 0x1200;
            registers.BC.Full = 0x5678;
            registers.DE.Full = 0x9ABC;
            registers.HL.Full = 0xDEF1;
            registers.SP.Full = 0xFFF4;

            ram = new RAM();
            instructionSet = new InstructionSet();
        }

        #region XOR n

        [TestMethod()]
        public void XorAMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xAF],
                ArithmeticInstructions.XorA(InstructionAccessors.ReadByteFromAccessor(Accessor.A)),
                "XOR A", 4);
        }
        [TestMethod()]
        public void XorBMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xA8],
                ArithmeticInstructions.XorA(InstructionAccessors.ReadByteFromAccessor(Accessor.B)),
                "XOR B", 4);
        }
        [TestMethod()]
        public void XorCMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xA9],
                ArithmeticInstructions.XorA(InstructionAccessors.ReadByteFromAccessor(Accessor.C)),
                "XOR C", 4);
        }
        [TestMethod()]
        public void XorDMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xAA],
                ArithmeticInstructions.XorA(InstructionAccessors.ReadByteFromAccessor(Accessor.D)),
                "XOR D", 4);
        }
        [TestMethod()]
        public void XorEMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xAB],
                ArithmeticInstructions.XorA(InstructionAccessors.ReadByteFromAccessor(Accessor.E)),
                "XOR E", 4);
        }
        [TestMethod()]
        public void XorHMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xAC],
                ArithmeticInstructions.XorA(InstructionAccessors.ReadByteFromAccessor(Accessor.H)),
                "XOR H", 4);
        }
        [TestMethod()]
        public void XorLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xAD],
                ArithmeticInstructions.XorA(InstructionAccessors.ReadByteFromAccessor(Accessor.L)),
                "XOR L", 4);
        }
        [TestMethod()]
        public void XorAtHLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xAE],
                ArithmeticInstructions.XorA(InstructionAccessors.ReadByteFromAccessor(Accessor.atHL)),
                "XOR (HL)", 8);
        }

        #endregion

        #region ADD A, n

        [TestMethod()]
        public void AddAMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x87],
                ArithmeticInstructions.AddA(InstructionAccessors.ReadByteFromAccessor(Accessor.A)),
                "ADD A, A", 4);
        }
        [TestMethod()]
        public void AddBMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x80],
                ArithmeticInstructions.AddA(InstructionAccessors.ReadByteFromAccessor(Accessor.B)),
                "ADD A, B", 4);
        }
        [TestMethod()]
        public void AddCMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x81],
                ArithmeticInstructions.AddA(InstructionAccessors.ReadByteFromAccessor(Accessor.C)),
                "ADD A, C", 4);
        }
        [TestMethod()]
        public void AddDMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x82],
                ArithmeticInstructions.AddA(InstructionAccessors.ReadByteFromAccessor(Accessor.D)),
                "ADD A, D", 4);
        }
        [TestMethod()]
        public void AddAEappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x83],
                ArithmeticInstructions.AddA(InstructionAccessors.ReadByteFromAccessor(Accessor.E)),
                "ADD A, E", 4);
        }
        [TestMethod()]
        public void AddHMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x84],
                ArithmeticInstructions.AddA(InstructionAccessors.ReadByteFromAccessor(Accessor.H)),
                "ADD A, H", 4);
        }
        [TestMethod()]
        public void AddLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x85],
                ArithmeticInstructions.AddA(InstructionAccessors.ReadByteFromAccessor(Accessor.L)),
                "ADD A, L", 4);
        }
        [TestMethod()]
        public void AddAtHLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x86],
                ArithmeticInstructions.AddA(InstructionAccessors.ReadByteFromAccessor(Accessor.atHL)),
                "ADD A, (HL)", 8);
        }
        [TestMethod()]
        public void AddNMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xC6],
                ArithmeticInstructions.AddA(InstructionAccessors.ReadByteFromAccessor(Accessor.n)),
                "ADD A, n", 8);
        }

        #endregion

        #region ADD HL, nn

        [TestMethod()]
        public void AddBCMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x09],
                ArithmeticInstructions.AddHL(InstructionAccessors.ReadShortFromAccessor(Accessor.BC)),
                "ADD HL, BC", 8);
        }
        [TestMethod()]
        public void AddDEMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x19],
                ArithmeticInstructions.AddHL(InstructionAccessors.ReadShortFromAccessor(Accessor.DE)),
                "ADD HL, DE", 8);
        }
        [TestMethod()]
        public void AddHLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x29],
                ArithmeticInstructions.AddHL(InstructionAccessors.ReadShortFromAccessor(Accessor.HL)),
                "ADD HL, HL", 8);
        }
        [TestMethod()]
        public void AddSPMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x39],
                ArithmeticInstructions.AddHL(InstructionAccessors.ReadShortFromAccessor(Accessor.SP)),
                "ADD HL, SP", 8);
        }

        #endregion

        #region ADC A, n

        [TestMethod()]
        public void AddCarryAMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x8F],
                ArithmeticInstructions.AddCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.A)),
                "ADC A, A", 4);
        }
        [TestMethod()]
        public void AddCarryBMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x88],
                ArithmeticInstructions.AddCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.B)),
                "ADC A, B", 4);
        }
        [TestMethod()]
        public void AddCarryCMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x89],
                ArithmeticInstructions.AddCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.C)),
                "ADC A, C", 4);
        }
        [TestMethod()]
        public void AddCarryDMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x8A],
                ArithmeticInstructions.AddCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.D)),
                "ADC A, D", 4);
        }
        [TestMethod()]
        public void AddCarryEMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x8B],
                ArithmeticInstructions.AddCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.E)),
                "ADC A, E", 4);
        }
        [TestMethod()]
        public void AddCarryHMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x8C],
                ArithmeticInstructions.AddCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.H)),
                "ADC A, H", 4);
        }
        [TestMethod()]
        public void AddCarryLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x8D],
                ArithmeticInstructions.AddCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.L)),
                "ADC A, L", 4);
        }
        [TestMethod()]
        public void AddCarryAtHLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x8E],
                ArithmeticInstructions.AddCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.atHL)),
                "ADC A, (HL)", 8);
        }
        [TestMethod()]
        public void AddCarryNMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xCE],
                ArithmeticInstructions.AddCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.n)),
                "ADC A, n", 8);
        }

        #endregion

        #region SUB A, n

        [TestMethod()]
        public void SubAMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x97],
                ArithmeticInstructions.SubA(InstructionAccessors.ReadByteFromAccessor(Accessor.A)),
                "SUB A, A", 4);
        }
        [TestMethod()]
        public void SubBMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x90],
                ArithmeticInstructions.SubA(InstructionAccessors.ReadByteFromAccessor(Accessor.B)),
                "SUB A, B", 4);
        }
        [TestMethod()]
        public void SubCMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x91],
                ArithmeticInstructions.SubA(InstructionAccessors.ReadByteFromAccessor(Accessor.C)),
                "SUB A, C", 4);
        }
        [TestMethod()]
        public void SubDMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x92],
                ArithmeticInstructions.SubA(InstructionAccessors.ReadByteFromAccessor(Accessor.D)),
                "SUB A, D", 4);
        }
        [TestMethod()]
        public void SubEMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x93],
                ArithmeticInstructions.SubA(InstructionAccessors.ReadByteFromAccessor(Accessor.E)),
                "SUB A, E", 4);
        }
        [TestMethod()]
        public void SubHMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x94],
                ArithmeticInstructions.SubA(InstructionAccessors.ReadByteFromAccessor(Accessor.H)),
                "SUB A, H", 4);
        }
        [TestMethod()]
        public void SubLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x95],
                ArithmeticInstructions.SubA(InstructionAccessors.ReadByteFromAccessor(Accessor.L)),
                "SUB A, L", 4);
        }
        [TestMethod()]
        public void SubAtHLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x96],
                ArithmeticInstructions.SubA(InstructionAccessors.ReadByteFromAccessor(Accessor.atHL)),
                "SUB A, (HL)", 8);
        }
        [TestMethod()]
        public void SubNMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xD6],
                ArithmeticInstructions.SubA(InstructionAccessors.ReadByteFromAccessor(Accessor.n)),
                "SUB A, n", 8);
        }

        #endregion

        #region SBC A, n

        [TestMethod()]
        public void SubCarryAMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x9F],
                ArithmeticInstructions.SubCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.A)),
                "SBC A, A", 4);
        }
        [TestMethod()]
        public void SubCarryBMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x98],
                ArithmeticInstructions.SubCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.B)),
                "SBC A, B", 4);
        }
        [TestMethod()]
        public void SubCarryCMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x99],
                ArithmeticInstructions.SubCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.C)),
                "SBC A, C", 4);
        }
        [TestMethod()]
        public void SubCarryDMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x9A],
                ArithmeticInstructions.SubCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.D)),
                "SBC A, D", 4);
        }
        [TestMethod()]
        public void SubCarryEMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x9B],
                ArithmeticInstructions.SubCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.E)),
                "SBC A, E", 4);
        }
        [TestMethod()]
        public void SubCarryHMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x9C],
                ArithmeticInstructions.SubCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.H)),
                "SBC A, H", 4);
        }
        [TestMethod()]
        public void SubCarryLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x9D],
                ArithmeticInstructions.SubCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.L)),
                "SBC A, L", 4);
        }
        [TestMethod()]
        public void SubCarryAtHLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x9E],
                ArithmeticInstructions.SubCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.atHL)),
                "SBC A, (HL)", 8);
        }
        [TestMethod()]
        public void SubCarryNMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xDE],
                ArithmeticInstructions.SubCarryA(InstructionAccessors.ReadByteFromAccessor(Accessor.n)),
                "SBC A, n", 8);
        }

        #endregion

        #region INC n

        [TestMethod()]
        public void IncAMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x3C],
                ArithmeticInstructions.Inc(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.A), 
                    InstructionAccessors.WriteByteToAccessor(Accessor.A)),
                "INC A", 4);
        }
        [TestMethod()]
        public void IncBMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x04],
                ArithmeticInstructions.Inc(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.B),
                    InstructionAccessors.WriteByteToAccessor(Accessor.B)),
                "INC B", 4);
        }
        [TestMethod()]
        public void IncCMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x0C],
                ArithmeticInstructions.Inc(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.C),
                    InstructionAccessors.WriteByteToAccessor(Accessor.C)),
                "INC C", 4);
        }
        [TestMethod()]
        public void IncDMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x14],
                ArithmeticInstructions.Inc(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.D),
                    InstructionAccessors.WriteByteToAccessor(Accessor.D)),
                "INC D", 4);
        }
        [TestMethod()]
        public void IncEMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x1C],
                ArithmeticInstructions.Inc(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.E),
                    InstructionAccessors.WriteByteToAccessor(Accessor.E)),
                "INC E", 4);
        }
        [TestMethod()]
        public void IncHMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x24],
                ArithmeticInstructions.Inc(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.H),
                    InstructionAccessors.WriteByteToAccessor(Accessor.H)),
                "INC H", 4);
        }
        [TestMethod()]
        public void IncLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x2C],
                ArithmeticInstructions.Inc(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.L),
                    InstructionAccessors.WriteByteToAccessor(Accessor.L)),
                "INC L", 4);
        }
        [TestMethod()]
        public void IncAtHLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x34],
                ArithmeticInstructions.Inc(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.atHL),
                    InstructionAccessors.WriteByteToAccessor(Accessor.atHL)),
                "INC (HL)", 12);
        }

        #endregion

        #region INC nn

        [TestMethod()]
        public void IncBCMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x03],
                ArithmeticInstructions.Inc(
                    InstructionAccessors.ReadShortFromAccessor(Accessor.BC),
                    InstructionAccessors.WriteShortToAccessor(Accessor.BC)),
                "INC BC", 8);
        }
        [TestMethod()]
        public void IncDEMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x13],
                ArithmeticInstructions.Inc(
                    InstructionAccessors.ReadShortFromAccessor(Accessor.DE),
                    InstructionAccessors.WriteShortToAccessor(Accessor.DE)),
                "INC DE", 8);
        }
        [TestMethod()]
        public void IncHLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x23],
                ArithmeticInstructions.Inc(
                    InstructionAccessors.ReadShortFromAccessor(Accessor.HL),
                    InstructionAccessors.WriteShortToAccessor(Accessor.HL)),
                "INC HL", 8);
        }
        [TestMethod()]
        public void IncSPMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x33],
                ArithmeticInstructions.Inc(
                    InstructionAccessors.ReadShortFromAccessor(Accessor.SP),
                    InstructionAccessors.WriteShortToAccessor(Accessor.SP)),
                "INC SP", 8);
        }

        #endregion

        #region DEC n

        [TestMethod()]
        public void DecAMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x3D],
                ArithmeticInstructions.Dec(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.A),
                    InstructionAccessors.WriteByteToAccessor(Accessor.A)),
                "DEC A", 4);
        }
        [TestMethod()]
        public void DecBMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x05],
                ArithmeticInstructions.Dec(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.B),
                    InstructionAccessors.WriteByteToAccessor(Accessor.B)),
                "DEC B", 4);
        }
        [TestMethod()]
        public void DecCMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x0D],
                ArithmeticInstructions.Dec(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.C),
                    InstructionAccessors.WriteByteToAccessor(Accessor.C)),
                "DEC C", 4);
        }
        [TestMethod()]
        public void DecDMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x15],
                ArithmeticInstructions.Dec(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.D),
                    InstructionAccessors.WriteByteToAccessor(Accessor.D)),
                "DEC D", 4);
        }
        [TestMethod()]
        public void DecEMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x1D],
                ArithmeticInstructions.Dec(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.E),
                    InstructionAccessors.WriteByteToAccessor(Accessor.E)),
                "DEC E", 4);
        }
        [TestMethod()]
        public void DecHMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x25],
                ArithmeticInstructions.Dec(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.H),
                    InstructionAccessors.WriteByteToAccessor(Accessor.H)),
                "DEC H", 4);
        }
        [TestMethod()]
        public void DecLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x2D],
                ArithmeticInstructions.Dec(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.L),
                    InstructionAccessors.WriteByteToAccessor(Accessor.L)),
                "DEC L", 4);
        }
        [TestMethod()]
        public void DecAtHLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x35],
                ArithmeticInstructions.Dec(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.atHL),
                    InstructionAccessors.WriteByteToAccessor(Accessor.atHL)),
                "DEC (HL)", 12);
        }

        #endregion

        #region DEC nn

        [TestMethod()]
        public void DecBCMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x0B],
                ArithmeticInstructions.Dec(
                    InstructionAccessors.ReadShortFromAccessor(Accessor.BC),
                    InstructionAccessors.WriteShortToAccessor(Accessor.BC)),
                "DEC BC", 8);
        }
        [TestMethod()]
        public void DecDEMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x1B],
                ArithmeticInstructions.Dec(
                    InstructionAccessors.ReadShortFromAccessor(Accessor.DE),
                    InstructionAccessors.WriteShortToAccessor(Accessor.DE)),
                "DEC DE", 8);
        }
        [TestMethod()]
        public void DecHLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x2B],
                ArithmeticInstructions.Dec(
                    InstructionAccessors.ReadShortFromAccessor(Accessor.HL),
                    InstructionAccessors.WriteShortToAccessor(Accessor.HL)),
                "DEC HL", 8);
        }
        [TestMethod()]
        public void DecSPMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x3B],
                ArithmeticInstructions.Dec(
                    InstructionAccessors.ReadShortFromAccessor(Accessor.SP),
                    InstructionAccessors.WriteShortToAccessor(Accessor.SP)),
                "DEC SP", 8);
        }

        #endregion

        #region CPL

        [TestMethod()]
        public void ComplementMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x2F],
                ArithmeticInstructions.Complement(),
                "CPL", 4);
        }

        #endregion
    }
}