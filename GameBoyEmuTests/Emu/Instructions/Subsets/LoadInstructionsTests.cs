﻿using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;
using GameBoyEmuTests.Emu.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace GameBoyEmu.Emu.Instructions.Subsets.Tests
{
    [TestClass()]
    public class LoadInstructionsTests
    {
        Registers registers;
        RAM ram;

        [TestInitialize()]
        public void Setup()
        {
            registers = new Registers();

            registers.PC.Full = 0x100;
            registers.AF.Full = 0x1200;
            registers.BC.Full = 0x5678;
            registers.DE.Full = 0x9ABC;
            registers.HL.Full = 0xDEF1;
            registers.SP.Full = 0xFFF4;

            ram = new RAM();
        }

        #region LD n, n

        [TestMethod()]
        public void LoadByteTest()
        {
            AccessorToken<byte> output = null;
            var load = LoadInstructions.Load(
                AccessorMocks.GenerateReadAccessor<byte>(0x34),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            load(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x34);
        }

        #endregion

        #region LD nn, nn

        [TestMethod()]
        public void LoadShortTest()
        {
            AccessorToken<UInt16> output = null;
            var load = LoadInstructions.Load(
                AccessorMocks.GenerateReadAccessor<UInt16>(0x3434),
                AccessorMocks.GenerateWriteAccessor<UInt16>(t => output = t));

            load(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x3434);
        }

        #endregion

        #region LD HL, SP+n

        //TODO: maybe test all flag conditions here

        [TestMethod()]
        public void LoadSPnIntoHLWithPositiveNTest()
        {
            var load = LoadInstructions.LoadSPnIntoHL();
            registers.PC.Full = 0xC000;
            registers.SP.Full = 0x1000;
            ram.WriteByte(registers.PC.Full, 0x15, true);

            load(ram, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(0x1015, registers.HL.Full);
            Assert.AreEqual(false, registers.flagH);
            Assert.AreEqual(false, registers.flagC);
        }

        [TestMethod()]
        public void LoadSPnIntoHLWithNegativeNTest()
        {
            var load = LoadInstructions.LoadSPnIntoHL();
            registers.PC.Full = 0xC000;
            registers.SP.Full = 0x1000;
            ram.WriteByte(registers.PC.Full, 0xEB, true); // 0xEB = -15

            load(ram, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(0x0FEB, registers.HL.Full);
            Assert.AreEqual(true, registers.flagH);
            Assert.AreEqual(true, registers.flagC);
        }

        #endregion

        #region POP nn/PUSH nn

        [TestMethod()]
        public void PopTest()
        {
            AccessorToken<UInt16> output = null;
            var pop = LoadInstructions.Pop(AccessorMocks.GenerateWriteAccessor<UInt16>(t => output = t));
            ram.WriteShort(registers.SP.Full, 0x9876, true);

            pop(ram, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(0x9876, output.writeValue);
            Assert.AreEqual(0xFFF6, registers.SP.Full);
        }

        [TestMethod()]
        public void PushTest()
        {
            var push = LoadInstructions.Push(AccessorMocks.GenerateReadAccessor<UInt16>(0x1337));

            push(ram, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(0x1337, ram.ReadShort(0xFFF2, true));
            Assert.AreEqual(0xFFF2, registers.SP.Full);
        }

        #endregion
    }
}