﻿using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;
using GameBoyEmuTests.Emu.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace GameBoyEmu.Emu.Instructions.Subsets.Tests
{
    [TestClass()]
    public class JumpInstructionsTests
    {
        Registers registers;

        [TestInitialize()]
        public void Setup()
        {
            registers = new Registers();

            registers.PC.Full = 0x100;
            registers.AF.Full = 0x1200;
            registers.BC.Full = 0x5678;
            registers.DE.Full = 0x9ABC;
            registers.HL.Full = 0xDEF1;
            registers.SP.Full = 0xFFF4;
        }

        #region JP/JR cc, nn

        [TestMethod()]
        public void JumpWithoutConditionTest()
        {
            var jump = JumpInstructions.Jump(null, AccessorMocks.GenerateReadAccessor<UInt16>(200));

            jump(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.PC.Full, 200); // Should change PC
        }

        [TestMethod()]
        public void JumpWithConditionTrueTest()
        {
            var jump = JumpInstructions.Jump(AccessorMocks.GenerateFlagAccessor(true), AccessorMocks.GenerateReadAccessor<UInt16>(200));

            jump(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.PC.Full, 200); // Should change PC
        }

        [TestMethod()]
        public void JumpWithConditionFalseTest()
        {
            var jump = JumpInstructions.Jump(AccessorMocks.GenerateFlagAccessor(false), AccessorMocks.GenerateReadAccessor<UInt16>(200));

            jump(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.PC.Full, 0x100); // Should not change PC
        }

        #endregion
    }
}