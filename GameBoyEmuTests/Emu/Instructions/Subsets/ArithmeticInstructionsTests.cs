﻿using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;
using GameBoyEmuTests.Emu.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace GameBoyEmu.Emu.Instructions.Subsets.Tests
{
    [TestClass()]
    public class ArithmeticInstructionsTests
    {
        Registers registers;

        [TestInitialize()]
        public void Setup()
        {
            registers = new Registers();

            registers.PC.Full = 0x100;
            registers.AF.Full = 0x1200;
            registers.BC.Full = 0x5678;
            registers.DE.Full = 0x9ABC;
            registers.HL.Full = 0xDEF1;
            registers.SP.Full = 0xFFF4;
        }

        #region XOR n

        [TestMethod()]
        public void XorAResetsFlagsNHCTest()
        {
            var xorA = ArithmeticInstructions.XorA(AccessorMocks.GenerateReadAccessor<byte>(0x50));

            registers.flagN = true;
            registers.flagH = true;
            registers.flagC = true;
            xorA(null, registers, ExecutionParameters.DEFAULT);

            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void XorAWithValueDifferentFromATest()
        {
            var xorA = ArithmeticInstructions.XorA(AccessorMocks.GenerateReadAccessor<byte>(0x50));

            xorA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x42); // Output is XOR
            AssertUtils.AssertFlags(registers, false, false, false, false); // FlagZ = false
        }

        [TestMethod()]
        public void XorAWithValueEqualToATest()
        {
            var xorA = ArithmeticInstructions.XorA(AccessorMocks.GenerateReadAccessor<byte>(0x12));

            xorA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0); // Output is XOR
            AssertUtils.AssertFlags(registers, true, false, false, false); // FlagZ = true
        }

        #endregion

        #region ADD A, n

        [TestMethod()]
        public void AddAResetsFlagNTest()
        {
            var addA = ArithmeticInstructions.AddA(AccessorMocks.GenerateReadAccessor<byte>(0x01));

            registers.flagN = true;
            addA(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, false);
        }

        [TestMethod()]
        public void AddAWithValueThatHasNoSideEffectsTest()
        {
            var addA = ArithmeticInstructions.AddA(AccessorMocks.GenerateReadAccessor<byte>(0x01));

            addA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x13); // Output is sum
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void AddAWithValueThatCausesHalfCarryTest()
        {
            var addA = ArithmeticInstructions.AddA(AccessorMocks.GenerateReadAccessor<byte>(0x0F));

            addA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x21); // Output is sum
            AssertUtils.AssertFlags(registers, false, false, true, false);
        }

        [TestMethod()]
        public void AddAWithValueThatCausesCarryTest()
        {
            var addA = ArithmeticInstructions.AddA(AccessorMocks.GenerateReadAccessor<byte>(0xF0));

            addA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x2); // Output is sum, but overflowed
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        [TestMethod()]
        public void AddAWithValueThatCausesCarryAndHalfCarryTest()
        {
            var addA = ArithmeticInstructions.AddA(AccessorMocks.GenerateReadAccessor<byte>(0xFF));

            addA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x11); // Output is sum, but overflowed
            AssertUtils.AssertFlags(registers, false, false, true, true);
        }

        [TestMethod()]
        public void AddAWithValueThatOutputsZeroTest()
        {
            var addA = ArithmeticInstructions.AddA(AccessorMocks.GenerateReadAccessor<byte>(0xEE));

            addA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0); // Output is sum, but overflowed
            AssertUtils.AssertFlags(registers, true, false, true, true);
        }

        #endregion

        #region ADD HL, nn

        [TestMethod()]
        public void AddHLResetsFlagNTest()
        {
            var addHL = ArithmeticInstructions.AddHL(AccessorMocks.GenerateReadAccessor<UInt16>(0x1));

            registers.flagN = true;
            addHL(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, false);
        }

        [TestMethod()]
        public void AddHLWithValueThatHasNoSideEffectsTest()
        {
            var addHL = ArithmeticInstructions.AddHL(AccessorMocks.GenerateReadAccessor<UInt16>(0x1));

            addHL(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.HL.Full, 0xDEF2); // Output is sum, but overflowed
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void AddHLWithValueThatCausesCarryTest()
        {
            var addHL = ArithmeticInstructions.AddHL(AccessorMocks.GenerateReadAccessor<UInt16>(0x3000));

            addHL(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.HL.Full, 0x0EF1); // Output is sum, but overflowed
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        [TestMethod()]
        public void AddHLWithValueThatCausesHalfCarryTest()
        {
            var addHL = ArithmeticInstructions.AddHL(AccessorMocks.GenerateReadAccessor<UInt16>(0x0FFF));

            addHL(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.HL.Full, 0xEEF0); // Output is sum
            AssertUtils.AssertFlags(registers, false, false, true, false);
        }

        [TestMethod()]
        public void AddHLWithValueThatCausesCarryHalfCarryTest()
        {
            var addHL = ArithmeticInstructions.AddHL(AccessorMocks.GenerateReadAccessor<UInt16>(0xFFFF));

            addHL(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.HL.Full, 0xDEF0); // Output is sum, but overflowed
            AssertUtils.AssertFlags(registers, false, false, true, true);
        }

        [TestMethod()]
        public void AddHLWithValueThatOutputsZeroTest()
        {
            var addHL = ArithmeticInstructions.AddHL(AccessorMocks.GenerateReadAccessor<UInt16>(0x210F));

            addHL(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.HL.Full, 0x0); // Output is sum, but overflowed
            AssertUtils.AssertFlags(registers, false, false, true, true); // Z not set on AddHL
        }

        #endregion

        #region ADC A, n

        [TestMethod()]
        public void AddCarryAResetsFlagNTest()
        {
            var addCarryA = ArithmeticInstructions.AddCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x1));

            registers.flagN = true;
            addCarryA(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, false);
        }

        [TestMethod()]
        public void AddCarryAWithCarryNotSetAndValueThatHasNoSideEffectsTest()
        {
            var addCarryA = ArithmeticInstructions.AddCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x1));

            addCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x13); // Output is sum
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void AddCarryAWithCarrySetAndValueThatHasNoSideEffectsTest()
        {
            var addCarryA = ArithmeticInstructions.AddCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x1));

            registers.flagC = true;
            addCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x14); // Output is sum
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void AddCarryAWithCarryNotSetAndValueThatCausesHalfCarryTest()
        {
            var addCarryA = ArithmeticInstructions.AddCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x0F));

            addCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x21); // Output is sum
            AssertUtils.AssertFlags(registers, false, false, true, false);
        }

        [TestMethod()]
        public void AddCarryAWithCarrySetAndValueThatCausesHalfCarryTest()
        {
            var addCarryA = ArithmeticInstructions.AddCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x0E));

            registers.flagC = true;
            addCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x21); // Output is sum
            AssertUtils.AssertFlags(registers, false, false, true, false);
        }

        [TestMethod()]
        public void AddCarryAWithCarryNotSetAndValueThatCausesCarryTest()
        {
            var addCarryA = ArithmeticInstructions.AddCarryA(AccessorMocks.GenerateReadAccessor<byte>(0xF0));

            addCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x2); // Output is sum, but overflowed
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        [TestMethod()]
        public void AddCarryAWithCarrySetAndValueThatCausesCarryTest()
        {
            var addCarryA = ArithmeticInstructions.AddCarryA(AccessorMocks.GenerateReadAccessor<byte>(0xF1));

            registers.flagC = true;
            addCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x4); // Output is sum, but overflowed
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        [TestMethod()]
        public void AddCarryAWithCarryNotSetAndValueThatCausesCarryAndHalfCarryTest()
        {
            var addCarryA = ArithmeticInstructions.AddCarryA(AccessorMocks.GenerateReadAccessor<byte>(0xFF));

            addCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x11); // Output is sum, but overflowed
            AssertUtils.AssertFlags(registers, false, false, true, true);
        }

        [TestMethod()]
        public void AddCarryAWithCarrySetAndValueThatCausesCarryAndHalfCarryTest()
        {
            var addCarryA = ArithmeticInstructions.AddCarryA(AccessorMocks.GenerateReadAccessor<byte>(0xEE));

            registers.flagC = true;
            addCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x1); // Output is sum, but overflowed
            AssertUtils.AssertFlags(registers, false, false, true, true);
        }

        [TestMethod()]
        public void AddCarryAWithCarryNotSetAndValueThatOutputsZeroTest()
        {
            var addCarryA = ArithmeticInstructions.AddCarryA(AccessorMocks.GenerateReadAccessor<byte>(0xEE));

            addCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0); // Output is sum, but overflowed
            AssertUtils.AssertFlags(registers, true, false, true, true);
        }

        [TestMethod()]
        public void AddCarryAWithCarrySetAndValueThatOutputsZeroTest()
        {
            var addCarryA = ArithmeticInstructions.AddCarryA(AccessorMocks.GenerateReadAccessor<byte>(0xED));

            registers.flagC = true;
            addCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0); // Output is sum, but overflowed
            AssertUtils.AssertFlags(registers, true, false, true, true);
        }

        #endregion

        #region SUB A, n

        [TestMethod()]
        public void SubASetsFlagNTest()
        {
            var subA = ArithmeticInstructions.SubA(AccessorMocks.GenerateReadAccessor<byte>(0x1));

            registers.flagN = false;
            subA(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, true);
        }

        [TestMethod()]
        public void SubAWithValueThatHasNoSideEffectsTest()
        {
            var subA = ArithmeticInstructions.SubA(AccessorMocks.GenerateReadAccessor<byte>(0x1));

            subA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x11); // Output is subtraction, but underflowed
            AssertUtils.AssertFlags(registers, false, true, false, false);
        }

        [TestMethod()]
        public void SubAWithValueThatCausesBorrowTest()
        {
            var subA = ArithmeticInstructions.SubA(AccessorMocks.GenerateReadAccessor<byte>(0x20));

            subA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0xF2); // Output is subtraction, but underflowed
            AssertUtils.AssertFlags(registers, false, true, false, true);
        }

        [TestMethod()]
        public void SubAWithValueThatCausesHalfBorrowTest()
        {
            var subA = ArithmeticInstructions.SubA(AccessorMocks.GenerateReadAccessor<byte>(0x03));

            subA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0xF); // Output is subtraction
            AssertUtils.AssertFlags(registers, false, true, true, false);
        }

        [TestMethod()]
        public void SubAWithValueThatCausesBorrowAndHalfBorrowTest()
        {
            var subA = ArithmeticInstructions.SubA(AccessorMocks.GenerateReadAccessor<byte>(0x13));

            subA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0xFF); // Output is subtraction, but underflowed
            AssertUtils.AssertFlags(registers, false, true, true, true);
        }

        [TestMethod()]
        public void SubAWithValueThatOutputsZeroTest()
        {
            var subA = ArithmeticInstructions.SubA(AccessorMocks.GenerateReadAccessor<byte>(0x12));

            subA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x0); // Output is subtraction
            AssertUtils.AssertFlags(registers, true, true, false, false);
        }

        #endregion

        #region SBC A, n

        [TestMethod()]
        public void SubCarryASetsFlagNTest()
        {
            var subCarryA = ArithmeticInstructions.SubCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x1));

            registers.flagN = false;
            subCarryA(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, true);
        }

        [TestMethod()]
        public void SubCarryAWithCarryNotSetAndValueThatHasNoSideEffectsTest()
        {
            var subCarryA = ArithmeticInstructions.SubCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x1));

            subCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x11); // Output is subtraction, but underflowed
            AssertUtils.AssertFlags(registers, false, true, false, false);
        }

        [TestMethod()]
        public void SubCarryAWithCarrySetAndValueThatHasNoSideEffectsTest()
        {
            var subCarryA = ArithmeticInstructions.SubCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x1));

            registers.flagC = true;
            subCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x10); // Output is subtraction, but underflowed
            AssertUtils.AssertFlags(registers, false, true, false, false);
        }

        [TestMethod()]
        public void SubCarryAWithCarryNotSetAndValueThatCausesBorrowTest()
        {
            var subCarryA = ArithmeticInstructions.SubCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x20));

            subCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0xF2); // Output is subtraction, but underflowed
            AssertUtils.AssertFlags(registers, false, true, false, true);
        }

        [TestMethod()]
        public void SubCarryAWithCarrySetAndValueThatCausesBorrowTest()
        {
            var subCarryA = ArithmeticInstructions.SubCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x20));

            registers.flagC = true;
            subCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0xF1); // Output is subtraction, but underflowed
            AssertUtils.AssertFlags(registers, false, true, false, true);
        }

        [TestMethod()]
        public void SubCarryAWithCarryNotSetAndValueThatCausesHalfBorrowTest()
        {
            var subCarryA = ArithmeticInstructions.SubCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x0F));

            subCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x3); // Output is subtraction
            AssertUtils.AssertFlags(registers, false, true, true, false);
        }

        [TestMethod()]
        public void SubCarryAWithCarrySetAndValueThatCausesHalfBorrowTest()
        {
            var subCarryA = ArithmeticInstructions.SubCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x02));

            registers.flagC = true;
            subCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0xF); // Output is subtraction
            AssertUtils.AssertFlags(registers, false, true, true, false);
        }

        [TestMethod()]
        public void SubCarryAWithCarryNotSetAndValueThatCausesBorrowAndHalfBorrowTest()
        {
            var subCarryA = ArithmeticInstructions.SubCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x13));

            subCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0xFF); // Output is subtraction, but underflowed
            AssertUtils.AssertFlags(registers, false, true, true, true);
        }

        [TestMethod()]
        public void SubCarryAWithCarrySetAndValueThatCausesBorrowAndHalfBorrowTest()
        {
            var subCarryA = ArithmeticInstructions.SubCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x12));

            registers.flagC = true;
            subCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0xFF); // Output is subtraction, but underflowed
            AssertUtils.AssertFlags(registers, false, true, true, true);
        }

        [TestMethod()]
        public void SubCarryAWithCarryNotSetAndValueThatOutputsZeroTest()
        {
            var subCarryA = ArithmeticInstructions.SubCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x12));

            subCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x0); // Output is subtraction
            AssertUtils.AssertFlags(registers, true, true, false, false);
        }

        [TestMethod()]
        public void SubCarryAWithCarrySetAndValueThatOutputsZeroTest()
        {
            var subCarryA = ArithmeticInstructions.SubCarryA(AccessorMocks.GenerateReadAccessor<byte>(0x11));

            registers.flagC = true;
            subCarryA(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x0); // Output is subtraction
            AssertUtils.AssertFlags(registers, true, true, false, false);
        }

        #endregion

        #region INC n

        [TestMethod()]
        public void IncByteResetsFlagNTest()
        {
            AccessorToken<byte> output = null;
            var inc = ArithmeticInstructions.Inc(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagN = true;
            inc(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, false);
        }

        [TestMethod()]
        public void IncByteOnValueThatHasNoSideEffectsTest()
        {
            AccessorToken<byte> output = null;
            var inc = ArithmeticInstructions.Inc(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            inc(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x2);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void IncByteOnValueThatWillCauseHalfCarryTest()
        {
            AccessorToken<byte> output = null;
            var inc = ArithmeticInstructions.Inc(
                AccessorMocks.GenerateReadAccessor<byte>(0xF),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            inc(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x10);
            AssertUtils.AssertFlags(registers, false, false, true, false);
        }

        [TestMethod()]
        public void IncByteOnValueThatWillCauseOverflowTest()
        {
            AccessorToken<byte> output = null;
            var inc = ArithmeticInstructions.Inc(
                AccessorMocks.GenerateReadAccessor<byte>(0xFF),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            inc(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x0);
            AssertUtils.AssertFlags(registers, true, false, true, false); //C not affected
        }

        #endregion

        #region INC nn

        [TestMethod()]
        public void IncShortDoesNotAffectFlagsTest()
        {
            AccessorToken<UInt16> output = null;
            var inc = ArithmeticInstructions.Inc(
                AccessorMocks.GenerateReadAccessor<UInt16>(0x1),
                AccessorMocks.GenerateWriteAccessor<UInt16>(t => output = t));

            inc(null, registers, ExecutionParameters.DEFAULT);

            AssertUtils.AssertFlags(registers, false, false, false, false); //flags not affected

            // Test that flags are not affected
            registers.flagZ = true;
            registers.flagN = true;
            registers.flagH = true;
            registers.flagC = true;
            inc(null, registers, ExecutionParameters.DEFAULT);

            AssertUtils.AssertFlags(registers, true, true, true, true); //flags not affected
        }

        [TestMethod()]
        public void IncShortOnValueThatHasNoSideEffectsTest()
        {
            AccessorToken<UInt16> output = null;
            var inc = ArithmeticInstructions.Inc(
                AccessorMocks.GenerateReadAccessor<UInt16>(0x1),
                AccessorMocks.GenerateWriteAccessor<UInt16>(t => output = t));

            inc(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x2);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void IncShortOnValueThatWillCauseHalfCarryTest()
        {
            AccessorToken<UInt16> output = null;
            var inc = ArithmeticInstructions.Inc(
                AccessorMocks.GenerateReadAccessor<UInt16>(0xF),
                AccessorMocks.GenerateWriteAccessor<UInt16>(t => output = t));

            inc(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x10);
            AssertUtils.AssertFlags(registers, false, false, false, false); //flags not affected
        }

        [TestMethod()]
        public void IncShortOnValueThatWillCauseOverflowTest()
        {
            AccessorToken<UInt16> output = null;
            var inc = ArithmeticInstructions.Inc(
                AccessorMocks.GenerateReadAccessor<UInt16>(0xFFFF),
                AccessorMocks.GenerateWriteAccessor<UInt16>(t => output = t));

            inc(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x0);
            AssertUtils.AssertFlags(registers, false, false, false, false); //flags not affected
        }

        #endregion

        #region DEC n

        [TestMethod()]
        public void DecByteSetsFlagNTest()
        {
            AccessorToken<byte> output = null;
            var dec = ArithmeticInstructions.Dec(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagN = false;
            dec(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, true);
        }

        [TestMethod()]
        public void DecByteOnValueThatHasNoSideEffectsTest()
        {
            AccessorToken<byte> output = null;
            var dec = ArithmeticInstructions.Dec(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            dec(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x0);
            AssertUtils.AssertFlags(registers, true, true, false, false);
        }

        [TestMethod()]
        public void DecByteOnValueThatWillCauseHalfBorrowTest()
        {
            AccessorToken<byte> output = null;
            var dec = ArithmeticInstructions.Dec(
                AccessorMocks.GenerateReadAccessor<byte>(0x10),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            dec(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0xF);
            AssertUtils.AssertFlags(registers, false, true, true, false);
        }

        [TestMethod()]
        public void DecByteOnValueThatWillCauseUnderflowTest()
        {
            AccessorToken<byte> output = null;
            var dec = ArithmeticInstructions.Dec(
                AccessorMocks.GenerateReadAccessor<byte>(0x0),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            dec(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0xFF);
            AssertUtils.AssertFlags(registers, false, true, true, false); //C not affected
        }

        #endregion

        #region DEC nn

        [TestMethod()]
        public void DecShortDoesNotAffectFlagsTest()
        {
            AccessorToken<UInt16> output = null;
            var dec = ArithmeticInstructions.Dec(
                AccessorMocks.GenerateReadAccessor<UInt16>(0x1),
                AccessorMocks.GenerateWriteAccessor<UInt16>(t => output = t));

            dec(null, registers, ExecutionParameters.DEFAULT);

            AssertUtils.AssertFlags(registers, false, false, false, false); //flags not affected

            registers.flagZ = true;
            registers.flagN = true;
            registers.flagH = true;
            registers.flagC = true;
            dec(null, registers, ExecutionParameters.DEFAULT);

            AssertUtils.AssertFlags(registers, true, true, true, true); //flags not affected
        }

        [TestMethod()]
        public void DecShortOnValueThatHasNoSideEffectsTest()
        {
            AccessorToken<UInt16> output = null;
            var dec = ArithmeticInstructions.Dec(
                AccessorMocks.GenerateReadAccessor<UInt16>(0x1),
                AccessorMocks.GenerateWriteAccessor<UInt16>(t => output = t));

            dec(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x0);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void DecShortOnValueThatWillCauseHalfCarryTest()
        {
            AccessorToken<UInt16> output = null;
            var dec = ArithmeticInstructions.Dec(
                AccessorMocks.GenerateReadAccessor<UInt16>(0x10),
                AccessorMocks.GenerateWriteAccessor<UInt16>(t => output = t));

            dec(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0xF);
            AssertUtils.AssertFlags(registers, false, false, false, false); //flags not affected
        }

        [TestMethod()]
        public void DecShortOnValueThatWillCauseUnderflowTest()
        {
            AccessorToken<UInt16> output = null;
            var dec = ArithmeticInstructions.Dec(
                AccessorMocks.GenerateReadAccessor<UInt16>(0x0),
                AccessorMocks.GenerateWriteAccessor<UInt16>(t => output = t));

            dec(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0xFFFF);
            AssertUtils.AssertFlags(registers, false, false, false, false); //flags not affected
        }

        #endregion

        #region CPL

        [TestMethod()]
        public void ComplementDoesNotAffectFlagsZCTest()
        {
            var cpl = ArithmeticInstructions.Complement();

            cpl(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagZ, false);
            Assert.AreEqual(registers.flagC, false);

            registers.flagZ = true;
            registers.flagC = true;
            cpl(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagZ, true);
            Assert.AreEqual(registers.flagC, true);
        }

        [TestMethod()]
        public void ComplementSetsFlagsNHTest()
        {
            var cpl = ArithmeticInstructions.Complement();

            registers.flagN = false;
            registers.flagH = false;
            cpl(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, true);
            Assert.AreEqual(registers.flagH, true);
        }

        [TestMethod()]
        public void ComplementFlipsABitsTest()
        {
            var cpl = ArithmeticInstructions.Complement();

            cpl(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.AF.Left, 0xED);
            Assert.AreEqual(registers.flagN, true);
            Assert.AreEqual(registers.flagH, true);
        }

        #endregion
    }
}