﻿using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;
using GameBoyEmuTests.Emu.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameBoyEmu.Emu.Instructions.Subsets.Tests
{
    [TestClass()]
    public class CompareInstructionsTests
    {
        Registers registers;

        [TestInitialize()]
        public void Setup()
        {
            registers = new Registers();

            registers.PC.Full = 0x100;
            registers.AF.Full = 0x1200;
            registers.BC.Full = 0x5678;
            registers.DE.Full = 0x9ABC;
            registers.HL.Full = 0xDEF1;
            registers.SP.Full = 0xFFF4;
        }

        #region CP n

        [TestMethod()]
        public void CompareSetsFlagN()
        {
            var compare = CompareInstructions.CompareAccumulator(AccessorMocks.GenerateReadAccessor<byte>(0x1));

            registers.flagN = false;
            compare(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, true);
        }

        [TestMethod()]
        public void CompareWithValueThatHasNoSideEffectsTest()
        {
            var compare = CompareInstructions.CompareAccumulator(AccessorMocks.GenerateReadAccessor<byte>(0x1));

            compare(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x12);
            AssertUtils.AssertFlags(registers, false, true, false, false);
        }

        [TestMethod()]
        public void CompareWithValueThatWillOutputZeroTest()
        {
            var compare = CompareInstructions.CompareAccumulator(AccessorMocks.GenerateReadAccessor<byte>(0x12));

            compare(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x12);
            AssertUtils.AssertFlags(registers, true, true, false, false);
        }

        [TestMethod()]
        public void CompareWithValueThatWillCauseHalfBorrowTest()
        {
            var compare = CompareInstructions.CompareAccumulator(AccessorMocks.GenerateReadAccessor<byte>(0x0F));

            compare(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x12);
            AssertUtils.AssertFlags(registers, false, true, true, false);
        }

        [TestMethod()]
        public void CompareWithValueThatWillCauseBorrowTest()
        {
            var compare = CompareInstructions.CompareAccumulator(AccessorMocks.GenerateReadAccessor<byte>(0xF0));

            compare(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.AF.Left, 0x12);
            AssertUtils.AssertFlags(registers, false, true, false, true);
        }

        #endregion
    }
}