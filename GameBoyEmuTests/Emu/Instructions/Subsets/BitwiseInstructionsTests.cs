﻿using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;
using GameBoyEmuTests.Emu.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameBoyEmu.Emu.Instructions.Subsets.Tests
{
    [TestClass()]
    public class BitwiseInstructionsTests
    {
        Registers registers;

        [TestInitialize()]
        public void Setup()
        {
            registers = new Registers();

            registers.PC.Full = 0x100;
            registers.AF.Full = 0x1200;
            registers.BC.Full = 0x5678;
            registers.DE.Full = 0x9ABC;
            registers.HL.Full = 0xDEF1;
            registers.SP.Full = 0xFFF4;
        }

        #region AND n

        [TestMethod()]
        public void AndASetsFlagHResetsFlagsNCTest()
        {
            var andA = BitwiseInstructions.AndA(AccessorMocks.GenerateReadAccessor<byte>(0xFF));

            registers.flagN = true;
            registers.flagC = true;
            andA(null, registers, ExecutionParameters.DEFAULT);
            AssertUtils.AssertFlags(registers, false, false, true, false);
        }

        [TestMethod()]
        public void AndAWithValueThatMatchesNoBitsTest()
        {
            var andA = BitwiseInstructions.AndA(AccessorMocks.GenerateReadAccessor<byte>(0xED)); // 0xED = NOT 0x12

            andA(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.AF.Left, 0x0);
            AssertUtils.AssertFlags(registers, true, false, true, false);
        }

        [TestMethod()]
        public void AndAWithValueThatMatchesAllBitsTest()
        {
            var andA = BitwiseInstructions.AndA(AccessorMocks.GenerateReadAccessor<byte>(0xFF));

            andA(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.AF.Left, 0x12);
            AssertUtils.AssertFlags(registers, false, false, true, false);
        }

        [TestMethod()]
        public void AndAWithValueThatMatchesSomeBitsTest()
        {
            var andA = BitwiseInstructions.AndA(AccessorMocks.GenerateReadAccessor<byte>(0x0F));

            andA(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.AF.Left, 0x02);
            AssertUtils.AssertFlags(registers, false, false, true, false);
        }

        #endregion

        #region OR n

        [TestMethod()]
        public void OrASetsResetsFlagsNHCTest()
        {
            var orA = BitwiseInstructions.OrA(AccessorMocks.GenerateReadAccessor<byte>(0xFF));

            registers.flagN = true;
            registers.flagH = true;
            registers.flagC = true;
            orA(null, registers, ExecutionParameters.DEFAULT);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void OrAWithValue0Test()
        {
            var orA = BitwiseInstructions.OrA(AccessorMocks.GenerateReadAccessor<byte>(0x0));

            orA(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.AF.Left, 0x12);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void OrAWithValueFFTest()
        {
            var orA = BitwiseInstructions.OrA(AccessorMocks.GenerateReadAccessor<byte>(0xFF));

            orA(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.AF.Left, 0xFF);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void OrAWithValueThatHasSomeBitsSetTest()
        {
            var orA = BitwiseInstructions.OrA(AccessorMocks.GenerateReadAccessor<byte>(0x0F));

            orA(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.AF.Left, 0x1F);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void OrAOnZeroWithZeroTest()
        {
            var orA = BitwiseInstructions.OrA(AccessorMocks.GenerateReadAccessor<byte>(0x0));

            registers.AF.Left = 0;
            orA(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.AF.Left, 0x0);
            AssertUtils.AssertFlags(registers, true, false, false, false);
        }

        #endregion
    }
}