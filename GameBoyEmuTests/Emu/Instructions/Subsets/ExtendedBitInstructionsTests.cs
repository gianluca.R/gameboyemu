﻿using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;
using GameBoyEmuTests.Emu.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameBoyEmu.Emu.Instructions.Subsets.Tests
{
    [TestClass()]
    public class ExtendedBitInstructionsTests
    {
        Registers registers;

        [TestInitialize()]
        public void Setup()
        {
            registers = new Registers();

            registers.PC.Full = 0x100;
            registers.AF.Full = 0x1200;
            registers.BC.Full = 0x5678;
            registers.DE.Full = 0x9ABC;
            registers.HL.Full = 0xDEF1;
            registers.SP.Full = 0xFFF4;
        }

        #region RES b, n

        [TestMethod()]
        public void ResetDoesNotAffectFlagsTest()
        {
            AccessorToken<byte> output = null;
            var res = ExtendedBitInstructions.Reset(
                0,
                AccessorMocks.GenerateReadAccessor<byte>(0xF),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            res(null, registers, ExecutionParameters.DEFAULT);

            AssertUtils.AssertFlags(registers, false, false, false, false); // flags not affected

            registers.flagZ = true;
            registers.flagN = true;
            registers.flagH = true;
            registers.flagC = true;
            res(null, registers, ExecutionParameters.DEFAULT);

            AssertUtils.AssertFlags(registers, true, true, true, true); // flags not affected
        }

        [TestMethod()]
        public void ResetOnBit0WithValueThatHasItSetTest()
        {
            AccessorToken<byte> output = null;
            var res = ExtendedBitInstructions.Reset(
                0,
                AccessorMocks.GenerateReadAccessor<byte>(0xF),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            res(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0xE);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void ResetOnBit0WithValueThatHasItNotSetTest()
        {
            AccessorToken<byte> output = null;
            var res = ExtendedBitInstructions.Reset(
                0,
                AccessorMocks.GenerateReadAccessor<byte>(0xE),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            res(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0xE);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void ResetOnBit7WithValueThatHasItSetTest()
        {
            AccessorToken<byte> output = null;
            var res = ExtendedBitInstructions.Reset(
                7,
                AccessorMocks.GenerateReadAccessor<byte>(0xFF),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            res(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x7F);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void ResetOnBit7WithValueThatHasItNotSetTest()
        {
            AccessorToken<byte> output = null;
            var res = ExtendedBitInstructions.Reset(
                7,
                AccessorMocks.GenerateReadAccessor<byte>(0x7F),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            res(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x7F);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        #endregion

        #region SET b, n

        [TestMethod()]
        public void SetDoesNotAffectFlagsTest()
        {
            AccessorToken<byte> output = null;
            var set = ExtendedBitInstructions.Set(
                0,
                AccessorMocks.GenerateReadAccessor<byte>(0xE),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            set(null, registers, ExecutionParameters.DEFAULT);

            AssertUtils.AssertFlags(registers, false, false, false, false); // flags not affected

            registers.flagZ = true;
            registers.flagN = true;
            registers.flagH = true;
            registers.flagC = true;
            set(null, registers, ExecutionParameters.DEFAULT);

            AssertUtils.AssertFlags(registers, true, true, true, true); // flags not affected
        }

        [TestMethod()]
        public void SetOnBit0WithValueThatHasItNotSetTest()
        {
            AccessorToken<byte> output = null;
            var set = ExtendedBitInstructions.Set(
                0,
                AccessorMocks.GenerateReadAccessor<byte>(0xE),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            set(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0xF);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void SetOnBit0WithValueThatHasItSetTest()
        {
            AccessorToken<byte> output = null;
            var set = ExtendedBitInstructions.Set(
                0,
                AccessorMocks.GenerateReadAccessor<byte>(0xF),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            set(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0xF);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void SetOnBit7WithValueThatHasItNotSetTest()
        {
            AccessorToken<byte> output = null;
            var set = ExtendedBitInstructions.Set(
                7,
                AccessorMocks.GenerateReadAccessor<byte>(0x7F),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            set(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0xFF);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void SetOnBit7WithValueThatHasItSetTest()
        {
            AccessorToken<byte> output = null;
            var set = ExtendedBitInstructions.Set(
                7,
                AccessorMocks.GenerateReadAccessor<byte>(0xFF),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            set(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0xFF);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }
        #endregion

        #region BIT b, n

        [TestMethod()]
        public void TestDoesNotAffectCarryTest()
        {
            var test = ExtendedBitInstructions.Test(0, AccessorMocks.GenerateReadAccessor<byte>(0xF));

            test(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagC, false);

            registers.flagC = true;
            test(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagC, true);
        }

        [TestMethod()]
        public void TestOnBit0WithValueThatHasItSetTest()
        {
            var test = ExtendedBitInstructions.Test(0, AccessorMocks.GenerateReadAccessor<byte>(0xF));

            test(null, registers, ExecutionParameters.DEFAULT);

            AssertUtils.AssertFlags(registers, false, false, true, false);
        }

        [TestMethod()]
        public void TestOnBit0WithValueThatHasItNotSetTest()
        {
            var test = ExtendedBitInstructions.Test(0, AccessorMocks.GenerateReadAccessor<byte>(0xE));

            test(null, registers, ExecutionParameters.DEFAULT);

            AssertUtils.AssertFlags(registers, true, false, true, false);
        }

        [TestMethod()]
        public void TestOnBit7WithValueThatHasItSetTest()
        {
            var test = ExtendedBitInstructions.Test(7, AccessorMocks.GenerateReadAccessor<byte>(0xFF));

            test(null, registers, ExecutionParameters.DEFAULT);

            AssertUtils.AssertFlags(registers, false, false, true, false);
        }

        [TestMethod()]
        public void TestOnBit7WithValueThatHasItNotSetTest()
        {
            var test = ExtendedBitInstructions.Test(7, AccessorMocks.GenerateReadAccessor<byte>(0x7F));

            test(null, registers, ExecutionParameters.DEFAULT);

            AssertUtils.AssertFlags(registers, true, false, true, false);
        }
        #endregion

        #region SWAP n

        [TestMethod()]
        public void SwapResetsFlagsNHCTest()
        {
            AccessorToken<byte> output = null;
            var swap = ExtendedBitInstructions.Swap(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagC = true;
            registers.flagH = true;
            registers.flagN = true;
            swap(null, registers, ExecutionParameters.DEFAULT);

            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void SwapOnZeroTest()
        {
            AccessorToken<byte> output = null;
            var swap = ExtendedBitInstructions.Swap(
                AccessorMocks.GenerateReadAccessor<byte>(0x0),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            swap(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x0);
            AssertUtils.AssertFlags(registers, true, false, false, false);
        }

        [TestMethod()]
        public void SwapOnValueWithOneNibbleTest()
        {
            AccessorToken<byte> output = null;
            var swap = ExtendedBitInstructions.Swap(
                AccessorMocks.GenerateReadAccessor<byte>(0x5),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            swap(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x50);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void SwapOnValueWithTwoNibblesTest()
        {
            AccessorToken<byte> output = null;
            var swap = ExtendedBitInstructions.Swap(
                AccessorMocks.GenerateReadAccessor<byte>(0x35),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            swap(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x53);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        #endregion

        #region RRC n

        [TestMethod()]
        public void RotateRightResetsFlagsNHTest()
        {
            AccessorToken<byte> output = null;
            var rotateRight = ExtendedBitInstructions.RotateRight(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagH = true;
            registers.flagN = true;
            rotateRight(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, false);
            Assert.AreEqual(registers.flagH, false);
        }

        [TestMethod()]
        public void RotateRightOnValueWithBit0NotSetTest()
        {
            AccessorToken<byte> output = null;
            var rotateRight = ExtendedBitInstructions.RotateRight(
                AccessorMocks.GenerateReadAccessor<byte>(0xE),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            rotateRight(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x7);
            AssertUtils.AssertFlags(registers, false, false, false, false); // C not set because bit0=0
        }

        [TestMethod()]
        public void RotateRightOnValueWithBit0SetTest()
        {
            AccessorToken<byte> output = null;
            var rotateRight = ExtendedBitInstructions.RotateRight(
                AccessorMocks.GenerateReadAccessor<byte>(0xF),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            rotateRight(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x87);
            AssertUtils.AssertFlags(registers, false, false, false, true); // C set because bit0=1
        }

        [TestMethod()]
        public void RotateRightOnValue0Test()
        {
            AccessorToken<byte> output = null;
            var rotateRight = ExtendedBitInstructions.RotateRight(
                AccessorMocks.GenerateReadAccessor<byte>(0x0),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            rotateRight(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x0);
            AssertUtils.AssertFlags(registers, true, false, false, false);
        }

        #endregion

        #region RR n

        [TestMethod()]
        public void RotateRightThroughCarryResetsFlagsNHTest()
        {
            AccessorToken<byte> output = null;
            var rotateRightThroughCarry = ExtendedBitInstructions.RotateRightThroughCarry(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagH = true;
            registers.flagN = true;
            rotateRightThroughCarry(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, false);
            Assert.AreEqual(registers.flagH, false);
        }

        [TestMethod()]
        public void RotateRightThroughCarryOnValueWithBit0NotSetAndCarryNotSetTest()
        {
            AccessorToken<byte> output = null;
            var rotateRightThroughCarry = ExtendedBitInstructions.RotateRightThroughCarry(
                AccessorMocks.GenerateReadAccessor<byte>(0xE),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            rotateRightThroughCarry(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x7);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void RotateRightThroughCarryOnValueWithBit0NotSetAndCarrySetTest()
        {
            AccessorToken<byte> output = null;
            var rotateRightThroughCarry = ExtendedBitInstructions.RotateRightThroughCarry(
                AccessorMocks.GenerateReadAccessor<byte>(0xE),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagC = true;
            rotateRightThroughCarry(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x87);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void RotateRightThroughCarryOnValueWithBit0SetAndCarryNotSetTest()
        {
            AccessorToken<byte> output = null;
            var rotateRightThroughCarry = ExtendedBitInstructions.RotateRightThroughCarry(
                AccessorMocks.GenerateReadAccessor<byte>(0xF),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            rotateRightThroughCarry(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x7);
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        [TestMethod()]
        public void RotateRightThroughCarryOnValueWithBit0SetAndCarrySetTest()
        {
            AccessorToken<byte> output = null;
            var rotateRightThroughCarry = ExtendedBitInstructions.RotateRightThroughCarry(
                AccessorMocks.GenerateReadAccessor<byte>(0xF),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagC = true;
            rotateRightThroughCarry(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x87);
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        [TestMethod()]
        public void RotateRightThroughCarryOnValueWithOnlyBit0AndCarryNotSetTest()
        {
            AccessorToken<byte> output = null;
            var rotateRightThroughCarry = ExtendedBitInstructions.RotateRightThroughCarry(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            rotateRightThroughCarry(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x0);
            AssertUtils.AssertFlags(registers, true, false, false, true);
        }

        [TestMethod()]
        public void RotateRightThroughCarryOnValueWithOnlyBit0AndCarrySetTest()
        {
            AccessorToken<byte> output = null;
            var rotateRightThroughCarry = ExtendedBitInstructions.RotateRightThroughCarry(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagC = true;
            rotateRightThroughCarry(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x80);
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        #endregion

        #region RLC n

        [TestMethod()]
        public void RotateLeftResetsFlagsNHTest()
        {
            AccessorToken<byte> output = null;
            var rotateLeft = ExtendedBitInstructions.RotateLeft(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagH = true;
            registers.flagN = true;
            rotateLeft(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, false);
            Assert.AreEqual(registers.flagH, false);
        }

        [TestMethod()]
        public void RotateLeftOnValueWithBit7NotSetTest()
        {
            AccessorToken<byte> output = null;
            var rotateLeft = ExtendedBitInstructions.RotateLeft(
                AccessorMocks.GenerateReadAccessor<byte>(0x7F),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            rotateLeft(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0xFE);
            AssertUtils.AssertFlags(registers, false, false, false, false); // C not set because bit7=0
        }

        [TestMethod()]
        public void RotateLeftOnValueWithBit7SetTest()
        {
            AccessorToken<byte> output = null;
            var rotateLEft = ExtendedBitInstructions.RotateLeft(
                AccessorMocks.GenerateReadAccessor<byte>(0x80),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            rotateLEft(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x1);
            AssertUtils.AssertFlags(registers, false, false, false, true); // C set because bit7=1
        }

        [TestMethod()]
        public void RotateLeftOnValue0Test()
        {
            AccessorToken<byte> output = null;
            var rotateLeft = ExtendedBitInstructions.RotateLeft(
                AccessorMocks.GenerateReadAccessor<byte>(0x0),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            rotateLeft(null, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(output.writeValue, 0x0);
            AssertUtils.AssertFlags(registers, true, false, false, false);
        }

        #endregion

        #region RL n

        [TestMethod()]
        public void RotateLeftThroughCarryResetsFlagsNHTest()
        {
            AccessorToken<byte> output = null;
            var rotateLeftThroughCarry = ExtendedBitInstructions.RotateLeftThroughCarry(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagH = true;
            registers.flagN = true;
            rotateLeftThroughCarry(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, false);
            Assert.AreEqual(registers.flagH, false);
        }

        [TestMethod()]
        public void RotateLeftThroughCarryOnValueWithBit7NotSetAndCarryNotSetTest()
        {
            AccessorToken<byte> output = null;
            var rotateLeftThroughCarry = ExtendedBitInstructions.RotateLeftThroughCarry(
                AccessorMocks.GenerateReadAccessor<byte>(0x7F),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            rotateLeftThroughCarry(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0xFE);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void RotateLeftThroughCarryOnValueWithBit7NotSetAndCarrySetTest()
        {
            AccessorToken<byte> output = null;
            var rotateLeftThroughCarry = ExtendedBitInstructions.RotateLeftThroughCarry(
                AccessorMocks.GenerateReadAccessor<byte>(0x7F),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagC = true;
            rotateLeftThroughCarry(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0xFF);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void RotateLeftThroughCarryOnValueWithBit7SetAndCarryNotSetTest()
        {
            AccessorToken<byte> output = null;
            var rotateLeftThroughCarry = ExtendedBitInstructions.RotateLeftThroughCarry(
                AccessorMocks.GenerateReadAccessor<byte>(0xFF),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            rotateLeftThroughCarry(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0xFE);
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        [TestMethod()]
        public void RotateLeftThroughCarryOnValueWithBit7SetAndCarrySetTest()
        {
            AccessorToken<byte> output = null;
            var rotateLeftThroughCarry = ExtendedBitInstructions.RotateLeftThroughCarry(
                AccessorMocks.GenerateReadAccessor<byte>(0xFF),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagC = true;
            rotateLeftThroughCarry(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0xFF);
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        [TestMethod()]
        public void RotateLeftThroughCarryOnValueWithOnlyBit7AndCarryNotSetTest()
        {
            AccessorToken<byte> output = null;
            var rotateLeftThroughCarry = ExtendedBitInstructions.RotateLeftThroughCarry(
                AccessorMocks.GenerateReadAccessor<byte>(0x80),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            rotateLeftThroughCarry(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x0);
            AssertUtils.AssertFlags(registers, true, false, false, true);
        }

        [TestMethod()]
        public void RotateLeftThroughCarryOnValueWithOnlyBit7AndCarrySetTest()
        {
            AccessorToken<byte> output = null;
            var rotateLeftThroughCarry = ExtendedBitInstructions.RotateLeftThroughCarry(
                AccessorMocks.GenerateReadAccessor<byte>(0x80),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagC = true;
            rotateLeftThroughCarry(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x1);
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        #endregion

        #region SLA n

        [TestMethod()]
        public void ShiftLeftArithmeticResetsFlagsNHTest()
        {
            AccessorToken<byte> output = null;
            var shiftLeftArithmetic = ExtendedBitInstructions.ShiftLeftArithmetic(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagH = true;
            registers.flagN = true;
            shiftLeftArithmetic(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, false);
            Assert.AreEqual(registers.flagH, false);
        }

        [TestMethod()]
        public void ShiftLeftArithmeticWithBit7NotSetTest()
        {
            AccessorToken<byte> output = null;
            var shiftLeftArithmetic = ExtendedBitInstructions.ShiftLeftArithmetic(
                AccessorMocks.GenerateReadAccessor<byte>(0x7F),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            shiftLeftArithmetic(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0xFE);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void ShiftLeftArithmeticWithBit7SetTest()
        {
            AccessorToken<byte> output = null;
            var shiftLeftArithmetic = ExtendedBitInstructions.ShiftLeftArithmetic(
                AccessorMocks.GenerateReadAccessor<byte>(0xFF),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            shiftLeftArithmetic(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0xFE);
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        [TestMethod()]
        public void ShiftLeftArithmeticWithOnlyBit7SetTest()
        {
            AccessorToken<byte> output = null;
            var shiftLeftArithmetic = ExtendedBitInstructions.ShiftLeftArithmetic(
                AccessorMocks.GenerateReadAccessor<byte>(0x80),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            shiftLeftArithmetic(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x0);
            AssertUtils.AssertFlags(registers, true, false, false, true);
        }

        #endregion

        #region SRA n

        [TestMethod()]
        public void ShiftRightArithmeticResetsFlagsNHTest()
        {
            AccessorToken<byte> output = null;
            var shiftRightArithmetic = ExtendedBitInstructions.ShiftRightArithmetic(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagH = true;
            registers.flagN = true;
            shiftRightArithmetic(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, false);
            Assert.AreEqual(registers.flagH, false);
        }

        [TestMethod()]
        public void ShiftRightArithmeticWithBit0NotSetAndBit7NotSetTest()
        {
            AccessorToken<byte> output = null;
            var shiftRightArithmetic = ExtendedBitInstructions.ShiftRightArithmetic(
                AccessorMocks.GenerateReadAccessor<byte>(0x7E),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            shiftRightArithmetic(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x3F);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void ShiftRightArithmeticWithBit0NotSetAndBit7SetTest()
        {
            AccessorToken<byte> output = null;
            var shiftRightArithmetic = ExtendedBitInstructions.ShiftRightArithmetic(
                AccessorMocks.GenerateReadAccessor<byte>(0xFE),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            shiftRightArithmetic(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0xFF);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void ShiftRightArithmeticWithBit0SetAndBit7NotSetTest()
        {
            AccessorToken<byte> output = null;
            var shiftRightArithmetic = ExtendedBitInstructions.ShiftRightArithmetic(
                AccessorMocks.GenerateReadAccessor<byte>(0x7F),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            shiftRightArithmetic(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x3F);
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        [TestMethod()]
        public void ShiftRightArithmeticWithBit0SetAndBit7SetTest()
        {
            AccessorToken<byte> output = null;
            var shiftRightArithmetic = ExtendedBitInstructions.ShiftRightArithmetic(
                AccessorMocks.GenerateReadAccessor<byte>(0xBD),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            shiftRightArithmetic(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0xDE);
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        [TestMethod()]
        public void ShiftRightArithmeticWithOnlyBit0SetTest()
        {
            AccessorToken<byte> output = null;
            var shiftRightArithmetic = ExtendedBitInstructions.ShiftRightArithmetic(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            shiftRightArithmetic(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x0);
            AssertUtils.AssertFlags(registers, true, false, false, true);
        }

        #endregion

        #region SRL n

        [TestMethod()]
        public void ShiftRightLogicResetsFlagsNHTest()
        {
            AccessorToken<byte> output = null;
            var shiftRightLogic = ExtendedBitInstructions.ShiftRightLogic(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            registers.flagH = true;
            registers.flagN = true;
            shiftRightLogic(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(registers.flagN, false);
            Assert.AreEqual(registers.flagH, false);
        }

        [TestMethod()]
        public void ShiftRightLogicWithBit0NotSetTest()
        {
            AccessorToken<byte> output = null;
            var shiftRightLogic = ExtendedBitInstructions.ShiftRightLogic(
                AccessorMocks.GenerateReadAccessor<byte>(0xFE),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            shiftRightLogic(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x7F);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void ShiftRightLogicWithBit0SetTest()
        {
            AccessorToken<byte> output = null;
            var shiftRightLogic = ExtendedBitInstructions.ShiftRightLogic(
                AccessorMocks.GenerateReadAccessor<byte>(0xFF),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            shiftRightLogic(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x7F);
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        [TestMethod()]
        public void ShiftRightLogicWithOnlyBit0SetTest()
        {
            AccessorToken<byte> output = null;
            var shiftRightLogic = ExtendedBitInstructions.ShiftRightLogic(
                AccessorMocks.GenerateReadAccessor<byte>(0x1),
                AccessorMocks.GenerateWriteAccessor<byte>(t => output = t));

            shiftRightLogic(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(output.writeValue, 0x0);
            AssertUtils.AssertFlags(registers, true, false, false, true);
        }

        #endregion
    }
}