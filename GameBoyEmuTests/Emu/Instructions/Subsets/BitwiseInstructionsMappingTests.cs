﻿using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;
using GameBoyEmuTests.Emu.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Reflection;
using System.Runtime.Serialization;

namespace GameBoyEmu.Emu.Instructions.Subsets.Tests
{
    [TestClass()]
    public class BitwiseInstructionsMappingTests
    {
        Registers registers;
        InstructionSet instructionSet;
        RAM ram;

        [TestInitialize()]
        public void Setup()
        {
            registers = new Registers();

            registers.PC.Full = 0x100;
            registers.AF.Full = 0x1200;
            registers.BC.Full = 0x5678;
            registers.DE.Full = 0x9ABC;
            registers.HL.Full = 0xDEF1;
            registers.SP.Full = 0xFFF4;

            ram = new RAM();
            instructionSet = new InstructionSet();
        }

        #region AND n

        [TestMethod()]
        public void AndAMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xA7],
                BitwiseInstructions.AndA(InstructionAccessors.ReadByteFromAccessor(Accessor.A)),
                "AND A", 4);
        }
        [TestMethod()]
        public void AndBMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xA0],
                BitwiseInstructions.AndA(InstructionAccessors.ReadByteFromAccessor(Accessor.B)),
                "AND B", 4);
        }
        [TestMethod()]
        public void AndCMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xA1],
                BitwiseInstructions.AndA(InstructionAccessors.ReadByteFromAccessor(Accessor.C)),
                "AND C", 4);
        }
        [TestMethod()]
        public void AndDMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xA2],
                BitwiseInstructions.AndA(InstructionAccessors.ReadByteFromAccessor(Accessor.D)),
                "AND D", 4);
        }
        [TestMethod()]
        public void AndEMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xA3],
                BitwiseInstructions.AndA(InstructionAccessors.ReadByteFromAccessor(Accessor.E)),
                "AND E", 4);
        }
        [TestMethod()]
        public void AndHMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xA4],
                BitwiseInstructions.AndA(InstructionAccessors.ReadByteFromAccessor(Accessor.H)),
                "AND H", 4);
        }
        [TestMethod()]
        public void AndLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xA5],
                BitwiseInstructions.AndA(InstructionAccessors.ReadByteFromAccessor(Accessor.L)),
                "AND L", 4);
        }
        [TestMethod()]
        public void AndAtHLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xA6],
                BitwiseInstructions.AndA(InstructionAccessors.ReadByteFromAccessor(Accessor.atHL)),
                "AND (HL)", 8);
        }
        [TestMethod()]
        public void AndNMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xE6],
                BitwiseInstructions.AndA(InstructionAccessors.ReadByteFromAccessor(Accessor.n)),
                "AND n", 8);
        }

        #endregion

        #region OR n

        [TestMethod()]
        public void OrAMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xB7],
                BitwiseInstructions.OrA(InstructionAccessors.ReadByteFromAccessor(Accessor.A)),
                "OR A", 4);
        }
        [TestMethod()]
        public void OrBMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xB0],
                BitwiseInstructions.OrA(InstructionAccessors.ReadByteFromAccessor(Accessor.B)),
                "OR B", 4);
        }
        [TestMethod()]
        public void OrCMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xB1],
                BitwiseInstructions.OrA(InstructionAccessors.ReadByteFromAccessor(Accessor.C)),
                "OR C", 4);
        }
        [TestMethod()]
        public void OrDMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xB2],
                BitwiseInstructions.OrA(InstructionAccessors.ReadByteFromAccessor(Accessor.D)),
                "OR D", 4);
        }
        [TestMethod()]
        public void OrEMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xB3],
                BitwiseInstructions.OrA(InstructionAccessors.ReadByteFromAccessor(Accessor.E)),
                "OR E", 4);
        }
        [TestMethod()]
        public void OrHMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xB4],
                BitwiseInstructions.OrA(InstructionAccessors.ReadByteFromAccessor(Accessor.H)),
                "OR H", 4);
        }
        [TestMethod()]
        public void OrLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xB5],
                BitwiseInstructions.OrA(InstructionAccessors.ReadByteFromAccessor(Accessor.L)),
                "OR L", 4);
        }
        [TestMethod()]
        public void OrAtHLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xB6],
                BitwiseInstructions.OrA(InstructionAccessors.ReadByteFromAccessor(Accessor.atHL)),
                "OR (HL)", 8);
        }
        [TestMethod()]
        public void OrNMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0xF6],
                BitwiseInstructions.OrA(InstructionAccessors.ReadByteFromAccessor(Accessor.n)),
                "OR n", 8);
        }

        #endregion
    }
}