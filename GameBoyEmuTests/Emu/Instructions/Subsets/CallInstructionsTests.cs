﻿using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;
using GameBoyEmuTests.Emu.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace GameBoyEmu.Emu.Instructions.Subsets.Tests
{
    [TestClass()]
    public class CallInstructionsTests
    {
        Registers registers;
        RAM ram;

        [TestInitialize()]
        public void Setup()
        {
            registers = new Registers();

            registers.PC.Full = 0x100;
            registers.AF.Full = 0x1200;
            registers.BC.Full = 0x5678;
            registers.DE.Full = 0x9ABC;
            registers.HL.Full = 0xDEF1;
            registers.SP.Full = 0xFFF4;

            ram = new RAM();
        }

        #region CALL cc, nn

        [TestMethod()]
        public void CallWithoutConditionTest()
        {
            var call = CallInstructions.Call(null, AccessorMocks.GenerateReadAccessor<UInt16>(200));

            call(ram, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.PC.Full, 200); // Should change PC
            Assert.AreEqual(registers.SP.Full, 0xFFF2); // SP decreased after push
            Assert.AreEqual(ram.ReadShort(0xFFF2, true), 0x100); // Old PC is in memory at stack pointer
        }

        [TestMethod()]
        public void CallWithConditionTrueTest()
        {
            var call = CallInstructions.Call(AccessorMocks.GenerateFlagAccessor(true), AccessorMocks.GenerateReadAccessor<UInt16>(200));

            call(ram, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.PC.Full, 200); // Should change PC
            Assert.AreEqual(registers.SP.Full, 0xFFF2); // SP decreased after push
            Assert.AreEqual(ram.ReadShort(0xFFF2, true), 0x100); // Old PC is in memory at stack pointer
        }

        [TestMethod()]
        public void CallWithConditionFalseTest()
        {
            var call = CallInstructions.Call(AccessorMocks.GenerateFlagAccessor(false), AccessorMocks.GenerateReadAccessor<UInt16>(200));

            call(ram, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.PC.Full, 0x100); // PC unchanged
            Assert.AreEqual(registers.SP.Full, 0xFFF4); // SP unchanged
        }

        #endregion

        #region RET cc

        [TestMethod()]
        public void ReturnWithoutConditionTest()
        {
            var ret = CallInstructions.Return(null);
            ram.WriteShort(registers.SP.Full, 0x200, true); //Next value in stack

            ret(ram, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.PC.Full, 0x200); // PC changed to value in stack
            Assert.AreEqual(registers.SP.Full, 0xFFF6); // SP increased
        }

        [TestMethod()]
        public void ReturnWithConditionTrueTest()
        {
            var ret = CallInstructions.Return(AccessorMocks.GenerateFlagAccessor(true));
            ram.WriteShort(registers.SP.Full, 0x200, true); //Next value in stack

            ret(ram, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.PC.Full, 0x200); // PC changed to value in stack
            Assert.AreEqual(registers.SP.Full, 0xFFF6); // SP increased
        }

        [TestMethod()]
        public void ReturnWithConditionFalseTest()
        {
            var ret = CallInstructions.Return(AccessorMocks.GenerateFlagAccessor(false));
            ram.WriteShort(registers.SP.Full, 0x200, true); //Next value in stack

            ret(ram, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.PC.Full, 0x100); // PC unchanged
            Assert.AreEqual(registers.SP.Full, 0xFFF4); // SP unchanged
        }

        #endregion

        #region RETI

        [TestMethod()]
        public void ReturnInterruptTest()
        {
            var reti = CallInstructions.ReturnInterrupt();
            ram.WriteShort(registers.SP.Full, 0x200, true); //Next value in stack
            registers.IME = false;

            reti(ram, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.PC.Full, 0x200); // PC changed to value in stack
            Assert.AreEqual(registers.SP.Full, 0xFFF6); // SP increased
            Assert.AreEqual(registers.IME, true); // IME restored
        }

        #endregion

        #region RST n

        [TestMethod()]
        public void ResetTest()
        {
            var reset = CallInstructions.Reset(0x28);

            reset(ram, registers, ExecutionParameters.DEFAULT);

            Assert.AreEqual(registers.PC.Full, 0x28); // Should change PC
            Assert.AreEqual(registers.SP.Full, 0xFFF2); // SP decreased after push
            Assert.AreEqual(ram.ReadShort(0xFFF2, true), 0x100); // Old PC is in memory at stack pointer
        }

        #endregion
    }
}