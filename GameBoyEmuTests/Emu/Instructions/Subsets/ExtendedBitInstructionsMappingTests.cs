﻿using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;
using GameBoyEmuTests.Emu.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Reflection;
using System.Runtime.Serialization;

namespace GameBoyEmu.Emu.Instructions.Subsets.Tests
{
    [TestClass()]
    public class ExtendedBitInstructionsMappingTests
    {
        Registers registers;
        InstructionSet instructionSet;
        RAM ram;

        [TestInitialize()]
        public void Setup()
        {
            registers = new Registers();

            registers.PC.Full = 0x100;
            registers.AF.Full = 0x1200;
            registers.BC.Full = 0x5678;
            registers.DE.Full = 0x9ABC;
            registers.HL.Full = 0xDEF1;
            registers.SP.Full = 0xFFF4;

            ram = new RAM();
            instructionSet = new InstructionSet();
        }

        #region SWAP n

        [TestMethod()]
        public void SwapAMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.extendedInstructionSet[0x37],
                ExtendedBitInstructions.Swap(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.A),
                    InstructionAccessors.WriteByteToAccessor(Accessor.A)),
                "SWAP A", 8);
        }
        [TestMethod()]
        public void SwapBMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.extendedInstructionSet[0x30],
                ExtendedBitInstructions.Swap(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.B),
                    InstructionAccessors.WriteByteToAccessor(Accessor.B)),
                "SWAP B", 8);
        }
        [TestMethod()]
        public void SwapCMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.extendedInstructionSet[0x31],
                ExtendedBitInstructions.Swap(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.C),
                    InstructionAccessors.WriteByteToAccessor(Accessor.C)),
                "SWAP C", 8);
        }
        [TestMethod()]
        public void SwapDMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.extendedInstructionSet[0x32],
                ExtendedBitInstructions.Swap(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.D),
                    InstructionAccessors.WriteByteToAccessor(Accessor.D)),
                "SWAP D", 8);
        }
        [TestMethod()]
        public void SwapEMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.extendedInstructionSet[0x33],
                ExtendedBitInstructions.Swap(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.E),
                    InstructionAccessors.WriteByteToAccessor(Accessor.E)),
                "SWAP E", 8);
        }
        [TestMethod()]
        public void SwapHMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.extendedInstructionSet[0x34],
                ExtendedBitInstructions.Swap(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.H),
                    InstructionAccessors.WriteByteToAccessor(Accessor.H)),
                "SWAP H", 8);
        }
        [TestMethod()]
        public void SwapLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.extendedInstructionSet[0x35],
                ExtendedBitInstructions.Swap(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.L),
                    InstructionAccessors.WriteByteToAccessor(Accessor.L)),
                "SWAP L", 8);
        }
        [TestMethod()]
        public void SwapAtHLMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.extendedInstructionSet[0x36],
                ExtendedBitInstructions.Swap(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.atHL),
                    InstructionAccessors.WriteByteToAccessor(Accessor.atHL)),
                "SWAP (HL)", 16);
        }

        #endregion

        #region Rotate A

        [TestMethod()]
        public void RLCAMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x07],
                ExtendedBitInstructions.RotateLeft(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.A),
                    InstructionAccessors.WriteByteToAccessor(Accessor.A),
                    true),
                "RLCA", 4);
        }
        [TestMethod()]
        public void RLAMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x17],
                ExtendedBitInstructions.RotateLeftThroughCarry(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.A),
                    InstructionAccessors.WriteByteToAccessor(Accessor.A),
                    true),
                "RLA", 4);
        }
        [TestMethod()]
        public void RRCAMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x0F],
                ExtendedBitInstructions.RotateRight(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.A),
                    InstructionAccessors.WriteByteToAccessor(Accessor.A),
                    true),
                "RRCA", 4);
        }
        [TestMethod()]
        public void RRAMappingTest()
        {
            AssertUtils.ExecuteInstructionAndAssertBehaviour(
                ram, registers,
                instructionSet.instructionSet[0x1F],
                ExtendedBitInstructions.RotateRightThroughCarry(
                    InstructionAccessors.ReadByteFromAccessor(Accessor.A),
                    InstructionAccessors.WriteByteToAccessor(Accessor.A),
                    true),
                "RRA", 4);
        }

        #endregion

    }
}