﻿using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;
using GameBoyEmuTests.Emu.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GameBoyEmu.Emu.Instructions.Subsets.Tests
{
    [TestClass()]
    public class DecimalInstructionsTests
    {
        Registers registers;

        [TestInitialize()]
        public void Setup()
        {
            registers = new Registers();

            registers.PC.Full = 0x100;
            registers.AF.Full = 0x1200;
            registers.BC.Full = 0x5678;
            registers.DE.Full = 0x9ABC;
            registers.HL.Full = 0xDEF1;
            registers.SP.Full = 0xFFF4;
        }

        #region DAA after addition

        [TestMethod()]
        public void DecimalAdjustAAfterAdditionWithoutHalfCarryTest()
        {
            registers.AF.Left = 0x32;
            var addA = ArithmeticInstructions.AddA(AccessorMocks.GenerateReadAccessor<byte>(0x25));
            var daa = DecimalInstructions.DecimalAdjustA();

            addA(null, registers, ExecutionParameters.DEFAULT);
            daa(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(0x57, registers.AF.Left);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void DecimalAdjustAAfterAdditionWithHalfCarryTest()
        {
            registers.AF.Left = 0x32;
            var addA = ArithmeticInstructions.AddA(AccessorMocks.GenerateReadAccessor<byte>(0x29));
            var daa = DecimalInstructions.DecimalAdjustA();

            addA(null, registers, ExecutionParameters.DEFAULT);
            daa(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(0x61, registers.AF.Left);
            AssertUtils.AssertFlags(registers, false, false, false, false);
        }

        [TestMethod()]
        public void DecimalAdjustAAfterAdditionWithCarryTest()
        {
            registers.AF.Left = 0x32;
            var addA = ArithmeticInstructions.AddA(AccessorMocks.GenerateReadAccessor<byte>(0x81));
            var daa = DecimalInstructions.DecimalAdjustA();

            addA(null, registers, ExecutionParameters.DEFAULT);
            daa(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(0x13, registers.AF.Left);
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        [TestMethod()]
        public void DecimalAdjustAAfterAdditionWithCarryAndHalfCarryTest()
        {
            registers.AF.Left = 0x32;
            var addA = ArithmeticInstructions.AddA(AccessorMocks.GenerateReadAccessor<byte>(0x89));
            var daa = DecimalInstructions.DecimalAdjustA();

            addA(null, registers, ExecutionParameters.DEFAULT);
            daa(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(0x21, registers.AF.Left);
            AssertUtils.AssertFlags(registers, false, false, false, true);
        }

        [TestMethod()]
        public void DecimalAdjustAAfterAdditionWithValueThatOutputsZeroTest()
        {
            registers.AF.Left = 0x32;
            var addA = ArithmeticInstructions.AddA(AccessorMocks.GenerateReadAccessor<byte>(0x68));
            var daa = DecimalInstructions.DecimalAdjustA();

            addA(null, registers, ExecutionParameters.DEFAULT);
            daa(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(0x0, registers.AF.Left);
            AssertUtils.AssertFlags(registers, true, false, false, true);
        }

        #endregion

        #region DAA after subtraction

        [TestMethod()]
        public void DecimalAdjustAAfterSubtractionWithoutHalfBorrowTest()
        {
            registers.AF.Left = 0x67;
            var subA = ArithmeticInstructions.SubA(AccessorMocks.GenerateReadAccessor<byte>(0x32));
            var daa = DecimalInstructions.DecimalAdjustA();

            subA(null, registers, ExecutionParameters.DEFAULT);
            daa(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(0x35, registers.AF.Left);
            AssertUtils.AssertFlags(registers, false, true, false, false);
        }

        [TestMethod()]
        public void DecimalAdjustAAfterSubtractionWithHalfBorrowTest()
        {
            registers.AF.Left = 0x67;
            var subA = ArithmeticInstructions.SubA(AccessorMocks.GenerateReadAccessor<byte>(0x38));
            var daa = DecimalInstructions.DecimalAdjustA();

            subA(null, registers, ExecutionParameters.DEFAULT);
            daa(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(0x29, registers.AF.Left);
            AssertUtils.AssertFlags(registers, false, true, false, false);
        }

        [TestMethod()]
        public void DecimalAdjustAAfterSubtractionWithBorrowTest()
        {
            registers.AF.Left = 0x67;
            var subA = ArithmeticInstructions.SubA(AccessorMocks.GenerateReadAccessor<byte>(0x85));
            var daa = DecimalInstructions.DecimalAdjustA();

            subA(null, registers, ExecutionParameters.DEFAULT);
            daa(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(0x82, registers.AF.Left);
            AssertUtils.AssertFlags(registers, false, true, false, false);
        }

        [TestMethod()]
        public void DecimalAdjustAAfterSubtractionWithBorrowAndHalfBorrowTest()
        {
            registers.AF.Left = 0x67;
            var subA = ArithmeticInstructions.SubA(AccessorMocks.GenerateReadAccessor<byte>(0x89));
            var daa = DecimalInstructions.DecimalAdjustA();

            subA(null, registers, ExecutionParameters.DEFAULT);
            daa(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(0x78, registers.AF.Left);
            AssertUtils.AssertFlags(registers, false, true, false, false);
        }

        [TestMethod()]
        public void DecimalAdjustAAfterSubtractionWithValueThatOutputsZeroTest()
        {
            registers.AF.Left = 0x67;
            var subA = ArithmeticInstructions.SubA(AccessorMocks.GenerateReadAccessor<byte>(0x67));
            var daa = DecimalInstructions.DecimalAdjustA();

            subA(null, registers, ExecutionParameters.DEFAULT);
            daa(null, registers, ExecutionParameters.DEFAULT);
            Assert.AreEqual(0x0, registers.AF.Left);
            AssertUtils.AssertFlags(registers, true, true, false, false);
        }

        #endregion
    }
}