﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.VirtualComponents.Auxiliary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace GameBoyEmu.Emu.VirtualComponents
{
    [TestClass()]
    public class RAMTests
    {
        Registers registers;
        RAM ram;

        [TestInitialize()]
        public void Setup()
        {
            registers = new Registers();

            registers.PC.Full = 0x100;
            registers.AF.Full = 0x1200;
            registers.BC.Full = 0x5678;
            registers.DE.Full = 0x9ABC;
            registers.HL.Full = 0xDEF1;
            registers.SP.Full = 0xFFF4;
            ram = new RAM();
        }

        #region EnableDynamic

        [TestMethod()]
        public void EnableDynamicWithMBC1Test()
        {
            ICartridgeHeader header = Substitute.For<ICartridgeHeader>();
            header.CartridgeType.Returns(CartridgeType.MBC1);
            ram.EnableDynamic(header);
            Assert.AreEqual(CartridgeType.MBC1, ram.CartridgeMemoryType);

            header = Substitute.For<ICartridgeHeader>();
            header.CartridgeType.Returns(CartridgeType.MBC1_RAM);
            ram.EnableDynamic(header);
            Assert.AreEqual(CartridgeType.MBC1, ram.CartridgeMemoryType);

            header = Substitute.For<ICartridgeHeader>();
            header.CartridgeType.Returns(CartridgeType.MBC1_RAM_BATTERY);
            ram.EnableDynamic(header);
            Assert.AreEqual(CartridgeType.MBC1, ram.CartridgeMemoryType);
        }

        [TestMethod()]
        public void EnableDynamicWithMBC3Test()
        {
            ICartridgeHeader header = Substitute.For<ICartridgeHeader>();
            header.CartridgeType.Returns(CartridgeType.MBC3);
            ram.EnableDynamic(header);
            Assert.AreEqual(CartridgeType.MBC3, ram.CartridgeMemoryType);

            header = Substitute.For<ICartridgeHeader>();
            header.CartridgeType.Returns(CartridgeType.MBC3_RAM);
            ram.EnableDynamic(header);
            Assert.AreEqual(CartridgeType.MBC3, ram.CartridgeMemoryType);

            header = Substitute.For<ICartridgeHeader>();
            header.CartridgeType.Returns(CartridgeType.MBC3_RAM_BATTERY);
            ram.EnableDynamic(header);
            Assert.AreEqual(CartridgeType.MBC3, ram.CartridgeMemoryType);

            header = Substitute.For<ICartridgeHeader>();
            header.CartridgeType.Returns(CartridgeType.MBC3_TIMER_BATTERY);
            ram.EnableDynamic(header);
            Assert.AreEqual(CartridgeType.MBC3, ram.CartridgeMemoryType);

            header = Substitute.For<ICartridgeHeader>();
            header.CartridgeType.Returns(CartridgeType.MBC3_TIMER_RAM_BATTERY);
            ram.EnableDynamic(header);
            Assert.AreEqual(CartridgeType.MBC3, ram.CartridgeMemoryType);
        }

        #endregion
    }
}