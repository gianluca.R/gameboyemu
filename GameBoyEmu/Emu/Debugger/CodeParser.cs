﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Instructions;
using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.Utils;
using GameBoyEmu.Emu.VirtualComponents;
using System;
using System.Collections.Generic;

namespace GameBoyEmu.Emu.Debugger
{
    static class CodeParser
    {
        static InstructionSet instructionSet = new InstructionSet(); // This contains (almost) everything we need to parse instructions

        public static SortedList<UInt16, ParsedInstruction> ParseLines(RAM ram, Registers registers, UInt16 start)
        {
            SortedList<UInt16, ParsedInstruction> lines = new SortedList<UInt16, ParsedInstruction>();

            Registers fakeRegisters = new Registers(); // We'll use these to simulate code inside the debugger
            fakeRegisters.PC.Full = start;
            fakeRegisters.AF.Full = registers.AF.Full;
            fakeRegisters.BC.Full = registers.BC.Full;
            fakeRegisters.DE.Full = registers.DE.Full;
            fakeRegisters.HL.Full = registers.HL.Full;
            fakeRegisters.SP.Full = registers.SP.Full;

            bool hasLooped = false;
            while (!hasLooped || fakeRegisters.PC.Full < start)
            {
                UInt16 pos = fakeRegisters.PC.Full;
                byte opCode = ram.ReadByte(fakeRegisters.PC.Full, true);
                fakeRegisters.PC.Full++;

                string hex = HexUtils.NumberToHex(opCode, 2);
                string address = HexUtils.NumberToHex(pos, 4);

                InstructionDescriptor original;
                if (opCode == InstructionSet.ExtendedOpCode)
                {
                    if (fakeRegisters.PC.Full >= 0xFFFF) // TODO: avoid repetition
                    {
                        fakeRegisters.PC.Full = 0;
                        hasLooped = true;
                        continue;
                    }
                    opCode = ram.ReadByte(fakeRegisters.PC.Full, true);
                    instructionSet.extendedInstructionSet.TryGetValue(opCode, out original);
                    fakeRegisters.PC.Full++;
                }
                else
                {
                    instructionSet.instructionSet.TryGetValue(opCode, out original);
                }

                if (original != null)
                {
                    InstructionOutput output;
                    try
                    {
                        output = original.action?.Invoke(ram, fakeRegisters, ExecutionParameters.DEBUGGER);
                    }
                    catch // I'll get an exception if I go out of memory bounds, meaning we have to loop
                    {
                        fakeRegisters.PC.Full = 0;
                        hasLooped = true;
                        continue;
                    }

                    UInt16 bytesReadValue = 0;
                    for (int rawByteRead = pos + 1; rawByteRead < fakeRegisters.PC.Full; rawByteRead++)
                    {
                        byte directByteRead = ram.ReadByte(rawByteRead, true);
                        hex += " " + HexUtils.NumberToHex(directByteRead, 2);
                        bytesReadValue |= (UInt16)(directByteRead << (8 * (rawByteRead - (pos + 1))));
                    }

                    string dataRead = null;
                    string dataWrite = null;
                    string filledName = original.name;
                    if (output != null)
                    {
                        filledName = filledName.Replace("nn", HexUtils.NumberToHex(bytesReadValue, 4));
                        filledName = filledName.Replace("sn", HexUtils.NumberToHex((SByte)bytesReadValue, 2));
                        filledName = filledName.Replace("n", HexUtils.NumberToHex(bytesReadValue, 2));

                        if (output.readAccessor != null)
                        {
                            if (output.readAddress != null)
                            {
                                dataRead = output.readAccessor.Value.GetFriendlyName() + "@[" + HexUtils.NumberToHex(output.readAddress.Value, 4) + "]: " + HexUtils.NumberToHex(output.readValue, 4);
                            }
                            else
                            {
                                dataRead = output.readAccessor.Value.GetFriendlyName() + ": " + HexUtils.NumberToHex(output.readValue, 4);
                            }
                        }

                        if (output.writeAccessor != null)
                        {
                            if (output.writeAddress != null)
                            {
                                dataWrite += output.writeAccessor.Value.GetFriendlyName() + "@[" + HexUtils.NumberToHex(output.writeAddress.Value, 4) + "]: " + HexUtils.NumberToHex(output.writeValue.Value, 4);
                            }
                            else
                            {
                                dataWrite += output.writeAccessor.Value.GetFriendlyName() + ": " + HexUtils.NumberToHex(output.writeValue.Value, 4);
                            }
                        }
                    }
                    string comment = "";

                    lines.Add(pos, new ParsedInstruction(address, filledName, hex, dataRead, dataWrite, comment, pos, original));
                }
                else
                {
                    lines.Add(pos, new ParsedInstruction(address, "UNKNOWN", hex, "", "", "Unknown instruction", pos, null));
                }
                if (fakeRegisters.PC.Full < pos) // Overflowed!
                {
                    fakeRegisters.PC.Full = 0;
                    hasLooped = true;
                }
            }

            return lines;
        }

    }
}
