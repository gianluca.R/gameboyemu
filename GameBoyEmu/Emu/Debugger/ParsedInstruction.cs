﻿using GameBoyEmu.Emu.Instructions;
using System;

namespace GameBoyEmu.Emu.Debugger
{
    public class ParsedInstruction
    {
        public string Address { get; set; }
        public string Translated { get; set; }
        public string Hex { get; set; }
        public string DataRead { get; set; }
        public string DataWrite { get; set; }
        public string Comment { get; set; }
        public UInt16 RawAddress { get; set; }

        public InstructionDescriptor OriginalInstruction { get; set; }

        public ParsedInstruction(string address, string translated, string hex, string dataRead, string dataWrite, string comment, UInt16 rawAddress, InstructionDescriptor originalInstruction)
        {
            this.Address = address;
            this.Translated = translated;
            this.Hex = hex;
            this.DataRead = dataRead;
            this.DataWrite = dataWrite;
            this.Comment = comment;
            this.OriginalInstruction = originalInstruction;
            this.RawAddress = rawAddress;
        }
    }
}
