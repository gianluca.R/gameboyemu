﻿using GameBoyEmu.Emu.Utils;
using GameBoyEmu.Emu.VirtualComponents;
using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using PixelFormat = System.Windows.Media.PixelFormat;

namespace GameBoyEmu.Emu.Rendering
{
    class Renderer
    {
        private GameBoy GB;
        private byte[] videoBuffer;
        private bool[] pixelLocked;
        private int[] bgColor;

        const int ScreenWidth = 160;
        const int ScreenHeight = 144;

        private byte[] oamData;
        private VRAMSnapshot VRamSnapshot;

        private int windowSkippedLines = 0;
        public bool GBCMode = false;

        public Renderer(GameBoy gb)
        {
            this.GB = gb;
            ResetFrame();
        }

        public void ResetFrame()
        {
            videoBuffer = new byte[ScreenWidth * ScreenHeight * 3];
            pixelLocked = new bool[ScreenWidth * ScreenHeight];
            bgColor = new int[ScreenWidth * ScreenHeight];
            windowSkippedLines = 0;
        }

        public void updateOAM(byte[] oam)
        {
            oamData = oam;
        }

        public void updateVRAM(VRAMSnapshot vramSnapshot)
        {
            this.VRamSnapshot = vramSnapshot;
        }

        public BitmapSource PrintBuffer()
        {
            BitmapSource bms = CreateImage(ScreenWidth, ScreenHeight, videoBuffer);
            return bms;
        }

        public void RenderScanline(int line)
        {
            bool spriteAlwaysPriority = false;
            if (!GB.Gpu.Display.IsBGEnabled && GBCMode)
            {
                spriteAlwaysPriority = true;
            }

            if (GB.Gpu.Display.IsBGEnabled || GBCMode) { RenderBG(line); }
            if (GB.Gpu.Display.IsWindowEnabled) { RenderWindow(line); }
            if (GB.Gpu.Display.AreSpritesEnabled) { RenderSprites(line, spriteAlwaysPriority); }
        }

        public void RenderBG(int line)
        {
            int scrollX = GB.Gpu.Display.SCX;
            int scrollY = GB.Gpu.Display.SCY;
            int tileDataId = GB.Gpu.Display.UseAlternateTileData ? 0 : 1;
            if (GB.Gpu.Display.UseAlternateTileData) { tileDataId = 0; }

            // BG MAP
            int bgMapId = 0;
            if (GB.Gpu.Display.UseBGAlternateMap) { bgMapId = 1; }
            for (int screenX = 0; screenX < ScreenWidth; screenX++)
            {
                // line and screenX are relative to Screen, while through Scroll we want to point at the real coordinates in the BG map
                int bgTranslatedX = screenX + scrollX;
                int bgTranslatedY = line + scrollY;

                // BG will wrap if it overflows after scrolling
                if (bgTranslatedX >= 256) { bgTranslatedX -= 256; }
                if (bgTranslatedY >= 256) { bgTranslatedY -= 256; }

                RenderBGTileAtCoords(videoBuffer, bgTranslatedX, bgTranslatedY, bgMapId, tileDataId, screenX, line, ScreenWidth, false, false, true);
            }
        }

        public void RenderWindow(int line)
        {
            int windowX = GB.Gpu.Display.WX - 7;
            int windowY = GB.Gpu.Display.WY; // TODO: while windowX can be changed dynamically, wY can not, which means it should be saved somewhere before a new render starts

            if (windowX < 0 || windowX > 166 || windowY < 0 || windowY > 143)
            {
                windowSkippedLines++;
                return;
            } // Without increasing windowLine

            int translatedWindowY = line - windowY - windowSkippedLines;
            if (translatedWindowY < 0 || translatedWindowY >= ScreenHeight) { return; }

            // BG MAP
            int bgMapId = 0;
            if (GB.Gpu.Display.UseWindowAlternateMap) { bgMapId = 1; }
            for (int screenX = 0; screenX < ScreenWidth; screenX++)
            {
                int translatedWindowX = screenX - windowX;
                if (translatedWindowX < 0 || translatedWindowX >= ScreenWidth) { continue; }

                RenderBGTileAtCoords(videoBuffer, translatedWindowX, translatedWindowY, bgMapId, 1, screenX, line, ScreenWidth, false, false, true);
            }
        }

        public void RenderSprites(int line, bool overridePriority)
        {
            // Due to how priority works, and since we're rendering already in right order, we just need to make sure we don't draw a pixel twice
            List<int> alreadyWrittenPixels = new List<int>();
            bool bigSprites = GB.Gpu.Display.BigSprites;
            int spriteComplexiveHeight = bigSprites ? 16 : 8;

            for (int sprite = 0; sprite < 40; sprite++)
            {
                int spriteArrayOffset = sprite * 4;

                int spriteY = oamData[spriteArrayOffset + 0] - 16;
                if (spriteY + spriteComplexiveHeight <= line || spriteY > line || spriteY + spriteComplexiveHeight <= 0 || spriteY >= 160) { continue; }

                int spriteX = oamData[spriteArrayOffset + 1] - 8;
                if (spriteX + 8 <= 0 || spriteX > ScreenWidth || spriteX >= 168) { continue; }

                byte spriteAttributes = oamData[spriteArrayOffset + 3];
                bool drawBehindBG = BitPosition.GetBit(spriteAttributes, 7);
                bool spriteFlipX = BitPosition.GetBit(spriteAttributes, 5);
                bool spriteFlipY = BitPosition.GetBit(spriteAttributes, 6);
                int paletteId = BitPosition.GetBit(spriteAttributes, 4) ? 1 : 0;
                int vramBank = 0;
                if (GBCMode)
                {
                    paletteId = spriteAttributes & 0x7;
                    vramBank = BitPosition.GetBit(spriteAttributes, 3) ? 1 : 0;
                }

                int spriteTileNum = oamData[spriteArrayOffset + 2];
                int spriteRelativeY = line - spriteY;
                if (spriteFlipY)
                {
                    spriteRelativeY = (bigSprites ? 15 : 7) - spriteRelativeY;
                }
                if (bigSprites)
                {
                    spriteTileNum &= 0xFE;
                    if (spriteRelativeY >= 8)
                    {
                        spriteTileNum++;
                        spriteRelativeY -= 8;
                    }
                }

                for (int x = Math.Max(0, spriteX); x < Math.Min(spriteX + 8, ScreenWidth); x++)
                {
                    if (alreadyWrittenPixels.Contains(x))
                    {
                        continue;
                    }
                    if (pixelLocked[GetCoordinatesInBuffer(x, line, ScreenWidth, 1)])
                    {
                        drawBehindBG = true;
                    }
                    if (overridePriority)
                    {
                        drawBehindBG = false;
                    }

                    int spriteRelativeX = spriteFlipX ? (x - spriteX) : 7 - (x - spriteX);

                    if (RenderTileAtCoords(videoBuffer, spriteTileNum, spriteRelativeX, spriteRelativeY, vramBank, 0, 1, paletteId, x, line, ScreenWidth, true, drawBehindBG) >= 0)
                    {
                        alreadyWrittenPixels.Add(x);
                    }
                }
            }
        }

        /// <summary>
        /// Deprecated
        /// </summary>
        public BitmapSource RenderTileMap(RAM ram, int vramBank, int paletteCategory, int paletteId)
        {
            int width = 8 * 16;
            int height = 8 * 24;
            byte[] tmpBuffer = new byte[width * height * 3];
            byte[] vramData = ram.ReadBlock(0x8000, 16 * 384, true);

            for (int tileNum = 0; tileNum < 256; tileNum++)
            {
                int tileX = tileNum % 16;
                int tileY = tileNum / 16;

                for (int y = 0; y < 8; y++)
                {
                    for (int x = 0; x < 8; x++)
                    {
                        RenderTileAtCoords(tmpBuffer, tileNum, x, y, vramBank, 0, paletteCategory, paletteId, tileX * 8 + (7 - x), tileY * 8 + y, width, false, false);
                    }
                }
            }
            for (int tileNum = 0; tileNum < 127; tileNum++)
            {
                int tileX = (256 + tileNum) % 16;
                int tileY = (256 + tileNum) / 16;

                for (int y = 0; y < 8; y++)
                {
                    for (int x = 0; x < 8; x++)
                    {
                        RenderTileAtCoords(tmpBuffer, tileNum, x, y, vramBank, 1, 1, 0, tileX * 8 + (7 - x), tileY * 8 + y, width, false, false);
                    }
                }
            }

            BitmapSource bms = CreateImage(width, height, tmpBuffer);
            return bms;
        }

        /// <summary>
        /// Deprecated
        /// </summary>
        public BitmapSource RenderBGMap(int bgMapId, int tileDataId)
        {
            if (VRamSnapshot == null) { return null; }
            byte[] tmpBuffer = new byte[256 * 256 * 3];

            for (int y = 0; y < 256; y++)
            {
                for (int x = 0; x < 256; x++)
                {
                    RenderBGTileAtCoords(tmpBuffer, x, y, bgMapId, tileDataId, x, y, 256, false, false, false);
                }
            }
            return CreateImage(256, 256, tmpBuffer);
        }

        int RenderBGTileAtCoords(
            byte[] buffer,
            int tileMapX, int tileMapY,
            int bgMapId, int tileDataId,
            int targetX, int targetY, int targetWidth,
            bool hasAlpha, bool drawOnlyOverColorZero, bool outputToAuxiliaryBuffers)
        {
            int bgTileY = tileMapY / 8; // Find out which BG tile should we look at
            int bgTileX = tileMapX / 8;

            int tileRelativeY = tileMapY % 8; // Relative coordinates inside the tile that we will render
            int tileRelativeX = 7 - (tileMapX % 8);

            int tileNum;

            if (tileDataId == 0)
            {
                tileNum = VRamSnapshot.GetBGMap(0, bgMapId)[bgTileY * 32 + bgTileX];
            }
            else
            {
                tileNum = (sbyte)VRamSnapshot.GetBGMap(0, bgMapId)[bgTileY * 32 + bgTileX];
            }

            int paletteId = 0;
            int tileBank = 0;
            if (GBCMode)
            {
                byte bgAttributes = VRamSnapshot.GetBGMap(1, bgMapId)[bgTileY * 32 + bgTileX];
                paletteId = bgAttributes & 0x7;
                tileBank = BitPosition.GetBit(bgAttributes, 3) ? 1 : 0;
                if (BitPosition.GetBit(bgAttributes, 5)) // Horizontal flip
                {
                    tileRelativeX = 7 - tileRelativeX;
                }
                if (BitPosition.GetBit(bgAttributes, 6)) // Vertical flip
                {
                    tileRelativeY = 7 - tileRelativeY;
                }
                if (BitPosition.GetBit(bgAttributes, 7) && outputToAuxiliaryBuffers) // Priority over Sprite, except color 0
                {
                    pixelLocked[GetCoordinatesInBuffer(targetX, targetY, targetWidth, 1)] = true;
                }
            }

            int outputColor = RenderTileAtCoords(buffer, tileNum, tileRelativeX, tileRelativeY, tileBank, tileDataId, 0, paletteId, targetX, targetY, targetWidth, hasAlpha, drawOnlyOverColorZero);
            if (outputToAuxiliaryBuffers) { bgColor[GetCoordinatesInBuffer(targetX, targetY, targetWidth, 1)] = outputColor; }

            return outputColor;
        }

        int RenderTileAtCoords(
            byte[] buffer,
            int tileNum, int tileRelativeX, int tileRelativeY,
            int tileBank, int tileDataId, int paletteCategory, int paletteId,
            int targetX, int targetY, int targetWidth,
            bool hasAlpha, bool drawOnlyOverColorZero)
        {
            int tilePosInVRAM;

            if (tileDataId == 0)
            {
                tilePosInVRAM = tileNum * 16;
            }
            else
            {
                tilePosInVRAM = 0x800 + tileNum * 16;
            }

            int vramByteLine = tilePosInVRAM + (tileRelativeY * 2);
            byte vramByte1 = VRamSnapshot.GetTileData(tileBank, tileDataId)[vramByteLine];
            byte vramByte2 = VRamSnapshot.GetTileData(tileBank, tileDataId)[vramByteLine + 1];

            int maskedSubByte1 = vramByte1 & (0x1 << tileRelativeX);
            int maskedSubByte2 = vramByte2 & (0x1 << tileRelativeX);
            int subByte1 = maskedSubByte1 >> tileRelativeX;
            int subByte2 = maskedSubByte2 >> tileRelativeX;

            int subByte = subByte1 | (subByte2 << 1);

            return WritePixelByPalette(buffer, targetX, targetY, targetWidth, paletteCategory, paletteId, subByte, hasAlpha, drawOnlyOverColorZero);
        }

        /// <summary>
        /// Deprecated
        /// </summary>
        private int MapTileCoordinatesToImage(int tileX, int tileY, int tileRelativeX, int tileRelativeY, int tilesPerRow)
        {
            int targetX = tileX * 8 + tileRelativeX;
            int targetY = tileY * 8 + tileRelativeY;

            return (targetY * (8 * tilesPerRow) + targetX) * 3;
        }

        private int GetCoordinatesInBuffer(int x, int y, int width, int depth)
        {
            return (y * width + x) * depth;
        }


        private int WritePixelByPalette(byte[] imgData, int targetX, int targetY, int targetWidth, int paletteCategory, int paletteId, int colorNum, bool hasAlpha, bool drawOnlyOverColorZero)
        {
            var paletteContainer = GBCMode ? VRamSnapshot.GbcPalettes : VRamSnapshot.GbPalettes;
            int startPos = GetCoordinatesInBuffer(targetX, targetY, targetWidth, 3);

            if (drawOnlyOverColorZero)
            {
                int bgColorPos = GetCoordinatesInBuffer(targetX, targetY, targetWidth, 1);
                if (bgColor[bgColorPos] != 0) { return -1; }
            }
            if (colorNum != 0 || !hasAlpha)
            {
                imgData[startPos + 0] = paletteContainer[paletteCategory][paletteId].Colors[colorNum].B;
                imgData[startPos + 1] = paletteContainer[paletteCategory][paletteId].Colors[colorNum].G;
                imgData[startPos + 2] = paletteContainer[paletteCategory][paletteId].Colors[colorNum].R;
                return colorNum;
            }
            return -1;
        }

        private BitmapSource CreateImage(int width, int height, byte[] data)
        {
            PixelFormat pf = PixelFormats.Bgr24;
            int stride = (width * pf.BitsPerPixel + 7) / 8;
            return BitmapSource.Create(width, height, 96, 96, pf, null, data, stride);
        }
    }
}
