﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBoyEmu.Emu.Rendering
{
    public class VRAMSnapshot
    {
        private byte[][][] BGMap { get; set; } // Bank->Id->Data

        private byte[][][] TileData { get; set; } // Bank->Id->Data

        public Palette[][] GbPalettes;
        public Palette[][] GbcPalettes;

        public VRAMSnapshot()
        {
            BGMap = new byte[2][][];
            BGMap[0] = new byte[2][];
            BGMap[1] = new byte[2][];

            TileData = new byte[2][][];
            TileData[0] = new byte[2][];
            TileData[1] = new byte[2][];
        }

        public byte[] GetBGMap(int bank, int num)
        {
            return BGMap[bank][num];
        }
        public byte[] GetTileData(int bank, int num)
        {
            return TileData[bank][num];
        }
        public void SetBGMap(int bank, int num, byte[] data)
        {
            BGMap[bank][num] = data;
        }
        public void SetTileData(int bank, int num, byte[] data)
        {
            TileData[bank][num] = data;
        }
    }
}
