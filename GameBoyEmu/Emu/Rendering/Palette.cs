﻿namespace GameBoyEmu.Emu.Rendering
{
    public struct Color
    {
        public byte R;
        public byte G;
        public byte B;

        public Color(byte r, byte g, byte b)
        {
            this.R = r;
            this.G = g;
            this.B = b;
        }
    }
    public struct Palette
    {
        public Color[] Colors;

        public Palette(params Color[] colors)
        {
            this.Colors = colors;
        }
    }
}
