﻿using NAudio.Wave;

namespace GameBoyEmu.Emu.Audio.SampleProviders
{
    public interface IExtendedSampleProvider : ISampleProvider
    {
        void Reset();
        void Stop();
        void Play();
        void Pause();
        bool IsPlaying { get; }
    }
}
