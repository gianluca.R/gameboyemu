﻿using GameBoyEmu.Emu.Utils;
using NAudio.Wave;

namespace GameBoyEmu.Emu.Audio.SampleProviders
{
    public class CustomWaveGenerator : IExtendedSampleProvider
    {
        // Settings
        public byte[] WaveTable { get; set; }
        public int SampleRate { get; private set; } = 44100;
        public double Frequency { get; set; }
        public double Volume { get; set; }
        public double Clock { get; private set; }
        public bool IsPlaying { get; private set; }

        public int WaveTableIndex { get; private set; } = 0;


        public WaveFormat WaveFormat { get; private set; }

        public CustomWaveGenerator(int waveTableSize)
        {
            WaveFormat = WaveFormat.CreateIeeeFloatWaveFormat(SampleRate, 1);
            WaveTable = new byte[waveTableSize];
        }

        public void Reset()
        {
            Clock = 0;
            WaveTableIndex = 0; // Not sure about this
        }
        public void Play()
        {
            IsPlaying = true;
        }
        public void Pause()
        {
            IsPlaying = false;
        }
        public void Stop()
        {
            IsPlaying = false;
            Reset();
        }

        public int Read(float[] buffer, int offset, int count)
        {
            double clockLimit = SampleRate / (WaveTable.Length * Frequency);
            for (int n = 0; n < count; n++)
            {

                if (!IsPlaying)
                {
                    buffer[n + offset] = 0;
                    continue;
                }

                int val = WaveTable[WaveTableIndex];
                buffer[n + offset] = ((val / 15f) - 0.5f) * (float)Volume;

                Clock += 1.0;

                if (Clock > clockLimit)
                {
                    Clock -= clockLimit;
                    WaveTableIndex++;
                    if (WaveTableIndex >= WaveTable.Length)
                    {
                        WaveTableIndex = 0;
                    }
                }
            }
            return count;
        }
    }
}
