﻿using GameBoyEmu.Emu.VirtualComponents;
using NAudio.Wave;
using System;

namespace GameBoyEmu.Emu.Audio.SampleProviders
{
    public class NoiseSignalGenerator : IExtendedSampleProvider
    {
        // Settings
        public int SampleRate { get; private set; } = 44100;
        public bool IsPlaying { get; private set; }

        // Noise
        private Random rnd = new Random();
        public bool SmallNoiseStep { get; set; }
        public int NoiseTimerDivisor { get; set; }
        public double NoiseClock { get; set; }
        public int ClockShift { get; set; }

        private bool High = true;

        private int ClockTime
        {
            get
            {
                int res = (NoiseTimerDivisor == 0 ? 1 : NoiseTimerDivisor) << ClockShift;
                if (res == 0) res++;
                return res;
            }
        }
        public double TimerStep { get { return (double)SampleRate / (CPU.Frequency / ClockTime); } }


        public WaveFormat WaveFormat { get; private set; }

        public NoiseSignalGenerator()
        {
            WaveFormat = WaveFormat.CreateIeeeFloatWaveFormat(SampleRate, 1);
        }

        public void Reset()
        {
            NoiseClock = 0;
            High = true;
        }
        public void Play()
        {
            IsPlaying = true;
        }
        public void Pause()
        {
            IsPlaying = false;
        }
        public void Stop()
        {
            IsPlaying = false;
            Reset();
        }

        public int Read(float[] buffer, int offset, int count)
        {
            for (int n = 0; n < count; n++)
            {
                if (!IsPlaying)
                {
                    buffer[n + offset] = 0;
                    continue;
                }

                NoiseClock += 1.0;
                if (NoiseClock > TimerStep)
                {
                    NoiseClock -= TimerStep;
                    High = rnd.Next(2) == 1;
                }

                buffer[n + offset] = (High ? 1 : -1);
            }
            return count;
        }
    }
}
