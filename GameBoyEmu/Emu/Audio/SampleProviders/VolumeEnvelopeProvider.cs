﻿using NAudio.Wave;

namespace GameBoyEmu.Emu.Audio.SampleProviders
{
    public class VolumeEnvelopeProvider : IExtendedSampleProvider
    {
        // Settings
        public IExtendedSampleProvider Source { get; set; }
        public int SampleRate { get; private set; } = 44100;
        public float Volume { get; set; }
        public bool IsPlaying { get { return Source.IsPlaying; } }

        // Volume envelope
        public double VolumeEnvelopePhase { get; private set; }
        public int CurrentVolumeEnvelopeNumber { get; private set; }
        public bool VolumeEnvelopeActive { get; private set; }
        public bool VolumeEnvelopeIncrease { get; set; }
        public const double VolumeEnvelopeTime = 1.0 / 64.0;
        public int VolumeEnvelopeCount
        {
            get { return volumeEnvelopeCount; }
            set
            {
                volumeEnvelopeCount = value;
                CurrentVolumeEnvelopeNumber = 0;
            }
        }
        private int volumeEnvelopeCount;


        public WaveFormat WaveFormat { get; private set; }

        public VolumeEnvelopeProvider(IExtendedSampleProvider source)
        {
            this.Source = source;
            WaveFormat = WaveFormat.CreateIeeeFloatWaveFormat(SampleRate, 1);
        }

        public void Reset()
        {
            CurrentVolumeEnvelopeNumber = 0;
            VolumeEnvelopeActive = true;
            VolumeEnvelopePhase = 0;
        }
        public void Pause() { Source.Pause(); }
        public void Stop() { Source.Stop(); }
        public void Play() { Source.Play(); }

        public int Read(float[] buffer, int offset, int count)
        {
            Source.Read(buffer, offset, count);

            for (int n = 0; n < count; n++)
            {
                buffer[n + offset] *= Volume;

                VolumeEnvelopePhase += 1.0 / SampleRate;

                if (VolumeEnvelopeActive && VolumeEnvelopeCount > 0 && VolumeEnvelopePhase > VolumeEnvelopeTime / VolumeEnvelopeCount)
                {
                    VolumeEnvelopePhase -= VolumeEnvelopeTime;
                    if (CurrentVolumeEnvelopeNumber >= VolumeEnvelopeCount)
                    {
                        CurrentVolumeEnvelopeNumber = 0;
                        double delta = 1.0 / 15.0;
                        if (VolumeEnvelopeIncrease)
                        {
                            Volume += (float)delta;
                        }
                        else
                        {
                            Volume -= (float)delta;
                        }
                        if (Volume < 0)
                        {
                            VolumeEnvelopeActive = false;
                            Volume = 0;
                        }
                        else if (Volume > 1)
                        {
                            VolumeEnvelopeActive = false;
                            Volume = 1;
                        }
                    }
                    CurrentVolumeEnvelopeNumber++;
                }
            }
            return count;
        }
    }
}
