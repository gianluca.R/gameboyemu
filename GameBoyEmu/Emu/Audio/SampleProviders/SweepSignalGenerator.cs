﻿using NAudio.Wave;
using System;

namespace GameBoyEmu.Emu.Audio.SampleProviders
{
    public class SweepSignalGenerator : IExtendedSampleProvider
    {
        public enum WaveForm
        {
            Square, Sin, SawTooth, Pluck, Triangle, SmoothedSquare, Noise
        }

        // Settings
        private float[] waveTable;
        private WaveForm waveForm;
        public int SampleRate { get; private set; } = 44100;
        public double Frequency { get; set; }
        public double Phase { get; private set; }
        public bool IsPlaying { get; private set; }

        // Sweep
        public double InitialFrequency { get; set; }
        public double SweepTime { get; set; }
        public int SweepShiftCount { get; set; }
        public bool SweepIncrease { get; set; }
        public double SweepPhase { get; private set; }
        public int CurrentSweepShift { get; private set; }
        private double shadowFrequency;

        // Square
        public double WaveDuty { get; set; } = 0.5;

        public WaveFormat WaveFormat { get; private set; }

        public SweepSignalGenerator(WaveForm waveform)
        {
            WaveFormat = WaveFormat.CreateIeeeFloatWaveFormat(SampleRate, 1);
            waveTable = new float[SampleRate];
            this.waveForm = waveform;
            for (int index = 0; index < SampleRate; ++index)
            {
                switch (waveform)
                {
                    case WaveForm.Triangle:
                        waveTable[index] = (index < (SampleRate / 2)) ? (float)index / (SampleRate / 2) : (float)(SampleRate - index) / (SampleRate / 2);
                        break;
                    case WaveForm.Sin:
                        waveTable[index] = (float)Math.Sin(2 * Math.PI * index / SampleRate);
                        break;
                    case WaveForm.SawTooth:
                        waveTable[index] = (float)index / SampleRate;
                        break;
                    case WaveForm.Pluck:
                        waveTable[index] = (((float)index / SampleRate) + (((float)index < (SampleRate / 2)) ? -1 : 1) / 2);
                        break;
                }
            }
        }

        public void Reset()
        {
            Phase = 0;
            Frequency = InitialFrequency;

            shadowFrequency = InitialFrequency;
            CurrentSweepShift = 0;
            SweepPhase = 0;
        }
        public void Play()
        {
            IsPlaying = true;
        }
        public void Pause()
        {
            IsPlaying = false;
        }
        public void Stop()
        {
            IsPlaying = false;
            Reset();
        }

        public int Read(float[] buffer, int offset, int count)
        {
            for (int n = 0; n < count; n++)
            {
                if (!IsPlaying)
                {
                    buffer[n + offset] = 0;
                    continue;
                }

                int waveTableIndex = (int)Phase % SampleRate;
                if (waveForm == WaveForm.Square)
                {
                    buffer[n + offset] = (waveTableIndex < SampleRate * WaveDuty) ? 1 : -1;
                }
                else if (waveForm==WaveForm.SawTooth)
                {
                    buffer[n + offset] = (waveTableIndex < SampleRate * WaveDuty) 
                        ?
                        ((int)Phase % (int)(SampleRate * WaveDuty)) / (float)(SampleRate * WaveDuty)
                        : 
                        0;
                }
                else if (waveForm == WaveForm.SmoothedSquare)
                {
                    //(A/atan(1/delta))*atan(sin(2*pi*t*f)/delta); A=1
                    double delta = 0.4;
                    buffer[n + offset] = (waveTableIndex < SampleRate * WaveDuty)
                        ?
                        (float)((1.0 / Math.Atan(1.0 / delta)) * Math.Atan(Math.Sin(WaveDuty * 2 * Math.PI * waveTableIndex / SampleRate) / delta))
                        :
                        -1;
                }
                else
                {
                    buffer[n + offset] = waveTable[waveTableIndex];
                }

                Phase += Frequency;
                SweepPhase += 1.0 / SampleRate;

                if (Phase > SampleRate)
                {
                    Phase -= SampleRate;
                }
                StepSweep();
            }
            return count;
        }

        void StepSweep()
        {
            if (SweepTime > 0 && SweepShiftCount > 0 && SweepPhase > SweepTime)
            {
                SweepPhase -= SweepTime;

                double delta = shadowFrequency / Math.Pow(2, SweepShiftCount);
                if (!SweepIncrease)
                {
                    shadowFrequency += delta;
                }
                else
                {
                    shadowFrequency -= delta;
                }
                Frequency = shadowFrequency; // This way I discard any Frequency that was manually altered during our cycle, while still letting it play once

                if (CurrentSweepShift > SweepShiftCount)
                {
                    CurrentSweepShift = 0;
                    Frequency = InitialFrequency;
                    shadowFrequency = InitialFrequency;
                }
                CurrentSweepShift++;
            }
        }
    }
}
