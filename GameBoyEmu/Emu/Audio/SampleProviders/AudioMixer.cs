﻿using NAudio.Wave;

namespace GameBoyEmu.Emu.Audio.SampleProviders
{
    public class AudioMixer : ISampleProvider
    {
        // Settings
        public ISampleProvider[] Sources { get; set; }
        public int SampleRate { get; private set; } = 44100;
        public float LeftVolume { get; set; } = 1;
        public float RightVolume { get; set; } = 1;
        public bool[] SourceToLeft { get; set; }
        public bool[] SourceToRight { get; set; }


        public WaveFormat WaveFormat { get; private set; }

        public AudioMixer(ISampleProvider[] sources)
        {
            this.Sources = sources;
            SourceToLeft = new bool[Sources.Length];
            SourceToRight = new bool[Sources.Length];
            for (int k = 0; k < Sources.Length; k++)
            {
                SourceToLeft[k] = true;
                SourceToRight[k] = true;
            }

            WaveFormat = WaveFormat.CreateIeeeFloatWaveFormat(SampleRate, 2);
        }

        public int Read(float[] buffer, int offset, int count)
        {
            int monoCount = count / 2;
            for (int k = 0; k < Sources.Length; k++)
            {
                float[] sourceBuffer = new float[monoCount];
                int read = Sources[k].Read(sourceBuffer, 0, monoCount);

                for (int n = 0; n < read; n++)
                {
                    if (k == 0)
                    {
                        buffer[n * 2 + offset] = (SourceToLeft[k] ? sourceBuffer[n] * LeftVolume : 0) / Sources.Length;
                        buffer[n * 2 + 1 + offset] = (SourceToRight[k] ? sourceBuffer[n] * RightVolume : 0) / Sources.Length;
                    }
                    else
                    {
                        buffer[n * 2 + offset] += (SourceToLeft[k] ? sourceBuffer[n] * LeftVolume : 0) / Sources.Length;
                        buffer[n * 2 + 1 + offset] +=( SourceToRight[k] ? sourceBuffer[n] * RightVolume : 0) / Sources.Length;
                    }
                }
            }

            return count;
        }
    }
}
