﻿using GameBoyEmu.Emu.Audio.SampleProviders;
using System;

namespace GameBoyEmu.Emu.Audio
{
    public struct ChannelData
    {
        public bool IsOn;
        public bool Enabled;
        public VolumeEnvelopeProvider VolumeEnvelope; // Will output SignalGenerator's output modulated by volume shift
        public byte FrequencyLow;
        public byte FrequencyHigh;
        public bool PlayOnlyOnce;
        public DateTime TimeStarted;
        public double SoundLength;
        public double ReferenceFrequency;
        public double Frequency
        {
            get
            {
                int freq = (FrequencyHigh << 8) | FrequencyLow;
                return ReferenceFrequency / (2048.0 - freq);
            }
        }
    }
}
