﻿using GameBoyEmu.Emu.Instructions;
using GameBoyEmu.Emu.Utils;
using GameBoyEmu.Emu.VirtualComponents;
using System;
using System.Collections.Generic;

namespace GameBoyEmu.Emu.Optimization
{
    class LoopSkipper
    {
        public enum LoopCandidate
        {
            LY, INTERRUPT, DIV, INPUT, DISPLAY_MODE
        }

        class HistoryData
        {
            public UInt16 PC;
            public UInt16 AF;
            public UInt16 BC;
            public UInt16 DE;
            public UInt16 HL;
            public UInt16 SP;

            public Nullable<UInt16> readAddress;
            public Nullable<UInt16> writeAddress;
            public int value;

            public byte opCode;
            public UInt16 pos; // Should need banking info too, but eventually it will filter out

            public override bool Equals(object obj)
            {
                if (!(obj is HistoryData)) return false;
                HistoryData h = (HistoryData)obj;
                return pos == h.pos && opCode == h.opCode && PC == h.PC && AF == h.AF && BC == h.BC && DE == h.DE &&
                    HL == h.HL && SP == h.SP && readAddress == h.readAddress && writeAddress == h.writeAddress && value == h.value;
            }
        }

        const int RepeatsBeforeSkip = 3;
        const int MinSequenceLength = 3;
        const int MaxSequenceLength = 15;
        int MaxHistory { get { return RepeatsBeforeSkip * MaxSequenceLength; } }
        List<HistoryData> history = new List<HistoryData>();


        public void Add(UInt16 pos, InstructionDescriptor instruction, Registers registers, InstructionOutput instructionOutput)
        {
            HistoryData h = new HistoryData();
            h.pos = pos;
            h.PC = registers.PC.Full;
            h.AF = registers.AF.Full;
            h.BC = registers.BC.Full;
            h.DE = registers.DE.Full;
            h.HL = registers.HL.Full;
            h.SP = registers.SP.Full;
            if (instructionOutput != null)
            {
                h.readAddress = instructionOutput.readAddress;
                h.writeAddress = instructionOutput.writeAddress;
                h.value = instructionOutput.readValue;
            }
            history.Insert(0, h);
            if (history.Count > MaxHistory)
            {
                history.RemoveAt(history.Count - 1);
            }
        }

        public HashSet<LoopCandidate> Check()
        {
            if (history.Count < MaxHistory) { return null; }

            HistoryData firstElement = history[0];
            int nextPos = history.FindIndex(1, e => e.pos == firstElement.pos);
            if (nextPos == -1 || nextPos < MinSequenceLength || nextPos > MaxHistory / RepeatsBeforeSkip) { return null; }
            int delta = nextPos;

            HashSet<LoopCandidate> candidates = new HashSet<LoopCandidate>();

            for (int k = 0; k < nextPos; k++)
            {
                for (int repeats = 1; repeats < RepeatsBeforeSkip; repeats++)
                {
                    if (!history[k].Equals(history[k + delta * repeats])) { return null; }
                    if (history[k].readAddress == FixedAddresses.LCD_LY_REGISTER) { candidates.Add(LoopCandidate.LY); }
                    if (history[k].readAddress == FixedAddresses.DIV_REGISTER) { candidates.Add(LoopCandidate.DIV); }
                    if (history[k].readAddress == FixedAddresses.LCDC_STATUS_REGISTER) { candidates.Add(LoopCandidate.DISPLAY_MODE); }
                    if (history[k].readAddress == FixedAddresses.JOYPAD_REGISTER) { candidates.Add(LoopCandidate.INPUT); }
                }
            }

            // IF no cause was found by reading addresses, it might just wait an interrupt
            if (candidates.Count == 0)
            {
                candidates.Add(LoopCandidate.INTERRUPT);
            }

            return candidates;
        }
    }
}
