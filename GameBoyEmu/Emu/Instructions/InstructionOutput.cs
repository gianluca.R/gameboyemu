﻿using GameBoyEmu.Emu.Enums;
using System;

namespace GameBoyEmu.Emu.Instructions
{
    public class InstructionOutput
    {
        public UInt16? readAddress = null;
        public UInt16? writeAddress = null;
        public Accessor? readAccessor = null;
        public Accessor? writeAccessor = null;
        public int readValue;
        public int? writeValue;

        public InstructionOutput() { }
        public InstructionOutput(int readValue, int? writeValue, UInt16? readAddress, UInt16? writeAddress, Accessor? readAccessor, Accessor? writeAccessor)
        {
            this.readValue = readValue;
            this.readAddress = readAddress;
            this.writeAddress = writeAddress;
            this.readAccessor = readAccessor;
            this.writeAccessor = writeAccessor;
            if (writeAccessor != null)
            {
                this.writeValue = writeValue;
            }
        }

        public static InstructionOutput FromAccessorToken(AccessorToken<byte> a)
        {
            return new InstructionOutput(a.readValue, a.writeValue, a.readAddress, a.writeAddress, a.readAccessor, a.writeAccessor);
        }
        public static InstructionOutput FromAccessorToken(AccessorToken<UInt16> a)
        {
            return new InstructionOutput(a.readValue, a.writeValue, a.readAddress, a.writeAddress, a.readAccessor, a.writeAccessor);
        }
    }
}
