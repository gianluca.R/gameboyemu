﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.Utils;
using GameBoyEmu.Emu.VirtualComponents;
using System;

namespace GameBoyEmu.Emu.Instructions.Subsets
{
    public class ArithmeticInstructions
    {
        public static InstructionDescriptor.Action XorA(InstructionAccessors.ReadAccessor<byte> valueGetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                accessorToken.writeAccessor = Accessor.A;
                accessorToken.writeValue = (byte)(registers.AF.Left ^ accessorToken.readValue);

                RegisterHandler.SetA(registers, parameters, accessorToken.writeValue);
                RegisterHandler.SetFlags(
                    registers, parameters,
                    accessorToken.writeValue == 0, false, false, false);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action AddA(InstructionAccessors.ReadAccessor<byte> valueGetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                accessorToken.writeAccessor = Accessor.A;
                accessorToken.writeValue = (byte)(registers.AF.Left + accessorToken.readValue);

                byte oldA = registers.AF.Left;
                RegisterHandler.SetA(registers, parameters, accessorToken.writeValue);
                RegisterHandler.SetFlags(
                    registers, parameters,
                    accessorToken.writeValue == 0,
                    false,
                    ((accessorToken.readValue & 0xF) + (oldA & 0xF)) > 0xF, // Carry from bit 3
                    ((accessorToken.readValue & 0xFF) + (oldA & 0xFF)) > 0xFF); // Carry from bit 7

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action AddHL(InstructionAccessors.ReadAccessor<UInt16> valueGetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                accessorToken.writeAccessor = Accessor.HL;
                accessorToken.writeValue = (UInt16)(registers.HL.Full + accessorToken.readValue);

                UInt16 oldHL = registers.HL.Full;
                RegisterHandler.SetHL(registers, parameters, accessorToken.writeValue);
                RegisterHandler.SetFlags(
                    registers, parameters,
                    null,
                    false,
                    ((accessorToken.readValue & 0xFFF) + (oldHL & 0xFFF)) > 0xFFF, // Carry from bit 11
                    ((accessorToken.readValue & 0xFFFF) + (oldHL & 0xFFFF)) > 0xFFFF); //Carry from bit 15

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action AddCarryA(InstructionAccessors.ReadAccessor<byte> valueGetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                byte flag = (byte)(registers.flagC ? 1 : 0);
                accessorToken.writeAccessor = Accessor.A;
                accessorToken.writeValue = (byte)(registers.AF.Left + accessorToken.readValue + flag);

                byte oldA = registers.AF.Left;
                RegisterHandler.SetA(registers, parameters, accessorToken.writeValue);
                RegisterHandler.SetFlags(
                    registers, parameters,
                    accessorToken.writeValue == 0,
                    false,
                    ((accessorToken.readValue & 0xF) + (oldA & 0xF) + flag) > 0xF, // Carry from bit 3
                    ((accessorToken.readValue & 0xFF) + (oldA & 0xFF) + flag) > 0xFF); // Carry from bit 7

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action SubA(InstructionAccessors.ReadAccessor<byte> valueGetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                accessorToken.writeAccessor = Accessor.A;
                accessorToken.writeValue = (byte)(registers.AF.Left - accessorToken.readValue);
                byte oldA = registers.AF.Left;

                RegisterHandler.SetA(registers, parameters, accessorToken.writeValue);
                RegisterHandler.SetFlags(
                    registers, parameters,
                    accessorToken.writeValue == 0,
                    true,
                    (oldA & 0xF) < (accessorToken.readValue & 0xF), // Borrow from bit 4
                    oldA < accessorToken.readValue); // Set if any borrow

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action SubCarryA(InstructionAccessors.ReadAccessor<byte> valueGetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                byte flag = (byte)(registers.flagC ? 1 : 0);
                accessorToken.writeAccessor = Accessor.A;
                accessorToken.writeValue = (byte)(registers.AF.Left - accessorToken.readValue - flag);

                byte oldA = registers.AF.Left;
                RegisterHandler.SetA(registers, parameters, accessorToken.writeValue);
                RegisterHandler.SetFlags(
                    registers, parameters,
                    accessorToken.writeValue == 0,
                    true,
                    (oldA & 0xF) < (accessorToken.readValue & 0xF) + flag, // Borrow from bit 4
                    oldA < (accessorToken.readValue + flag)); // Set if any borrow

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action Inc(InstructionAccessors.ReadAccessor<byte> valueGetter, InstructionAccessors.WriteAccessor<byte> valueSetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                byte oldValue = accessorToken.readValue;
                accessorToken.writeValue = (byte)(accessorToken.readValue + 1);

                valueSetter(ram, registers, parameters, accessorToken);
                RegisterHandler.SetFlags(
                    registers, parameters,
                    accessorToken.writeValue == 0,
                    false,
                    (oldValue & 0xF) + 1 > 0xF, // Set if carry from bit 3
                    null);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action Inc(InstructionAccessors.ReadAccessor<UInt16> valueGetter, InstructionAccessors.WriteAccessor<UInt16> valueSetter)
        {
            return (RAM _ram, Registers _registers, ExecutionParameters instructionParameters) =>
            {
                var accessorToken = valueGetter(_ram, _registers, instructionParameters);
                accessorToken.writeValue = (UInt16)(accessorToken.readValue + 1);

                valueSetter(_ram, _registers, instructionParameters, accessorToken);
                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action Dec(InstructionAccessors.ReadAccessor<byte> valueGetter, InstructionAccessors.WriteAccessor<byte> valueSetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                byte oldValue = accessorToken.readValue;
                accessorToken.writeValue = (byte)(accessorToken.readValue - 1);

                valueSetter(ram, registers, parameters, accessorToken);
                RegisterHandler.SetFlags(
                    registers, parameters,
                    accessorToken.writeValue == 0,
                    true,
                    (oldValue & 0xF) < 1, // Set if borrow from bit 4
                    null);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action Dec(InstructionAccessors.ReadAccessor<UInt16> valueGetter, InstructionAccessors.WriteAccessor<UInt16> valueSetter)
        {
            return (RAM _ram, Registers _registers, ExecutionParameters instructionParameters) =>
            {
                var accessorToken = valueGetter(_ram, _registers, instructionParameters);
                accessorToken.writeValue = (UInt16)(accessorToken.readValue - 1);

                valueSetter(_ram, _registers, instructionParameters, accessorToken);
                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action Complement()
        {
            return (ram, registers, parameters) =>
            {
                byte oldA = registers.AF.Left;
                RegisterHandler.SetA(registers, parameters, (byte)~registers.AF.Left);
                RegisterHandler.SetFlagN(registers, parameters, true);
                RegisterHandler.SetFlagH(registers, parameters, true);
                return new InstructionOutput(oldA, registers.AF.Left, null, null, Accessor.A, Accessor.A);
            };
        }

        public static void Map(InstructionSet instructionSet)
        {
            MapIncDec(instructionSet);
            MapBitwise(instructionSet);
            MapAddSub(instructionSet);
        }

        private static void MapIncDec(InstructionSet instructionSet)
        {
            instructionSet.AddSeriesIncremental(
                0x04,
                0x08,
                "INC %a",
                a => Inc(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.sortedByteAccessors,
                AccessorMapping.accessorCostsBase4ExtraMemory);

            instructionSet.AddSeriesIncremental(
                0x03,
                0x10,
                "INC %a",
                a => Inc(InstructionAccessors.ReadShortFromAccessor(a), InstructionAccessors.WriteShortToAccessor(a)),
                AccessorMapping.sortedShortAccessors,
                AccessorMapping.accessorCostsBase4);

            instructionSet.AddSeriesIncremental(
                0x05,
                0x08,
                "DEC %a",
                a => Dec(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.sortedByteAccessors,
                AccessorMapping.accessorCostsBase4ExtraMemory);

            instructionSet.AddSeriesIncremental(
                0x0B,
                0x10,
                "DEC %a",
                a => Dec(InstructionAccessors.ReadShortFromAccessor(a), InstructionAccessors.WriteShortToAccessor(a)),
                AccessorMapping.sortedShortAccessors,
                AccessorMapping.accessorCostsBase4);
        }

        private static void MapBitwise(InstructionSet instructionSet)
        {
            instructionSet.AddSeries(
                0xA0,
                "XOR %a",
                a => XorA(InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(),
                AccessorMapping.accessorCostsBase4);

            instructionSet.Add(0x2F, "CPL", 4, Complement());
        }

        private static void MapAddSub(InstructionSet instructionSet)
        {
            instructionSet.AddSeries(
                0x80,
                "ADD A, %a",
                a => AddA(InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMap(),
                AccessorMapping.accessorCostsBase4);
            instructionSet.AddSeriesIncremental(
                0x09,
                0x10,
                "ADD HL, %a",
                a => AddHL(InstructionAccessors.ReadShortFromAccessor(a)),
                AccessorMapping.sortedShortAccessors,
                AccessorMapping.accessorCostsBase4);


            instructionSet.AddSeries(
                0x80,
                "ADC A, %a",
                a => AddCarryA(InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(),
                AccessorMapping.accessorCostsBase4);


            instructionSet.AddSeries(
                0x90,
                "SUB A, %a",
                a => SubA(InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMap(),
                AccessorMapping.accessorCostsBase4);
            instructionSet.AddSeries(
                0x90,
                "SBC A, %a",
                a => SubCarryA(InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(),
                AccessorMapping.accessorCostsBase4);

            // This instruction is one of a kind, since summing n and SP is handled by an accessor, this is pretty much just a load.
            instructionSet.Add(0xE8, "ADD SP, n", 16, LoadInstructions.Load(InstructionAccessors.ReadShortFromAccessor(Accessor.signedNPlusSP), InstructionAccessors.WriteShortToAccessor(Accessor.SP)));
        }
    }
}
