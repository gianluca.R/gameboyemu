﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Utils;
using System;

namespace GameBoyEmu.Emu.Instructions.Subsets
{
    public class JumpInstructions
    {
        public static InstructionDescriptor.Action Jump(InstructionAccessors.FlagAccessor condition, InstructionAccessors.ReadAccessor<UInt16> valueGetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                if ((condition == null || condition(ram, registers, parameters)))
                {
                    RegisterHandler.SetPC(registers, parameters, accessorToken.readValue);
                }
                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }

        public static void Map(InstructionSet instructionSet)
        {
            MapJumpInstructions(instructionSet);
            MapRelativeJumpInstructions(instructionSet);
        }
        public static void MapJumpInstructions(InstructionSet instructionSet)
        {
            instructionSet.Add(0xC3, "JP nn", 12, Jump(null, InstructionAccessors.ReadShortFromAccessor(Accessor.nn)));
            instructionSet.AddSeriesIncremental(
                0xC2,
                0x08,
                "JP %a, nn",
                a => Jump(InstructionAccessors.IsFlagSet(a), InstructionAccessors.ReadShortFromAccessor(Accessor.nn)),
                AccessorMapping.sorterJumpFlags,
                AccessorMapping.accessorCostsBase12);

            instructionSet.Add(0xE9, "JP (HL)", 4, Jump(null, InstructionAccessors.ReadShortFromAccessor(Accessor.HL)));
        }

        public static void MapRelativeJumpInstructions(InstructionSet instructionSet)
        {
            instructionSet.Add(0x18, "JR PC+n", 8, Jump(null, InstructionAccessors.ReadShortFromAccessor(Accessor.signedNPlusPC)));
            instructionSet.AddSeriesIncremental(
                0x20,
                0x08,
                "JR %a, PC+n",
                a => Jump(InstructionAccessors.IsFlagSet(a), InstructionAccessors.ReadShortFromAccessor(Accessor.signedNPlusPC)),
                AccessorMapping.sorterJumpFlags,
                AccessorMapping.accessorCostsBase8);
        }
    }
}
