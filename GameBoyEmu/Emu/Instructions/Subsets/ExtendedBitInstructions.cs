﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Utils;

namespace GameBoyEmu.Emu.Instructions.Subsets
{
    public class ExtendedBitInstructions
    {
        public static InstructionDescriptor.Action Reset(int bit, InstructionAccessors.ReadAccessor<byte> valueGetter, InstructionAccessors.WriteAccessor<byte> valueSetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                accessorToken.writeValue = BitPosition.SetBit(accessorToken.readValue, bit, false);

                valueSetter(ram, registers, parameters, accessorToken);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action Set(int bit, InstructionAccessors.ReadAccessor<byte> valueGetter, InstructionAccessors.WriteAccessor<byte> valueSetter)
        {
            return (_ram, _registers, instructionParameters) =>
            {
                var accessorToken = valueGetter(_ram, _registers, instructionParameters);
                accessorToken.writeValue = BitPosition.SetBit(accessorToken.readValue, bit, true);

                valueSetter(_ram, _registers, instructionParameters, accessorToken);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action Test(int bit, InstructionAccessors.ReadAccessor<byte> valueGetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                RegisterHandler.SetFlags(registers, parameters, !BitPosition.GetBit(accessorToken.readValue, bit), false, true, null);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action Swap(InstructionAccessors.ReadAccessor<byte> valueGetter, InstructionAccessors.WriteAccessor<byte> valueSetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                byte leftNibble = (byte)((accessorToken.readValue & 0xF) << 4);
                byte rightNibble = (byte)(((accessorToken.readValue & 0xF0) >> 4) & 0xF);
                accessorToken.writeValue = (byte)(leftNibble | rightNibble);

                valueSetter(ram, registers, parameters, accessorToken);
                RegisterHandler.SetFlags(registers, parameters, accessorToken.writeValue == 0, false, false, false);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action RotateRight(InstructionAccessors.ReadAccessor<byte> valueGetter, InstructionAccessors.WriteAccessor<byte> valueSetter, bool alwaysClearZ)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                bool carry = BitPosition.GetBit(accessorToken.readValue, 0);
                accessorToken.writeValue = BitPosition.SetBit((byte)(accessorToken.readValue >> 1), 7, carry);

                valueSetter(ram, registers, parameters, accessorToken);
                RegisterHandler.SetFlags(registers, parameters, accessorToken.writeValue == 0 && !alwaysClearZ, false, false, carry);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action RotateRightThroughCarry(InstructionAccessors.ReadAccessor<byte> valueGetter, InstructionAccessors.WriteAccessor<byte> valueSetter, bool alwaysClearZ)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                accessorToken.writeValue = BitPosition.SetBit((byte)(accessorToken.readValue >> 1), 7, registers.flagC);

                valueSetter(ram, registers, parameters, accessorToken);
                RegisterHandler.SetFlags(
                    registers, parameters,
                    accessorToken.writeValue == 0 && !alwaysClearZ,
                    false,
                    false,
                    BitPosition.GetBit(accessorToken.readValue, 0));

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action RotateLeft(InstructionAccessors.ReadAccessor<byte> valueGetter, InstructionAccessors.WriteAccessor<byte> valueSetter, bool alwaysClearZ)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                bool carry = BitPosition.GetBit(accessorToken.readValue, 7);
                accessorToken.writeValue = BitPosition.SetBit((byte)(accessorToken.readValue << 1), 0, carry);

                valueSetter(ram, registers, parameters, accessorToken);
                RegisterHandler.SetFlags(registers, parameters, accessorToken.writeValue == 0 && !alwaysClearZ, false, false, carry);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action RotateLeftThroughCarry(InstructionAccessors.ReadAccessor<byte> valueGetter, InstructionAccessors.WriteAccessor<byte> valueSetter, bool alwaysClearZ)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                accessorToken.writeValue = BitPosition.SetBit((byte)(accessorToken.readValue << 1), 0, registers.flagC);

                valueSetter(ram, registers, parameters, accessorToken);
                RegisterHandler.SetFlags(
                    registers, parameters,
                    accessorToken.writeValue == 0 && !alwaysClearZ,
                    false,
                    false,
                    BitPosition.GetBit(accessorToken.readValue, 7));

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action ShiftLeftArithmetic(InstructionAccessors.ReadAccessor<byte> valueGetter, InstructionAccessors.WriteAccessor<byte> valueSetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                bool carry = BitPosition.GetBit(accessorToken.readValue, 7);
                accessorToken.writeValue = BitPosition.SetBit((byte)(accessorToken.readValue << 1), 0, false);

                valueSetter(ram, registers, parameters, accessorToken);
                RegisterHandler.SetFlags(registers, parameters, accessorToken.writeValue == 0, false, false, carry);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action ShiftRightArithmetic(InstructionAccessors.ReadAccessor<byte> valueGetter, InstructionAccessors.WriteAccessor<byte> valueSetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                bool carry = BitPosition.GetBit(accessorToken.readValue, 0);
                bool oldMSB = BitPosition.GetBit(accessorToken.readValue, 7);
                accessorToken.writeValue = BitPosition.SetBit((byte)(accessorToken.readValue >> 1), 7, oldMSB);

                valueSetter(ram, registers, parameters, accessorToken);
                RegisterHandler.SetFlags(registers, parameters, accessorToken.writeValue == 0, false, false, carry);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action ShiftRightLogic(InstructionAccessors.ReadAccessor<byte> valueGetter, InstructionAccessors.WriteAccessor<byte> valueSetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                bool carry = BitPosition.GetBit(accessorToken.readValue, 0);
                accessorToken.writeValue = BitPosition.SetBit((byte)(accessorToken.readValue >> 1), 7, false);

                valueSetter(ram, registers, parameters, accessorToken);
                RegisterHandler.SetFlags(registers, parameters, accessorToken.writeValue == 0, false, false, carry);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }

        public static void MapExtended(InstructionSet instructionSet)
        {
            instructionSet.AddSeriesExt(
                0x30,
                "SWAP %a",
                a => Swap(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            MapResetInstructions(instructionSet);
            MapSetInstructions(instructionSet);
            MapTestInstructions(instructionSet);
            MapRotateInstructions(instructionSet);
            MapShiftInstructions(instructionSet);
        }

        public static void MapResetInstructions(InstructionSet instructionSet)
        {
            instructionSet.AddSeriesExt(
                0x80,
                "RES 0, %a",
                a => Reset(0, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0x80,
                "RES 1, %a",
                a => Reset(1, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0x90,
                "RES 2, %a",
                a => Reset(2, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0x90,
                "RES 3, %a",
                a => Reset(3, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0xA0,
                "RES 4, %a",
                a => Reset(4, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0xA0,
                "RES 5, %a",
                a => Reset(5, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0xB0,
                "RES 6, %a",
                a => Reset(6, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0xB0,
                "RES 7, %a",
                a => Reset(7, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);
        }

        public static void MapSetInstructions(InstructionSet instructionSet)
        {
            instructionSet.AddSeriesExt(
                0xC0,
                "SET 0, %a",
                a => Set(0, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0xC0,
                "SET 1, %a",
                a => Set(1, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0xD0,
                "SET 2, %a",
                a => Set(2, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0xD0,
                "SET 3, %a",
                a => Set(3, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0xE0,
                "SET 4, %a",
                a => Set(4, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0xE0,
                "SET 5, %a",
                a => Set(5, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0xF0,
                "SET 6, %a",
                a => Set(6, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0xF0,
                "SET 7, %a",
                a => Set(7, InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);
        }

        public static void MapTestInstructions(InstructionSet instructionSet)
        {
            instructionSet.AddSeriesExt(
                0x40,
                "BIT 0, %a",
                a => Test(0, InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0x40,
                "BIT 1, %a",
                a => Test(1, InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0x50,
                "BIT 2, %a",
                a => Test(2, InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0x50,
                "BIT 3, %a",
                a => Test(3, InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0x60,
                "BIT 4, %a",
                a => Test(4, InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0x60,
                "BIT 5, %a",
                a => Test(5, InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0x70,
                "BIT 6, %a",
                a => Test(6, InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.AddSeriesExt(
                0x70,
                "BIT 7, %a",
                a => Test(7, InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);
        }

        public static void MapRotateInstructions(InstructionSet instructionSet)
        {
            instructionSet.AddSeriesExt(
                0x10,
                "RL %a",
                a => RotateLeftThroughCarry(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a), false),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);
            instructionSet.AddSeriesExt(
                0x00,
                "RLC %a",
                a => RotateLeft(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a), false),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);


            instructionSet.AddSeriesExt(
                0x10,
                "RR %a",
                a => RotateRightThroughCarry(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a), false),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);
            instructionSet.AddSeriesExt(
                0x00,
                "RRC %a",
                a => RotateRight(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a), false),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);

            instructionSet.Add(
                0x07, "RLCA", 4,
                RotateLeft(InstructionAccessors.ReadByteFromAccessor(Accessor.A), InstructionAccessors.WriteByteToAccessor(Accessor.A), true));
            instructionSet.Add(
                0x0F, "RRCA", 4,
                RotateRight(InstructionAccessors.ReadByteFromAccessor(Accessor.A), InstructionAccessors.WriteByteToAccessor(Accessor.A), true));

            instructionSet.Add(
                0x17, "RLA", 4,
                RotateLeftThroughCarry(InstructionAccessors.ReadByteFromAccessor(Accessor.A), InstructionAccessors.WriteByteToAccessor(Accessor.A), true));
            instructionSet.Add(
                0x1F, "RRA", 4,
                RotateRightThroughCarry(InstructionAccessors.ReadByteFromAccessor(Accessor.A), InstructionAccessors.WriteByteToAccessor(Accessor.A), true));
        }

        public static void MapShiftInstructions(InstructionSet instructionSet)
        {
            instructionSet.AddSeriesExt(
                0x20,
                "SLA %a",
                a => ShiftLeftArithmetic(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);
            instructionSet.AddSeriesExt(
                0x20,
                "SRA %a",
                a => ShiftRightArithmetic(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);
            instructionSet.AddSeriesExt(
                0x30,
                "SRL %a",
                a => ShiftRightLogic(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase8ExtraMemory);
        }
    }
}
