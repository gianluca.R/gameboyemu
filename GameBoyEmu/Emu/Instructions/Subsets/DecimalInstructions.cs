﻿using GameBoyEmu.Emu.Enums;

namespace GameBoyEmu.Emu.Instructions.Subsets
{
    public class DecimalInstructions
    {
        public static InstructionDescriptor.Action DecimalAdjustA()
        {
            return (ram, registers, parameters) =>
            {
                byte startingValue = registers.AF.Left;
                byte value = startingValue;

                if (registers.flagN)
                {
                    if (registers.flagC)
                    {
                        value -= 0x60;
                    }
                    if (registers.flagH)
                    {
                        value -= 0x6;
                    }
                }
                else
                {
                    if (registers.flagC || value>0x99)
                    {
                        value += 0x60;
                        RegisterHandler.SetFlagC(registers, parameters, true);
                    }
                    if (registers.flagH || (value&0xF)>0x9)
                    {
                        value += 0x6;
                    }
                }

                RegisterHandler.SetA(registers, parameters, value);
                RegisterHandler.SetFlags(registers, parameters, value == 0, null, false, null);
                return new InstructionOutput(startingValue, value, null, null, Accessor.A, Accessor.A);
            };
        }

        public static void Map(InstructionSet instructionSet)
        {
            instructionSet.Add(0x27, "DAA", 4, DecimalAdjustA());
        }
    }
}
