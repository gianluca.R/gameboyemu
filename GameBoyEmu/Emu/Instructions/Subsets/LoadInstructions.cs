﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Utils;
using System;

namespace GameBoyEmu.Emu.Instructions.Subsets
{
    public class LoadInstructions
    {
        public static InstructionDescriptor.Action Load(InstructionAccessors.ReadAccessor<byte> valueGetter, InstructionAccessors.WriteAccessor<byte> valueSetter)
        {
            return (_ram, _registers, instructionParameters) =>
            {
                var accessorToken = valueGetter(_ram, _registers, instructionParameters);
                accessorToken.writeValue = accessorToken.readValue;

                valueSetter(_ram, _registers, instructionParameters, accessorToken);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action Load(InstructionAccessors.ReadAccessor<UInt16> valueGetter, InstructionAccessors.WriteAccessor<UInt16> valueSetter)
        {
            return (_ram, _registers, instructionParameters) =>
            {
                var accessorToken = valueGetter(_ram, _registers, instructionParameters);
                accessorToken.writeValue = accessorToken.readValue;

                valueSetter(_ram, _registers, instructionParameters, accessorToken);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action Pop(InstructionAccessors.WriteAccessor<UInt16> valueSetter)
        {
            return (_ram, _registers, instructionParameters) =>
            {
                UInt16 value = InstructionAccessors.PopStack(_ram, _registers, instructionParameters);
                AccessorToken<UInt16> accessorToken = new AccessorToken<UInt16>(value);
                accessorToken.writeValue = accessorToken.readValue;

                valueSetter(_ram, _registers, instructionParameters, accessorToken);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action Push(InstructionAccessors.ReadAccessor<UInt16> valueGetter)
        {
            return (_ram, _registers, instructionParameters) =>
            {
                var accessorToken = valueGetter(_ram, _registers, instructionParameters);
                InstructionAccessors.PushStack(_ram, _registers, accessorToken.readValue, instructionParameters);
                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }


        public static void Map(InstructionSet instructionSet)
        {
            MapLoadBase(instructionSet);
            MapLoadVariable(instructionSet);
            MapPopPush(instructionSet);
        }

        public static void MapLoadBase(InstructionSet instructionSet)
        {
            instructionSet.AddSeries(
                0x70,
                "LD A, %a",
                a => Load(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(Accessor.A)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase4);

            instructionSet.AddSeries(
                0x40,
                "LD B, %a",
                a => Load(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(Accessor.B)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase4);

            instructionSet.AddSeries(
                0x40,
                "LD C, %a",
                a => Load(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(Accessor.C)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase4);

            instructionSet.AddSeries(
                0x50,
                "LD D, %a",
                a => Load(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(Accessor.D)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase4);

            instructionSet.AddSeries(
                0x50,
                "LD E, %a",
                a => Load(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(Accessor.E)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase4);

            instructionSet.AddSeries(
                0x60,
                "LD H, %a",
                a => Load(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(Accessor.H)),
                AccessorMapping.getAccessorMap(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase4);

            instructionSet.AddSeries(
                0x60,
                "LD L, %a",
                a => Load(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(Accessor.L)),
                AccessorMapping.getAccessorMapAlternate(AccessorMapping.defaultReducedAccessorList),
                AccessorMapping.accessorCostsBase4);

            instructionSet.AddSeries(
                0x70,
                "LD (HL), %a",
                a => Load(InstructionAccessors.ReadByteFromAccessor(a), InstructionAccessors.WriteByteToAccessor(Accessor.atHL)),
                AccessorMapping.getAccessorMap(new[] { Accessor.A, Accessor.B, Accessor.C, Accessor.D, Accessor.E, Accessor.H, Accessor.L }),
                AccessorMapping.accessorCostsBase8);
        }

        public static void MapLoadVariable(InstructionSet instructionSet)
        {
            instructionSet.AddSeriesIncremental(
                0x06,
                0x08,
                "LD %a, n",
                a => Load(InstructionAccessors.ReadByteFromAccessor(Accessor.n), InstructionAccessors.WriteByteToAccessor(a)),
                AccessorMapping.sortedByteAccessors,
                AccessorMapping.accessorCostsBase8);

            instructionSet.AddSeriesIncremental(
                0x01,
                0x10,
                "LD %a, nn",
                a => Load(InstructionAccessors.ReadShortFromAccessor(Accessor.nn), InstructionAccessors.WriteShortToAccessor(a)),
                AccessorMapping.sortedShortAccessors,
                AccessorMapping.accessorCostsBase8);

            instructionSet.Add(0xEA, "LD (nn), A", 16, Load(InstructionAccessors.ReadByteFromAccessor(Accessor.A), InstructionAccessors.WriteByteToAccessor(Accessor.atNN)));
            instructionSet.Add(0xFA, "LD A, (nn)", 16, Load(InstructionAccessors.ReadByteFromAccessor(Accessor.atNN), InstructionAccessors.WriteByteToAccessor(Accessor.A)));

            instructionSet.Add(0xE0, "LD (FF00+n), A", 12, Load(InstructionAccessors.ReadByteFromAccessor(Accessor.A), InstructionAccessors.WriteByteToAccessor(Accessor.atNPlusIO)));
            instructionSet.Add(0xF0, "LD A, (FF00+n)", 12, Load(InstructionAccessors.ReadByteFromAccessor(Accessor.atNPlusIO), InstructionAccessors.WriteByteToAccessor(Accessor.A)));

            instructionSet.Add(0x22, "LD (HLI), A", 8, Load(InstructionAccessors.ReadByteFromAccessor(Accessor.A), InstructionAccessors.WriteByteToAccessor(Accessor.atHLI)));
            instructionSet.Add(0x2A, "LD A, (HLI)", 8, Load(InstructionAccessors.ReadByteFromAccessor(Accessor.atHLI), InstructionAccessors.WriteByteToAccessor(Accessor.A)));

            instructionSet.Add(0x32, "LD (HLD), A", 8, Load(InstructionAccessors.ReadByteFromAccessor(Accessor.A), InstructionAccessors.WriteByteToAccessor(Accessor.atHLD)));
            instructionSet.Add(0x3A, "LD A, (HLD)", 8, Load(InstructionAccessors.ReadByteFromAccessor(Accessor.atHLD), InstructionAccessors.WriteByteToAccessor(Accessor.A)));

            instructionSet.Add(0xE2, "LD (FF00+C), A", 8, Load(InstructionAccessors.ReadByteFromAccessor(Accessor.A), InstructionAccessors.WriteByteToAccessor(Accessor.atCPlusIO)));
            instructionSet.Add(0xF2, "LD A, (FF00+C)", 8, Load(InstructionAccessors.ReadByteFromAccessor(Accessor.atCPlusIO), InstructionAccessors.WriteByteToAccessor(Accessor.A)));

            instructionSet.Add(0x0A, "LD A, (BC)", 8, Load(InstructionAccessors.ReadByteFromAccessor(Accessor.atBC), InstructionAccessors.WriteByteToAccessor(Accessor.A)));
            instructionSet.Add(0x1A, "LD A, (DE)", 8, Load(InstructionAccessors.ReadByteFromAccessor(Accessor.atDE), InstructionAccessors.WriteByteToAccessor(Accessor.A)));
            instructionSet.Add(0x02, "LD (BC), A", 8, Load(InstructionAccessors.ReadByteFromAccessor(Accessor.A), InstructionAccessors.WriteByteToAccessor(Accessor.atBC)));
            instructionSet.Add(0x12, "LD (DE), A", 8, Load(InstructionAccessors.ReadByteFromAccessor(Accessor.A), InstructionAccessors.WriteByteToAccessor(Accessor.atDE)));

            instructionSet.Add(0xF8, "LD HL, SP+n", 12, Load(InstructionAccessors.ReadShortFromAccessor(Accessor.signedNPlusSP), InstructionAccessors.WriteShortToAccessor(Accessor.HL)));

            instructionSet.Add(0xF9, "LD SP, HL", 8, Load(InstructionAccessors.ReadShortFromAccessor(Accessor.HL), InstructionAccessors.WriteShortToAccessor(Accessor.SP)));

            instructionSet.Add(0x08, "LD (nn), SP", 20, Load(InstructionAccessors.ReadShortFromAccessor(Accessor.SP), InstructionAccessors.WriteShortToAccessor(Accessor.atNN)));
        }

        public static void MapPopPush(InstructionSet instructionSet)
        {
            instructionSet.AddSeriesIncremental(
                0xC1,
                0x10,
                "POP %a",
                a => Pop(InstructionAccessors.WriteShortToAccessor(a)),
                new[] { Accessor.BC, Accessor.DE, Accessor.HL, Accessor.AF },
                AccessorMapping.accessorCostsBase8);

            instructionSet.AddSeriesIncremental(
                0xC5,
                0x10,
                "PUSH %a",
                a => Push(InstructionAccessors.ReadShortFromAccessor(a)),
                new[] { Accessor.BC, Accessor.DE, Accessor.HL, Accessor.AF },
                AccessorMapping.accessorCostsBase12);
        }
    }
}
