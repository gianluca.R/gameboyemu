﻿using GameBoyEmu.Emu.Utils;
using System;

namespace GameBoyEmu.Emu.Instructions.Subsets
{
    public class ControlInstructions
    {
        public static InstructionDescriptor.Action SetIME(bool value)
        {
            return (ram, registers, parameters) =>
            {
                RegisterHandler.SetIME(registers, parameters, value);
                return null;
            };
        }
        public static InstructionDescriptor.Action ComplementCarryFlag()
        {
            return (ram, registers, parameters) =>
            {
                RegisterHandler.SetFlags(registers, parameters, null, false, false, !registers.flagC);
                return null;
            };
        }
        public static InstructionDescriptor.Action SetCarryFlag()
        {
            return (ram, registers, parameters) =>
            {
                RegisterHandler.SetFlags(registers, parameters, null, false, false, true);
                return null;
            };
        }
        public static InstructionDescriptor.Action Halt()
        {
            return (ram, registers, parameters) =>
            {
                byte IE = ram.ReadByte(FixedAddresses.INTERRUPT_ENABLE_REGISTER, true);
                if (registers.IME && IE != 0)
                {
                    RegisterHandler.SetHalted(registers, parameters, true);
                }
                return null;
            };
        }
        public static InstructionDescriptor.Action Stop()
        {
            return (ram, registers, parameters) =>
            {
                byte seq = InstructionAccessors.ReadByteAndIncreasePC(ram, registers, parameters);
                if (seq != 0x00 && !parameters.ReadOnlyMode) // In read only mode this might just be a wrongfully read instruction
                {
                    throw new Exception("STOP is always supposed to be 10 00");
                }
                byte IE = ram.ReadByte(FixedAddresses.INTERRUPT_ENABLE_REGISTER, true);
                if (registers.IME && IE != 0)
                {
                    // TODO: this is clearly not the right way to handle a stop: it should be halt+turn off lcd until a button is pressed
                    RegisterHandler.SetHalted(registers, parameters, true);
                }
                return null;
            };
        }


        public static void Map(InstructionSet instructionSet)
        {
            instructionSet.Add(0x00, "NOP", 4, null);
            instructionSet.Add(0xF3, "DI", 4, SetIME(false));
            instructionSet.Add(0xFB, "EI", 4, SetIME(true));

            instructionSet.Add(0x3F, "CCF", 4, ComplementCarryFlag());
            instructionSet.Add(0x37, "SCF", 4, SetCarryFlag());

            instructionSet.Add(0x76, "HALT", 4, Halt());
            instructionSet.Add(0x10, "STOP", 4, Stop());
        }
    }
}
