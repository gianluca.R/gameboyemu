﻿using GameBoyEmu.Emu.Utils;

namespace GameBoyEmu.Emu.Instructions.Subsets
{
    public class CompareInstructions
    {
        public static InstructionDescriptor.Action CompareAccumulator(InstructionAccessors.ReadAccessor<byte> valueGetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);

                RegisterHandler.SetFlags(
                    registers, parameters,
                    accessorToken.readValue == registers.AF.Left,
                    true,
                    (registers.AF.Left & 0xF) < (accessorToken.readValue & 0xF), // Set if borrow from bit 4
                     registers.AF.Left < accessorToken.readValue);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }


        public static void Map(InstructionSet instructionSet)
        {
            instructionSet.AddSeries(
                0xB0,
                "CP %a",
                a => CompareAccumulator(InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMapAlternate(),
                AccessorMapping.accessorCostsBase4);
        }
    }
}
