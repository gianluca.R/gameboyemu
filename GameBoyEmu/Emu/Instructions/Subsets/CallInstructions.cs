﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Utils;
using System;

namespace GameBoyEmu.Emu.Instructions.Subsets
{
    public class CallInstructions
    {
        public static InstructionDescriptor.Action Call(InstructionAccessors.FlagAccessor condition, InstructionAccessors.ReadAccessor<UInt16> valueGetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                if (condition == null || condition(ram, registers, parameters))
                {
                    InstructionAccessors.PushStack(ram, registers, registers.PC.Full, parameters);
                    RegisterHandler.SetPC(registers, parameters, accessorToken.readValue);
                }
                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action Return(InstructionAccessors.FlagAccessor condition)
        {
            return (ram, registers, parameters) =>
            {
                if ((condition == null || condition(ram, registers, parameters)))
                {
                    RegisterHandler.SetPC(registers, parameters, InstructionAccessors.PopStack(ram, registers, parameters));
                }
                return null;
            };
        }
        public static InstructionDescriptor.Action ReturnInterrupt()
        {
            return (ram, registers, parameters) =>
            {
                RegisterHandler.SetPC(registers, parameters, InstructionAccessors.PopStack(ram, registers, parameters));
                RegisterHandler.SetIME(registers, parameters, true);
                return null;
            };
        }
        public static InstructionDescriptor.Action Reset(UInt16 pos)
        {
            return (ram, registers, parameters) =>
            {
                InstructionAccessors.PushStack(ram, registers, registers.PC.Full, parameters);
                RegisterHandler.SetPC(registers, parameters, pos);
                return null;
            };
        }

        public static void Map(InstructionSet instructionSet)
        {
            instructionSet.Add(0xCD, "CALL nn", 12, Call(null, InstructionAccessors.ReadShortFromAccessor(Accessor.nn)));
            instructionSet.AddSeriesIncremental(
                0xC4,
                0x08,
                "CALL %a, nn",
                a => Call(InstructionAccessors.IsFlagSet(a), InstructionAccessors.ReadShortFromAccessor(Accessor.nn)),
                AccessorMapping.sorterJumpFlags,
                AccessorMapping.accessorCostsBase12);

            for (int k = 0; k < 8; k++)
            {
                byte rst = (byte)(k * 0x08);
                byte opCode = (byte)(0xC7 + rst);
                instructionSet.Add(opCode, "RST " + rst.ToString("x"), 32, Reset(rst));
            }

            instructionSet.Add(0xC9, "RET", 8, Return(null));
            instructionSet.AddSeriesIncremental(
                0xC0,
                0x08,
                "RET %a",
                a => Return(InstructionAccessors.IsFlagSet(a)),
                AccessorMapping.sorterJumpFlags,
                AccessorMapping.accessorCostsBase8);

            instructionSet.Add(0xD9, "RETI", 8, ReturnInterrupt());
        }
    }
}
