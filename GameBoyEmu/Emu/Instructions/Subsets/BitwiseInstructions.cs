﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Utils;

namespace GameBoyEmu.Emu.Instructions.Subsets
{
    public class BitwiseInstructions
    {
        public static InstructionDescriptor.Action AndA(InstructionAccessors.ReadAccessor<byte> valueGetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                accessorToken.writeAccessor = Accessor.A;
                accessorToken.writeValue = (byte)(registers.AF.Left & accessorToken.readValue);

                RegisterHandler.SetA(registers, parameters, accessorToken.writeValue);
                RegisterHandler.SetFlags(registers, parameters, accessorToken.writeValue == 0, false, true, false);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }
        public static InstructionDescriptor.Action OrA(InstructionAccessors.ReadAccessor<byte> valueGetter)
        {
            return (ram, registers, parameters) =>
            {
                var accessorToken = valueGetter(ram, registers, parameters);
                accessorToken.writeAccessor = Accessor.A;
                accessorToken.writeValue = (byte)(registers.AF.Left | accessorToken.readValue);

                RegisterHandler.SetA(registers, parameters, accessorToken.writeValue);
                RegisterHandler.SetFlags(registers, parameters, accessorToken.writeValue == 0, false, false, false);

                return InstructionOutput.FromAccessorToken(accessorToken);
            };
        }

        public static void Map(InstructionSet instructionSet)
        {
            MapAnd(instructionSet);
            MapOr(instructionSet);
        }

        public static void MapAnd(InstructionSet instructionSet)
        {
            instructionSet.AddSeries(
                0xA0,
                "AND %a",
                a => AndA(InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMap(),
                AccessorMapping.accessorCostsBase4);
        }

        public static void MapOr(InstructionSet instructionSet)
        {
            instructionSet.AddSeries(
                0xB0,
                "OR %a",
                a => OrA(InstructionAccessors.ReadByteFromAccessor(a)),
                AccessorMapping.getAccessorMap(),
                AccessorMapping.accessorCostsBase4);
        }
    }
}
