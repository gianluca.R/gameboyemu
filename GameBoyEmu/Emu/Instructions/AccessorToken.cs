﻿using GameBoyEmu.Emu.Enums;
using System;

namespace GameBoyEmu.Emu.Instructions
{
    public class AccessorToken<T>
    {
        public T readValue;
        public T writeValue;
        public UInt16? readAddress = null;
        public UInt16? writeAddress = null;
        public Accessor? readAccessor = null;
        public Accessor? writeAccessor = null;

        public AccessorToken() { }
        public AccessorToken(T value)
        {
            this.readValue = value;
        }
        public AccessorToken(T value, UInt16? readAddress) : this(value)
        {
            this.readAddress = readAddress;
        }
        public AccessorToken(T value, Accessor? readAccessor) : this(value)
        {
            this.readAccessor = readAccessor;
        }
        public AccessorToken(T value, UInt16? readAddress, Accessor? readAccessor) : this(value)
        {
            this.readAddress = readAddress;
            this.readAccessor = readAccessor;
        }
    }
}
