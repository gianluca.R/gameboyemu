﻿using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;

namespace GameBoyEmu.Emu.Instructions
{
    public class InstructionDescriptor
    {
        public delegate InstructionOutput Action(RAM ram, Registers registers, ExecutionParameters parameters);

        public string name;
        public int clock;
        public Action action;

        public InstructionDescriptor(string name, int clock, Action action)
        {
            this.name = name;
            this.action = action;
            this.clock = clock;
        }
    }
}
