﻿using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;
using System;

namespace GameBoyEmu.Emu.Instructions
{
    public class RegisterHandler
    {
        public static void SetFlags(Registers registers, ExecutionParameters parameters, bool? flagZ, bool? flagN, bool? flagH, bool? flagC)
        {
            if (!parameters.ReadOnlyMode)
            {
                if (flagZ.HasValue)
                {
                    registers.flagZ = flagZ.Value;
                }
                if (flagN.HasValue)
                {
                    registers.flagN = flagN.Value;
                }
                if (flagH.HasValue)
                {
                    registers.flagH = flagH.Value;
                }
                if (flagC.HasValue)
                {
                    registers.flagC = flagC.Value;
                }
            }
        }

        public static void SetFlagZ(Registers registers, ExecutionParameters parameters, bool value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.flagZ = value;
            }
        }
        public static void SetFlagN(Registers registers, ExecutionParameters parameters, bool value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.flagN = value;
            }
        }
        public static void SetFlagH(Registers registers, ExecutionParameters parameters, bool value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.flagH = value;
            }
        }
        public static void SetFlagC(Registers registers, ExecutionParameters parameters, bool value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.flagC = value;
            }
        }

        public static void SetA(Registers registers, ExecutionParameters parameters, byte value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.AF.Left = value;
            }
        }
        public static void SetF(Registers registers, ExecutionParameters parameters, byte value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.AF.Right = value;
            }
        }
        public static void SetB(Registers registers, ExecutionParameters parameters, byte value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.BC.Left = value;
            }
        }
        public static void SetC(Registers registers, ExecutionParameters parameters, byte value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.BC.Right = value;
            }
        }
        public static void SetD(Registers registers, ExecutionParameters parameters, byte value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.DE.Left = value;
            }
        }
        public static void SetE(Registers registers, ExecutionParameters parameters, byte value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.DE.Right = value;
            }
        }
        public static void SetH(Registers registers, ExecutionParameters parameters, byte value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.HL.Left = value;
            }
        }
        public static void SetL(Registers registers, ExecutionParameters parameters, byte value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.HL.Right = value;
            }
        }

        public static void SetAF(Registers registers, ExecutionParameters parameters, UInt16 value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.AF.Full = (UInt16)(value & 0xFFF0); // Lowest 4 bits are unused and can't be set
            }
        }
        public static void SetBC(Registers registers, ExecutionParameters parameters, UInt16 value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.BC.Full = value;
            }
        }
        public static void SetDE(Registers registers, ExecutionParameters parameters, UInt16 value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.DE.Full = value;
            }
        }

        public static void SetHL(Registers registers, ExecutionParameters parameters, UInt16 value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.HL.Full = value;
            }
        }
        public static void IncHL(Registers registers, ExecutionParameters parameters, UInt16 value)
        {
            SetHL(registers, parameters, (UInt16)(registers.HL.Full + value));
        }
        public static void DecHL(Registers registers, ExecutionParameters parameters, UInt16 value)
        {
            SetHL(registers, parameters, (UInt16)(registers.HL.Full - value));
        }
        public static void SetSP(Registers registers, ExecutionParameters parameters, UInt16 value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.SP.Full = value;
            }
        }
        public static void IncSP(Registers registers, ExecutionParameters parameters, UInt16 value)
        {
            SetSP(registers, parameters, (UInt16)(registers.SP.Full + value));
        }
        public static void DecSP(Registers registers, ExecutionParameters parameters, UInt16 value)
        {
            SetSP(registers, parameters, (UInt16)(registers.SP.Full - value));
        }

        public static void SetPC(Registers registers, ExecutionParameters parameters, UInt16 value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.PC.Full = value;
            }
        }

        public static void SetIME(Registers registers, ExecutionParameters parameters, bool value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.IME = value;
            }
        }
        public static void SetHalted(Registers registers, ExecutionParameters parameters, bool value)
        {
            if (!parameters.ReadOnlyMode)
            {
                registers.Halted = value;
            }
        }
    }
}
