﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.Utils;
using GameBoyEmu.Emu.VirtualComponents;
using System;

namespace GameBoyEmu.Emu.Instructions
{
    /// <summary>
    /// Collection of helper methods to generate Actions that can be fed to OperationDescriptors in a legible manner.
    /// </summary>
    public class InstructionAccessors
    {
        public delegate AccessorToken<T> ReadAccessor<T>(RAM ram, Registers registers, ExecutionParameters parameters);
        public delegate bool FlagAccessor(RAM ram, Registers registers, ExecutionParameters parameters);
        public delegate void WriteAccessor<T>(RAM ram, Registers registers, ExecutionParameters parameters, AccessorToken<T> accessorToken);

        /// <summary>
        /// Reads a byte at PC and increase PC.
        /// </summary>
        /// <returns></returns>
        static public byte ReadByteAndIncreasePC(RAM ram, Registers registers, ExecutionParameters parameters)
        {
            return ram.ReadByte(registers.PC.Full++, parameters.BypassLimitsMode);
        }
        /// <summary>
        /// Reads a byte at PC and increase PC.
        /// </summary>
        /// <returns></returns>
        static public SByte ReadSignedByteAndIncreasePC(RAM ram, Registers registers, ExecutionParameters parameters)
        {
            return (SByte)ram.ReadByte(registers.PC.Full++, parameters.BypassLimitsMode);
        }
        /// <summary>
        /// Reads a short at PC and increase PC.
        /// </summary>
        /// <returns></returns>
        static public UInt16 ReadShortAndIncreasePC(RAM ram, Registers registers, ExecutionParameters parameters)
        {
            UInt16 res = ram.ReadShort(registers.PC.Full, parameters.BypassLimitsMode);
            registers.PC.Full += 2;
            return res;
        }

        /// <summary>
        /// Pushes a short value at (SP), then decreases SP by 2
        /// </summary>
        /// <returns></returns>
        static public void PushStack(RAM ram, Registers registers, UInt16 value, ExecutionParameters parameters)
        {
            RegisterHandler.DecSP(registers, parameters, 2);
            if (!parameters.ReadOnlyMode)
            {
                ram.WriteShort(registers.SP.Full, value, parameters.BypassLimitsMode);
            }
        }
        /// <summary>
        /// Pushes a short value at (SP), then decreases SP by 2
        /// </summary>
        /// <returns></returns>
        static public UInt16 PopStack(RAM ram, Registers registers, ExecutionParameters parameters)
        {
            UInt16 value = ram.ReadShort(registers.SP.Full, parameters.BypassLimitsMode);
            RegisterHandler.IncSP(registers, parameters, 2);
            return value;
        }


        /// <summary>
        /// Returns an Action that will read from the specified 8bit accessor.
        /// </summary>
        /// <returns></returns>
        static public ReadAccessor<byte> ReadByteFromAccessor(Accessor register)
        {
            switch (register)
            {
                case Accessor.n:
                    return (ram, registers, parameters) => new AccessorToken<byte>(ReadByteAndIncreasePC(ram, registers, parameters), register);

                case Accessor.A:
                    return (ram, registers, parameters) => new AccessorToken<byte>(registers.AF.Left, register);
                case Accessor.F:
                    return (ram, registers, parameters) => new AccessorToken<byte>(registers.AF.Right, register);

                case Accessor.B:
                    return (ram, registers, parameters) => new AccessorToken<byte>(registers.BC.Left, register);
                case Accessor.C:
                    return (ram, registers, parameters) => new AccessorToken<byte>(registers.BC.Right, register);

                case Accessor.D:
                    return (ram, registers, parameters) => new AccessorToken<byte>(registers.DE.Left, register);
                case Accessor.E:
                    return (ram, registers, parameters) => new AccessorToken<byte>(registers.DE.Right, register);

                case Accessor.H:
                    return (ram, registers, parameters) => new AccessorToken<byte>(registers.HL.Left, register);
                case Accessor.L:
                    return (ram, registers, parameters) => new AccessorToken<byte>(registers.HL.Right, register);

                case Accessor.atBC:
                    return (ram, registers, parameters) => new AccessorToken<byte>(ram.ReadByte(registers.BC.Full, parameters.BypassLimitsMode), registers.BC.Full, register);
                case Accessor.atDE:
                    return (ram, registers, parameters) => new AccessorToken<byte>(ram.ReadByte(registers.DE.Full, parameters.BypassLimitsMode), registers.DE.Full, register);
                case Accessor.atHL:
                    return (ram, registers, parameters) => new AccessorToken<byte>(ram.ReadByte(registers.HL.Full, parameters.BypassLimitsMode), registers.HL.Full, register);
                case Accessor.atNN:
                    return (ram, registers, parameters) =>
                    {
                        UInt16 pos = ReadShortAndIncreasePC(ram, registers, parameters);
                        return new AccessorToken<byte>(ram.ReadByte(pos, parameters.BypassLimitsMode), pos, register);
                    };
                case Accessor.atHLI:
                    return (ram, registers, parameters) =>
                    {
                        UInt16 valueAddress = registers.HL.Full;
                        byte value = ram.ReadByte(valueAddress, parameters.BypassLimitsMode);
                        RegisterHandler.IncHL(registers, parameters, 1);
                        return new AccessorToken<byte>(value, valueAddress, register);
                    };
                case Accessor.atHLD:
                    return (ram, registers, parameters) =>
                    {
                        UInt16 valueAddress = registers.HL.Full;
                        byte value = ram.ReadByte(valueAddress, parameters.BypassLimitsMode);
                        RegisterHandler.DecHL(registers, parameters, 1);
                        return new AccessorToken<byte>(value, valueAddress, register);
                    };
                case Accessor.atNPlusIO:
                    return (ram, registers, parameters) =>
                    {
                        UInt16 pos = (UInt16)(ReadByteAndIncreasePC(ram, registers, parameters) + FixedAddresses.IO_PORTS.Start);
                        return new AccessorToken<byte>(ram.ReadByte(pos, parameters.BypassLimitsMode), pos, register);
                    };
                case Accessor.atCPlusIO:
                    return (ram, registers, parameters) =>
                    {
                        UInt16 pos = (UInt16)(registers.BC.Right + FixedAddresses.IO_PORTS.Start);
                        return new AccessorToken<byte>(ram.ReadByte(pos, parameters.BypassLimitsMode), pos, register);
                    };

                default:
                    throw new Exception("Invalid read byte accessor.");
            }
        }

        /// <summary>
        /// Returns an Action that will read from the specified 16bit accessor.
        /// </summary>
        /// <returns></returns>
        static public ReadAccessor<UInt16> ReadShortFromAccessor(Accessor register)
        {
            switch (register)
            {
                case Accessor.nn:
                    return (ram, registers, parameters) => new AccessorToken<UInt16>(ReadShortAndIncreasePC(ram, registers, parameters), register);
                case Accessor.signedNPlusPC:
                    return (ram, registers, parameters) => new AccessorToken<UInt16>((UInt16)(ReadSignedByteAndIncreasePC(ram, registers, parameters) + registers.PC.Full), register);
                case Accessor.signedNPlusSP: // This is a very special accessor, as executing it will alter flags. This is mostly just a byproduct of how the Z80 handles it, but we emulate it anyway.
                    return (ram, registers, parameters) =>
                    {
                        SByte value = ReadSignedByteAndIncreasePC(ram, registers, parameters);
                        byte unsignedValue = (byte)value;
                        UInt16 SP = registers.SP.Full;
                        RegisterHandler.SetFlags(
                            registers, parameters,
                            false,
                            false,
                            ((unsignedValue & 0xF) + (SP & 0xF)) > 0xF, // Carry from bit 3
                            ((unsignedValue & 0xFF) + (SP & 0xFF)) > 0xFF); // Carry from bit 7
                        return new AccessorToken<UInt16>((UInt16)(value + SP), register);
                    };


                case Accessor.AF:
                    return (ram, registers, parameters) => new AccessorToken<UInt16>(registers.AF.Full, register);
                case Accessor.BC:
                    return (ram, registers, parameters) => new AccessorToken<UInt16>(registers.BC.Full, register);
                case Accessor.DE:
                    return (ram, registers, parameters) => new AccessorToken<UInt16>(registers.DE.Full, register);
                case Accessor.HL:
                    return (ram, registers, parameters) => new AccessorToken<UInt16>(registers.HL.Full, register);
                case Accessor.SP:
                    return (ram, registers, parameters) => new AccessorToken<UInt16>(registers.SP.Full, register);

                case Accessor.atHL:
                    return (ram, registers, parameters) => new AccessorToken<UInt16>(ram.ReadShort(registers.HL.Full, parameters.BypassLimitsMode), registers.HL.Full, register);

                default:
                    throw new Exception("Invalid read short accessor.");
            }
        }

        /// <summary>
        /// Returns an Action that will write a value to the specified 8bit accessor.
        /// </summary>
        /// <returns></returns>
        static public WriteAccessor<byte> WriteByteToAccessor(Accessor register)
        {
            switch (register)
            {
                case Accessor.A:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        RegisterHandler.SetA(registers, parameters, accessorToken.writeValue);
                        accessorToken.writeAccessor = register;
                    };
                case Accessor.F:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        RegisterHandler.SetF(registers, parameters, accessorToken.writeValue);
                        accessorToken.writeAccessor = register;
                    };
                case Accessor.B:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        RegisterHandler.SetB(registers, parameters, accessorToken.writeValue);
                        accessorToken.writeAccessor = register;
                    };
                case Accessor.C:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        RegisterHandler.SetC(registers, parameters, accessorToken.writeValue);
                        accessorToken.writeAccessor = register;
                    };

                case Accessor.D:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        RegisterHandler.SetD(registers, parameters, accessorToken.writeValue);
                        accessorToken.writeAccessor = register;
                    };
                case Accessor.E:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        RegisterHandler.SetE(registers, parameters, accessorToken.writeValue);
                        accessorToken.writeAccessor = register;
                    };

                case Accessor.H:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        RegisterHandler.SetH(registers, parameters, accessorToken.writeValue);
                        accessorToken.writeAccessor = register;
                    };
                case Accessor.L:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        RegisterHandler.SetL(registers, parameters, accessorToken.writeValue);
                        accessorToken.writeAccessor = register;
                    };

                case Accessor.atBC:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        if (!parameters.ReadOnlyMode)
                        {
                            ram.WriteByte(registers.BC.Full, accessorToken.writeValue, parameters.BypassLimitsMode);
                        }
                        accessorToken.writeAddress = registers.BC.Full;
                        accessorToken.writeAccessor = register;
                    };
                case Accessor.atDE:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        if (!parameters.ReadOnlyMode)
                        {
                            ram.WriteByte(registers.DE.Full, accessorToken.writeValue, parameters.BypassLimitsMode);
                        }
                        accessorToken.writeAddress = registers.DE.Full;
                        accessorToken.writeAccessor = register;
                    };
                case Accessor.atHL:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        if (!parameters.ReadOnlyMode)
                        {
                            ram.WriteByte(registers.HL.Full, accessorToken.writeValue, parameters.BypassLimitsMode);
                        }
                        accessorToken.writeAddress = registers.HL.Full;
                        accessorToken.writeAccessor = register;
                    };
                case Accessor.atHLI:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        if (!parameters.ReadOnlyMode)
                        {
                            ram.WriteByte(registers.HL.Full, accessorToken.writeValue, parameters.BypassLimitsMode);
                        }
                        accessorToken.writeAddress = registers.HL.Full;
                        accessorToken.writeAccessor = register;
                        RegisterHandler.IncHL(registers, parameters, 1);
                    };
                case Accessor.atHLD:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        if (!parameters.ReadOnlyMode)
                        {
                            ram.WriteByte(registers.HL.Full, accessorToken.writeValue, parameters.BypassLimitsMode);
                        }
                        accessorToken.writeAddress = registers.HL.Full;
                        accessorToken.writeAccessor = register;
                        RegisterHandler.DecHL(registers, parameters, 1);
                    };
                case Accessor.atNPlusIO:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        UInt16 pos = (UInt16)(ReadByteAndIncreasePC(ram, registers, parameters) + FixedAddresses.IO_PORTS.Start);
                        if (!parameters.ReadOnlyMode)
                        {
                            ram.WriteByte(pos, accessorToken.writeValue, parameters.BypassLimitsMode);
                        }
                        accessorToken.writeAddress = pos;
                        accessorToken.writeAccessor = register;
                    };
                case Accessor.atCPlusIO:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        UInt16 pos = (UInt16)(registers.BC.Right + FixedAddresses.IO_PORTS.Start);
                        if (!parameters.ReadOnlyMode)
                        {
                            ram.WriteByte(pos, accessorToken.writeValue, parameters.BypassLimitsMode);
                        }
                        accessorToken.writeAddress = pos;
                        accessorToken.writeAccessor = register;
                    };
                case Accessor.atNN:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        UInt16 pos = ReadShortAndIncreasePC(ram, registers, parameters);
                        if (!parameters.ReadOnlyMode)
                        {
                            ram.WriteByte(pos, accessorToken.writeValue, parameters.BypassLimitsMode);
                        }
                        accessorToken.writeAddress = pos;
                        accessorToken.writeAccessor = register;
                    };

                default:
                    throw new Exception("Invalid write byte accessor.");
            }
        }

        /// <summary>
        /// Returns an Action that will write a value to the specified 16bit accessor.
        /// </summary>
        /// <returns></returns>
        static public WriteAccessor<UInt16> WriteShortToAccessor(Accessor register)
        {
            switch (register)
            {
                case Accessor.AF:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        RegisterHandler.SetAF(registers, parameters, accessorToken.writeValue);
                        accessorToken.writeAccessor = register;
                    };

                case Accessor.BC:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        RegisterHandler.SetBC(registers, parameters, accessorToken.writeValue);
                        accessorToken.writeAccessor = register;
                    };

                case Accessor.DE:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        RegisterHandler.SetDE(registers, parameters, accessorToken.writeValue);
                        accessorToken.writeAccessor = register;
                    };

                case Accessor.HL:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        RegisterHandler.SetHL(registers, parameters, accessorToken.writeValue);
                        accessorToken.writeAccessor = register;
                    };

                case Accessor.SP:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        RegisterHandler.SetSP(registers, parameters, accessorToken.writeValue);
                        accessorToken.writeAccessor = register;
                    };

                case Accessor.atNN:
                    return (ram, registers, parameters, accessorToken) =>
                    {
                        UInt16 pos = ReadShortAndIncreasePC(ram, registers, parameters);
                        if (!parameters.ReadOnlyMode)
                        {
                            ram.WriteShort(pos, accessorToken.writeValue, false);
                        }
                        accessorToken.writeAddress = pos;
                        accessorToken.writeAccessor = register;
                    };


                default:
                    throw new Exception("Invalid write short accessor");
            }
        }

        /// <summary>
        /// Returns an Action that will write a value to the specified 16bit accessor.
        /// </summary>
        /// <returns></returns>
        static public FlagAccessor IsFlagSet(Accessor register)
        {
            switch (register)
            {
                case Accessor.flagZ:
                    return (ram, registers, parameters) => registers.flagZ;
                case Accessor.flagNZ:
                    return (ram, registers, parameters) => !registers.flagZ;

                case Accessor.flagN:
                    return (ram, registers, parameters) => registers.flagN;
                case Accessor.flagNN:
                    return (ram, registers, parameters) => !registers.flagN;

                case Accessor.flagC:
                    return (ram, registers, parameters) => registers.flagC;
                case Accessor.flagNC:
                    return (ram, registers, parameters) => !registers.flagC;

                case Accessor.flagH:
                    return (ram, registers, parameters) => registers.flagH;
                case Accessor.flagNH:
                    return (ram, registers, parameters) => !registers.flagH;

                default:
                    throw new Exception("Invalid flag accessor");
            }
        }
    }
}
