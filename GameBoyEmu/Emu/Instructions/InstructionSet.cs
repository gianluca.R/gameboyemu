﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Instructions.Subsets;
using GameBoyEmu.Emu.Utils;
using System;
using System.Collections.Generic;

namespace GameBoyEmu.Emu.Instructions
{
    public class InstructionSet
    {
        public readonly Dictionary<byte, InstructionDescriptor> instructionSet = new Dictionary<byte, InstructionDescriptor>();
        public readonly Dictionary<byte, InstructionDescriptor> extendedInstructionSet = new Dictionary<byte, InstructionDescriptor>();

        public const byte ExtendedOpCode = 0xCB;

        public InstructionSet()
        {
            ControlInstructions.Map(this);
            JumpInstructions.Map(this);
            CompareInstructions.Map(this);
            ArithmeticInstructions.Map(this);
            LoadInstructions.Map(this);
            CallInstructions.Map(this);
            ExtendedBitInstructions.MapExtended(this);
            BitwiseInstructions.Map(this);
            DecimalInstructions.Map(this);
        }

        public void Add(byte opCode, string name, int clock, InstructionDescriptor.Action call)
        {
            instructionSet.Add(opCode, new InstructionDescriptor(name, clock, call));
        }
        public void AddExt(byte opCode, string name, int clock, InstructionDescriptor.Action call)
        {
            extendedInstructionSet.Add(opCode, new InstructionDescriptor(name, clock, call));
        }

        public void AddSeries(
            byte baseOpCode,
            string baseName,
            Func<Accessor, InstructionDescriptor.Action> callRetriever,
            IReadOnlyDictionary<Accessor, byte> accessorMap,
            IReadOnlyDictionary<Accessor, int> costMap)
        {
            foreach (var pair in accessorMap)
            {
                Add(
                    (byte)(baseOpCode | pair.Value),
                    baseName.Replace("%a", pair.Key.GetFriendlyName()),
                    costMap[pair.Key],
                    callRetriever(pair.Key)
                );
            }
        }
        public void AddSeriesExt(
            byte baseOpCode,
            string baseName,
            Func<Accessor, InstructionDescriptor.Action> callRetriever,
            IReadOnlyDictionary<Accessor, byte> accessorMap,
            IReadOnlyDictionary<Accessor, int> costMap)
        {
            foreach (var pair in accessorMap)
            {
                AddExt(
                    (byte)(baseOpCode | pair.Value),
                    baseName.Replace("%a", pair.Key.GetFriendlyName()),
                    costMap[pair.Key],
                    callRetriever(pair.Key)
                );
            }
        }

        public void AddSeriesIncremental(
            byte baseOpCode,
            byte increments,
            string baseName,
            Func<Accessor, InstructionDescriptor.Action> callRetriever,
            Accessor[] accessorList,
            IReadOnlyDictionary<Accessor, int> costMap)
        {
            byte toAdd = 0;
            foreach (Accessor a in accessorList)
            {
                Add(
                    (byte)(baseOpCode + toAdd),
                    baseName.Replace("%a", a.GetFriendlyName()),
                    costMap[a],
                    callRetriever(a)
                );
                toAdd += increments;
            }
        }
        public void AddSeriesIncrementalExt(
            byte baseOpCode,
            byte increments,
            string baseName,
            Func<Accessor, InstructionDescriptor.Action> callRetriever,
            Accessor[] accessorList,
            IReadOnlyDictionary<Accessor, int> costMap)
        {
            byte toAdd = 0;
            foreach (Accessor a in accessorList)
            {
                AddExt(
                    (byte)(baseOpCode + toAdd),
                    baseName.Replace("%a", a.GetFriendlyName()),
                    costMap[a],
                    callRetriever(a)
                );
                toAdd += increments;
            }
        }

        public void DumpLog()
        {
            Debug.Log("-----------------------------------------");
            Debug.Log("Instruction set dump");
            Debug.Log("-----------------------------------------");
            foreach (var pair in instructionSet)
            {
                string hex = pair.Key.ToString("x");
                string name = pair.Value.name;
                int clock = pair.Value.clock;
                Debug.Log(name + "\t\t\t\t" + hex + "\t" + clock);
            }
            string extHex = ExtendedOpCode.ToString("x");
            foreach (var pair in extendedInstructionSet)
            {
                string hex = extHex + " " + pair.Key.ToString("x");
                string name = pair.Value.name;
                int clock = pair.Value.clock;
                Debug.Log(name + "\t\t\t\t" + hex + "\t" + clock);
            }
            Debug.Log("-----------------------------------------");
        }
    }
}
