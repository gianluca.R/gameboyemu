﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBoyEmu.Emu.Utils
{
    public class BankingUtils
    {
        public static int AdjustMBC1RomBank(int romBank)
        {
            if (romBank % 0x20 == 0 && romBank <= 0x60) // This is a weird behaviour by MBC1, rom banks 0x20, 0x40 and 0x60 can't be accessed
            {
                romBank++;
            }
            return romBank;
        }
    }
}
