﻿using GameBoyEmu.Emu.Structs;
using System;
using System.Collections.Generic;

namespace GameBoyEmu.Emu.Utils
{
    static class FixedAddresses
    {
        public static readonly Range ROM_BANK_0 = new Range(0x0000, 0x3FFF);
        public static readonly Range ROM_BANK_N = new Range(0x4000, 0x7FFF);
        public static readonly Range VRAM = new Range(0x8000, 0x9FFF);
        public static readonly Range EXT_RAM = new Range(0xA000, 0xBFFF);
        public static readonly Range WRAM0 = new Range(0xC000, 0xCFFF);
        public static readonly Range WRAM1 = new Range(0xD000, 0xDFFF);
        public static readonly Range ECHO = new Range(0xE000, 0xFDFF);
        public static readonly Range OAM = new Range(0xFE00, 0xFE9F);
        public static readonly Range NOT_USABLE = new Range(0xFEA0, 0xFEFF);
        public static readonly Range IO_PORTS = new Range(0xFF00, 0xFF7F);
        public static readonly Range HRAM = new Range(0xFF80, 0xFFFE);


        //Other useful regions and subregions
        public static readonly Range VRAM_TILE_DATA_0 = new Range(0x8000, 0x8FFF);
        public static readonly Range VRAM_TILE_DATA_1 = new Range(0x8800, 0x97FF);
        public static readonly Range VRAM_BG_MAP_0 = new Range(0x9800, 0x9BFF);
        public static readonly Range VRAM_BG_MAP_1 = new Range(0x9C00, 0x9FFF);

        public static readonly Range WRITE_RAM_ENABLE = new Range(0x0, 0x1FFF);
        public static readonly Range WRITE_ROM_BANK_NUMBER = new Range(0x2000, 0x3FFF);
        public static readonly Range WRITE_ROM_BANK_NUMBER_HIGH_BIT = new Range(0x3000, 0x3FFF);
        public static readonly Range WRITE_RAM_BANK_NUMBER = new Range(0x4000, 0x5FFF);
        public static readonly Range WRITE_LATCH_CLOCK_DATA = new Range(0x6000, 0x7FFF);

        //Writer optimization helpers
        public static readonly Range WRITEABLE_ALWAYS_1 = WRAM0;
        public static readonly Range WRITEABLE_ALWAYS_2 = new Range(0xFF00, 0xFFFF);

        public static readonly Range READABLE_ROM = new Range(0x0000, 0x7FFF);
        public static readonly Range READABLE_WRAM = new Range(0xC000, 0xDFFF);
        public static readonly Range READABLE_ALWAYS = new Range(0xFF00, 0xFFFF);

        public const UInt16 ENTRY_POINT = 0x100;

        // Memory registers
        public const UInt16 JOYPAD_REGISTER = 0xFF00;
        public const UInt16 DIV_REGISTER = 0xFF04;
        public const UInt16 TIMER_COUNTER_REGISTER = 0xFF05;
        public const UInt16 TIMER_MODULO_REGISTER = 0xFF06;
        public const UInt16 TIMER_CONTROL_REGISTER = 0xFF07;
        public const UInt16 INTERRUPT_FLAG_REGISTER = 0xFF0F;
        public const UInt16 LCD_CONTROL_REGISTER = 0xFF40;
        public const UInt16 LCDC_STATUS_REGISTER = 0xFF41;
        public const UInt16 SCROLL_Y = 0xFF42;
        public const UInt16 SCROLL_X = 0xFF43;
        public const UInt16 LCD_LY_REGISTER = 0xFF44;
        public const UInt16 LCD_LY_COMPARE_REGISTER = 0xFF45;
        public const UInt16 OAM_DMA_TRANSFER_REGISTER = 0xFF46;
        public const UInt16 BGP_REGISTER = 0xFF47;
        public const UInt16 OBP0_REGISTER = 0xFF48;
        public const UInt16 OBP1_REGISTER = 0xFF49;
        public const UInt16 VRAM_BANK_REGISTER = 0xFF4F;
        public const UInt16 BACKGROUND_PALETTE_INDEX = 0xFF68; //GBC
        public const UInt16 BACKGROUND_PALETTE_DATA = 0xFF69; //GBC
        public const UInt16 SPRITE_PALETTE_INDEX = 0xFF6A; //GBC
        public const UInt16 SPRITE_PALETTE_DATA = 0xFF6B; //GBC
        public const UInt16 WINDOW_Y_REGISTER = 0xFF4A;
        public const UInt16 WINDOW_X_REGISTER = 0xFF4B;
        public const UInt16 GBC_DOUBLE_SPEED_REGISTER = 0xFF4D; //GBC
        public const UInt16 VRAM_DMA_SOURCE_HIGH = 0xFF51; //GBC
        public const UInt16 VRAM_DMA_SOURCE_LOW = 0xFF52; //GBC
        public const UInt16 VRAM_DMA_DESTINATION_HIGH = 0xFF53; //GBC
        public const UInt16 VRAM_DMA_DESTINATION_LOW = 0xFF54; //GBC
        public const UInt16 VRAM_DMA_START_AND_LENGTH = 0xFF55; //GBC
        public const UInt16 WRAM_BANK_SELECT_REGISTER = 0xFF70; //GBC
        public const UInt16 INTERRUPT_ENABLE_REGISTER = 0xFFFF;

        //Audio
        public const UInt16 SOUND_C1_SWEEP_REGISTER = 0xFF10; //NR10
        public const UInt16 SOUND_C1_DUTY_AND_LENGHT = 0xFF11; //NR11
        public const UInt16 SOUND_C1_VOLUME_ENVELOPE = 0xFF12; //NR12
        public const UInt16 SOUND_C1_FREQUENCY_LOW = 0xFF13; //NR13
        public const UInt16 SOUND_C1_FREQUENCY_HIGH_AND_CONTROL = 0xFF14; //NR14

        public const UInt16 SOUND_C2_DUTY_AND_LENGHT = 0xFF16; //NR21
        public const UInt16 SOUND_C2_VOLUME_ENVELOPE = 0xFF17; //NR22
        public const UInt16 SOUND_C2_FREQUENCY_LOW = 0xFF18; //NR23
        public const UInt16 SOUND_C2_FREQUENCY_HIGH_AND_CONTROL = 0xFF19; //NR24

        public const UInt16 SOUND_C3_ON_OFF = 0xFF1A; //NR30
        public const UInt16 SOUND_C3_LENGHT = 0xFF1B; //NR31
        public const UInt16 SOUND_C3_VOLUME = 0xFF1C; //NR32
        public const UInt16 SOUND_C3_FREQUENCY_LOW = 0xFF1D; //NR33
        public const UInt16 SOUND_C3_FREQUENCY_HIGH_AND_CONTROL = 0xFF1E; //NR34

        public const UInt16 SOUND_C3_WAVE_START = 0xFF30;
        public const UInt16 SOUND_C3_WAVE_END = 0xFF3F;

        public const UInt16 SOUND_C4_LENGHT = 0xFF20; //NR41
        public const UInt16 SOUND_C4_VOLUME_ENVELOPE = 0xFF21; //NR42
        public const UInt16 SOUND_C4_POLYNOMIAL_COUNTER = 0xFF22; //NR43
        public const UInt16 SOUND_C4_CONTROL = 0xFF23; //NR44

        public const UInt16 SOUND_CHANNEL_CONTROL_REGISTER = 0xFF24; //NR50
        public const UInt16 SOUND_OUTPUT_REGISTER = 0xFF25; //NR51
        public const UInt16 SOUND_CONTROL_REGISTER = 0xFF26; //NR52

        public static readonly Dictionary<UInt16, bool> AUDIO_REGISTERS = new Dictionary<UInt16, bool>() {
            { SOUND_C1_SWEEP_REGISTER, true },
            { SOUND_C1_DUTY_AND_LENGHT, true },
            { SOUND_C1_VOLUME_ENVELOPE, true },
            { SOUND_C1_FREQUENCY_LOW, true },
            { SOUND_C1_FREQUENCY_HIGH_AND_CONTROL, true },

            { SOUND_C2_DUTY_AND_LENGHT, true },
            { SOUND_C2_VOLUME_ENVELOPE, true },
            { SOUND_C2_FREQUENCY_LOW, true },
            { SOUND_C2_FREQUENCY_HIGH_AND_CONTROL, true },

            { SOUND_C3_ON_OFF, true },
            { SOUND_C3_LENGHT, true },
            { SOUND_C3_VOLUME, true },
            { SOUND_C3_FREQUENCY_LOW, true },
            { SOUND_C3_FREQUENCY_HIGH_AND_CONTROL, true },

            { SOUND_C4_LENGHT, true },
            { SOUND_C4_VOLUME_ENVELOPE, true },
            { SOUND_C4_POLYNOMIAL_COUNTER, true },
            { SOUND_C4_CONTROL, true },

            { SOUND_CHANNEL_CONTROL_REGISTER, true },
            { SOUND_OUTPUT_REGISTER, true },
            { SOUND_CONTROL_REGISTER, true }
        };
    }
}
