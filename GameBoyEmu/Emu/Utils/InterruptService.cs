﻿using GameBoyEmu.Emu.Instructions;
using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.VirtualComponents;
using System;

namespace GameBoyEmu.Emu.Utils
{
    public class InterruptService
    {
        private GameBoy GB;
        public const int VBLANK_INTERRUPT = 0;
        public const int STAT_INTERRUPT = 1;
        public const int TIMER_INTERRUPT = 2;
        public const int SERIAL_INTERRUPT = 3;
        public const int JOYPAD_INTERRUPT = 4;

        public bool[] EnabledInterrupts = new bool[5];
        public bool[] RequestedInterrupts = new bool[5];

        public byte IE
        {
            get
            {
                byte res = 0xE0;
                for (int k = 0; k <= 4; k++)
                {
                    res = BitPosition.SetBit(res, k, EnabledInterrupts[k]);
                }
                return res;
            }
            set
            {
                for (int k = 0; k <= 4; k++)
                {
                    EnabledInterrupts[k] = BitPosition.GetBit(value, k);
                }
            }
        }
        public byte IF
        {
            get
            {
                byte res = 0xE0;
                for (int k = 0; k <= 4; k++)
                {
                    res = BitPosition.SetBit(res, k, RequestedInterrupts[k]);
                }
                return res;
            }
            set
            {
                for (int k = 0; k <= 4; k++)
                {
                    RequestedInterrupts[k] = BitPosition.GetBit(value, k);
                }
            }
        }


        public InterruptService(GameBoy gb)
        {
            this.GB = gb;
            RequestedInterrupts[VBLANK_INTERRUPT] = true;
        }

        public bool HandleInterrupts()
        {
            if (!GB.Registers.IME) { return false; }

            for (byte k = 0; k <= 4; k++)
            {
                if (CanFire(k))
                {
                    PreFire(k);
                    return true;
                }
            }
            return false;
        }

        public bool CanFire(byte intNumber)
        {
            return GB.Registers.IME && EnabledInterrupts[intNumber] && RequestedInterrupts[intNumber];
        }

        private void PreFire(byte intNumber)
        {
            GB.Registers.Halted = false;
            InstructionAccessors.PushStack(GB.Ram, GB.Registers, GB.Registers.PC.Full, ExecutionParameters.CPU);

            if (CanFire(intNumber))
            {
                Fire(intNumber);
            }
            else
            {
                GB.Registers.PC.Full = 0; // Special behaviour when IE is reset by pushing PC onto our stack. This is undocumented behaviour.
            }
            GB.Registers.IME = false;
        }

        private void Fire(byte intNumber)
        {
            RequestedInterrupts[intNumber] = false;
            GB.Registers.PC.Full = (UInt16)(0x40 + (intNumber * 0x8));
        }

        public void RequestInterrupt(byte intNumber)
        {
            RequestedInterrupts[intNumber] = true;
        }
    }
}
