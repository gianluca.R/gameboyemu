﻿namespace GameBoyEmu.Emu.Utils
{
    class HexUtils
    {
        public static string NumberToHex(int num, int zeroCount)
        {
            string hex = num.ToString("x").ToUpper();
            while (hex.Length < zeroCount) { hex = "0" + hex; };
            return hex;
        }
    }
}
