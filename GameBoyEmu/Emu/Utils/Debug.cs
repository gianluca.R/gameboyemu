﻿using System;

namespace GameBoyEmu.Emu.Utils
{
    class Debug
    {
        const int LOGGING = 2; //0 - none, 1 - error, 2 - info, 3 - verbose

        public static void Error(Object msg)
        {
            if (LOGGING >= 1)
            {
                Console.Error.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] " + msg);
            }
        }
        public static void Log(Object msg)
        {
            if (LOGGING >= 2)
            {
                Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] " + msg);
            }
        }
        public static void Verbose(Object msg)
        {
            if (LOGGING >= 3)
            {
                Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] " + msg);
            }
        }
    }
}
