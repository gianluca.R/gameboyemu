﻿namespace GameBoyEmu.Emu.Utils
{
    class BitPosition
    {
        public const byte BIT_0 = 0x1;
        public const byte BIT_1 = 0x2;
        public const byte BIT_2 = 0x4;
        public const byte BIT_3 = 0x8;
        public const byte BIT_4 = 0x10;
        public const byte BIT_5 = 0x20;
        public const byte BIT_6 = 0x40;
        public const byte BIT_7 = 0x80;

        public static bool GetBit(int value, int bit)
        {
            return (value & (0x1 << bit)) != 0;
        }
        public static byte SetBit(byte value, int bit, bool set)
        {
            return (byte)SetBit((int)value, bit, set);
        }
        public static int SetBit(int value, int bit, bool set)
        {
            if (set)
            {
                return value | (0x1 << bit);
            }
            else
            {
                return value & ~(0x1 << bit);
            }
        }
    }
}
