﻿using GameBoyEmu.Emu.Enums;
using System.Collections.Generic;
using System.Linq;

namespace GameBoyEmu.Emu.Utils
{
    /// <summary>
    /// Most of the accessors always map to the same bits in opCodes, and clock times when used. We have a few different variants, which
    /// we save here and reuse everywhere, rather than having to write everything everytime.
    /// It is clear that these lists can be somewhat cumbersome, but since there's a logic behind Z80's opcodes, it makes sense
    /// to use these rather than mapping by hand every single opcode
    /// </summary>
    class AccessorMapping
    {
        public static readonly Accessor[] defaultReducedAccessorList = new[] {
            Accessor.A,
            Accessor.B,
            Accessor.C,
            Accessor.D,
            Accessor.E,
            Accessor.H,
            Accessor.L,
            Accessor.atHL
        };

        /// <summary>
        /// First variant of the accessor to bit mapping
        /// </summary>
        private static readonly IReadOnlyDictionary<Accessor, byte> accessorMap = new Dictionary<Accessor, byte>() {
            {Accessor.A, 0x7},
            {Accessor.B, 0x0},
            {Accessor.C, 0x1},
            {Accessor.D, 0x2},
            {Accessor.E, 0x3},
            {Accessor.H, 0x4},
            {Accessor.L, 0x5},
            {Accessor.atHL, 0x6},
            {Accessor.n, 0x46}
        };

        public static IReadOnlyDictionary<Accessor, byte> getAccessorMap()
        {
            return accessorMap;
        }
        public static IReadOnlyDictionary<Accessor, byte> getAccessorMap(Accessor[] filterAccessors)
        {
            return accessorMap.Where(p => filterAccessors.Contains(p.Key)).ToDictionary(p => p.Key, p => p.Value);
        }

        /// <summary>
        /// Alternate variant of the accessor to bit mapping.
        /// </summary>
        private static readonly IReadOnlyDictionary<Accessor, byte> accessorMapAlternate = new Dictionary<Accessor, byte>() {
            {Accessor.A, 0xF},
            {Accessor.B, 0x8},
            {Accessor.C, 0x9},
            {Accessor.D, 0xA},
            {Accessor.E, 0xB},
            {Accessor.H, 0xC},
            {Accessor.L, 0xD},
            {Accessor.atHL, 0xE},
            {Accessor.n, 0x4E}
        };

        public static IReadOnlyDictionary<Accessor, byte> getAccessorMapAlternate()
        {
            return accessorMapAlternate;
        }
        public static IReadOnlyDictionary<Accessor, byte> getAccessorMapAlternate(Accessor[] filterAccessors)
        {
            return accessorMapAlternate.Where(p => filterAccessors.Contains(p.Key)).ToDictionary(p => p.Key, p => p.Value);
        }

        /// <summary>
        /// Clock cost table for accessors when the base operation costs 4 for registers.
        /// </summary>
        public static readonly IReadOnlyDictionary<Accessor, int> accessorCostsBase4 = new Dictionary<Accessor, int>() {
            {Accessor.A, 4},
            {Accessor.B, 4},
            {Accessor.C, 4},
            {Accessor.D, 4},
            {Accessor.E, 4},
            {Accessor.H, 4},
            {Accessor.L, 4},
            {Accessor.AF, 8},
            {Accessor.BC, 8},
            {Accessor.DE, 8},
            {Accessor.HL, 8},
            {Accessor.SP, 8},
            {Accessor.atHL, 8},
            {Accessor.n, 8},

            {Accessor.flagZ, 4},
            {Accessor.flagNZ, 4},
            {Accessor.flagC, 4},
            {Accessor.flagNC, 4},
            {Accessor.flagN, 4},
            {Accessor.flagNN, 4},
            {Accessor.flagH, 4},
            {Accessor.flagNH, 4}
        };

        /// <summary>
        /// Clock cost table for accessors when the base operation costs 8 for registers.
        /// </summary>
        public static readonly IReadOnlyDictionary<Accessor, int> accessorCostsBase8 = new Dictionary<Accessor, int>() {
            {Accessor.A, 8},
            {Accessor.B, 8},
            {Accessor.C, 8},
            {Accessor.D, 8},
            {Accessor.E, 8},
            {Accessor.H, 8},
            {Accessor.L, 8},
            {Accessor.AF, 12},
            {Accessor.BC, 12},
            {Accessor.DE, 12},
            {Accessor.HL, 12},
            {Accessor.SP, 12},
            {Accessor.atHL, 12},
            {Accessor.n, 12},

            {Accessor.flagZ, 8},
            {Accessor.flagNZ, 8},
            {Accessor.flagC, 8},
            {Accessor.flagNC, 8},
            {Accessor.flagN, 8},
            {Accessor.flagNN, 8},
            {Accessor.flagH, 8},
            {Accessor.flagNH, 8}
        };

        /// <summary>
        /// Clock cost table for accessors when the base operation costs 12 for registers.
        /// </summary>
        public static readonly IReadOnlyDictionary<Accessor, int> accessorCostsBase12 = new Dictionary<Accessor, int>() {
            {Accessor.A, 12},
            {Accessor.B, 12},
            {Accessor.C, 12},
            {Accessor.D, 12},
            {Accessor.E, 12},
            {Accessor.H, 12},
            {Accessor.L, 12},
            {Accessor.AF, 16},
            {Accessor.BC, 16},
            {Accessor.DE, 16},
            {Accessor.HL, 16},
            {Accessor.SP, 16},
            {Accessor.atHL, 16},
            {Accessor.n, 16},

            {Accessor.flagZ, 12},
            {Accessor.flagNZ, 12},
            {Accessor.flagC, 12},
            {Accessor.flagNC, 12},
            {Accessor.flagN, 12},
            {Accessor.flagNN, 12},
            {Accessor.flagH, 12},
            {Accessor.flagNH, 12}
        };

        /// <summary>
        /// Clock cost table for accessors when the base operation costs 4 for registers but has increased cost for memory access.
        /// </summary>
        public static readonly IReadOnlyDictionary<Accessor, int> accessorCostsBase4ExtraMemory = new Dictionary<Accessor, int>() {
            {Accessor.A, 4},
            {Accessor.B, 4},
            {Accessor.C, 4},
            {Accessor.D, 4},
            {Accessor.E, 4},
            {Accessor.H, 4},
            {Accessor.L, 4},
            {Accessor.AF, 8},
            {Accessor.BC, 8},
            {Accessor.DE, 8},
            {Accessor.HL, 8},
            {Accessor.SP, 8},
            {Accessor.atHL, 12},
            {Accessor.n, 12},

            {Accessor.flagZ, 4},
            {Accessor.flagNZ, 4},
            {Accessor.flagC, 4},
            {Accessor.flagNC, 4},
            {Accessor.flagN, 4},
            {Accessor.flagNN, 4},
            {Accessor.flagH, 4},
            {Accessor.flagNH, 4}
        };

        /// <summary>
        /// Clock cost table for accessors when the base operation costs 8 for registers but has increased cost for memory access.
        /// </summary>
        public static readonly IReadOnlyDictionary<Accessor, int> accessorCostsBase8ExtraMemory = new Dictionary<Accessor, int>() {
            {Accessor.A, 8},
            {Accessor.B, 8},
            {Accessor.C, 8},
            {Accessor.D, 8},
            {Accessor.E, 8},
            {Accessor.H, 8},
            {Accessor.L, 8},
            {Accessor.AF, 12},
            {Accessor.BC, 12},
            {Accessor.DE, 12},
            {Accessor.HL, 12},
            {Accessor.SP, 12},
            {Accessor.atHL, 16},
            {Accessor.n, 16},

            {Accessor.flagZ, 8},
            {Accessor.flagNZ, 8},
            {Accessor.flagC, 8},
            {Accessor.flagNC, 8},
            {Accessor.flagN, 8},
            {Accessor.flagNN, 8},
            {Accessor.flagH, 8},
            {Accessor.flagNH, 8}
        };

        /// <summary>
        /// When they don't map to the same bits, accessors map to incremental values: their order is always fixed and used to calculate opCodes
        /// </summary>
        public static readonly Accessor[] sortedByteAccessors = new Accessor[] {
            Accessor.B,
            Accessor.C,
            Accessor.D,
            Accessor.E,
            Accessor.H,
            Accessor.L,
            Accessor.atHL,
            Accessor.A
        };

        public static readonly Accessor[] sortedShortAccessors = new Accessor[] {
            Accessor.BC,
            Accessor.DE,
            Accessor.HL,
            Accessor.SP,
        };

        public static readonly Accessor[] sorterJumpFlags = new Accessor[] {
            Accessor.flagNZ,
            Accessor.flagZ,
            Accessor.flagNC,
            Accessor.flagC,
        };
    }
}
