﻿using System;

namespace GameBoyEmu.Emu.Structs
{
    /// <summary>
    /// Inclusive address range.
    /// </summary>
    struct Range
    {
        public readonly UInt16 Start;
        public readonly UInt16 End;

        public UInt16 Length
        {
            get
            {
                return (UInt16)(End - Start + 1); // +1 since when End = Start, I expect length to be 1: (x,x) means I want only the byte at x
            }
        }

        public Range(UInt16 start, UInt16 end)
        {
            this.Start = start;
            this.End = end;
        }

        public bool Contains(int pos)
        {
            return this.Start <= pos && pos <= this.End;
        }
    }
}
