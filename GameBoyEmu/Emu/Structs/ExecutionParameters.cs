﻿namespace GameBoyEmu.Emu.Structs
{
    /// <summary>
    /// Inclusive address range.
    /// </summary>
    public struct ExecutionParameters
    {
        /// <summary>
        /// In read only mode no changes will be made to registers or ram, except for PC increasing while parsing instructions. It's meant for the debugger,
        /// to allow reading of sequential instructions while being able to display possible outputs.
        /// </summary>
        public readonly bool ReadOnlyMode;
        public readonly bool BypassLimitsMode;

        public ExecutionParameters(bool readOnly, bool bypassLimits)
        {
            this.ReadOnlyMode = readOnly;
            this.BypassLimitsMode = bypassLimits;
        }

        public static ExecutionParameters DEFAULT = new ExecutionParameters(false, false);
        public static ExecutionParameters CPU = new ExecutionParameters(false, true);
        public static ExecutionParameters DEBUGGER = new ExecutionParameters(true, true);
    }
}
