﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Rendering;
using GameBoyEmu.Emu.Utils;
using GameBoyEmu.Emu.VirtualComponents;
using GameBoyEmu.Emu.VirtualComponents.Auxiliary;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace GameBoyEmu
{
    public class GameBoy
    {
        private Task executionTask;
        private System.Threading.CancellationTokenSource cancellationTokenSource;
        private bool terminated = false;
        public Cartridge Cartridge;
        public ICartridgeHeader CartridgeHeader;
        public RAM Ram;
        public CPU Cpu;
        public GPU Gpu;
        public Registers Registers;
        public Soundboard Soundboard;
        public Timer Timer;
        public PaletteSystem PaletteSystem;
        public InterruptService Interrupt;
        public Joypad Joypad;
        public DMA DMA;
        public bool IsGBC = true;
        public bool GBCMode = false;

        public double FPS { get { return Gpu.FPS; } }
        public double CPS { get { return Cpu.CPS; } }
        public bool IsPaused { get; private set; } = false;

        public event EventHandler OnPause;

        public List<UInt16> Breakpoints { get; private set; } = new List<UInt16>();

        private Action<BitmapSource, BitmapSource> renderMethod;

        public GameBoy(Action<BitmapSource, BitmapSource> renderMethod)
        {
            this.renderMethod = renderMethod;
        }

        public void LoadCartridge(Cartridge cartridge)
        {
            Cartridge = cartridge;
            DMA = new DMA(this);
            Ram = new RAM(this);
            Run();
        }

        public async Task<bool> Restart()
        {
            await Terminate();
            Ram = new RAM(this);

            terminated = false;
            GBCMode = false;
            Run();
            return true;
        }

        public void Run()
        {
            if (Cartridge is null)
            {
                throw new Exception();
            }

            // First of all, let's dump our ROM in memory
            Ram.DumpCartridge(Cartridge);

            // Let's populate our header
            CartridgeHeader = new CartridgeHeader(Ram);

            if (IsGBC && (CartridgeHeader.CGBFlag == CGBFlag.CGB_ONLY || CartridgeHeader.CGBFlag == CGBFlag.CGB_SUPPORT))
            {
                GBCMode = true;
            }

            Ram.EnableDynamic(CartridgeHeader);
            Ram.LoadExternalMemory(Cartridge.SaveFileName);

            // Now we initialize our CPU and power it up
            Registers = new Registers();
            if (IsGBC)
            {
                // TODO: handle non cgb mode?
                Registers.AF.Left = 0x11; // This tells our game that this is a GBC
                Ram.WriteByte(0xFF6C, 0xFE, true);
                Ram.WriteByte(0xFF74, 0x00, true);
            }
            Gpu = new GPU(this, renderMethod, GBCMode);
            Timer = new Timer(this);
            Cpu = new CPU(Ram, Registers, Gpu, Timer, FixedAddresses.ENTRY_POINT);
            Soundboard = new Soundboard();
            PaletteSystem = new PaletteSystem();
            Interrupt = new InterruptService(this);
            Joypad = new Joypad(this);

            SetupEvents();

            cancellationTokenSource = new System.Threading.CancellationTokenSource();
            executionTask = new Task(new Action(() =>
            {
                while (!terminated)
                {
                    if (!IsPaused) { LoopStep(); }
                }
            }
            ), cancellationTokenSource.Token);
            executionTask.Start();
        }

        void SetupEvents()
        {
            Gpu.OnReadVRAM += () =>
               {
                   VRAMSnapshot res = new VRAMSnapshot();
                   res.GbPalettes = PaletteSystem.RetrieveGBPalettes();
                   // Bank 0
                   res.SetBGMap(0, 0, Ram.ReadVRAMDirect(0, FixedAddresses.VRAM_BG_MAP_0.Start, FixedAddresses.VRAM_BG_MAP_0.Length));
                   res.SetBGMap(0, 1, Ram.ReadVRAMDirect(0, FixedAddresses.VRAM_BG_MAP_1.Start, FixedAddresses.VRAM_BG_MAP_1.Length));
                   res.SetTileData(0, 0, Ram.ReadVRAMDirect(0, FixedAddresses.VRAM_TILE_DATA_0.Start, FixedAddresses.VRAM_TILE_DATA_0.Length));
                   res.SetTileData(0, 1, Ram.ReadVRAMDirect(0, FixedAddresses.VRAM_TILE_DATA_1.Start, FixedAddresses.VRAM_TILE_DATA_1.Length));

                   if (GBCMode)
                   {
                       res.GbcPalettes = PaletteSystem.RetrieveGBCPalettes();

                       // Bank 1
                       res.SetBGMap(1, 0, Ram.ReadVRAMDirect(1, FixedAddresses.VRAM_BG_MAP_0.Start, FixedAddresses.VRAM_BG_MAP_0.Length));
                       res.SetBGMap(1, 1, Ram.ReadVRAMDirect(1, FixedAddresses.VRAM_BG_MAP_1.Start, FixedAddresses.VRAM_BG_MAP_1.Length));
                       res.SetTileData(1, 0, Ram.ReadVRAMDirect(1, FixedAddresses.VRAM_TILE_DATA_0.Start, FixedAddresses.VRAM_TILE_DATA_0.Length));
                       res.SetTileData(1, 1, Ram.ReadVRAMDirect(1, FixedAddresses.VRAM_TILE_DATA_1.Start, FixedAddresses.VRAM_TILE_DATA_1.Length));
                   }
                   return res;
               };
        }

        public void LoopStep()
        {
            int clock = Cpu.ExecuteCPU();
            if (Breakpoints.Contains(Registers.PC.Full))
            {
                Pause();
                return;
            }

            if (Interrupt.HandleInterrupts())
            {
                clock += 20;
            }
            if (Breakpoints.Contains(Registers.PC.Full))
            {
                Pause();
                return;
            }

            Gpu.ExecuteGPU(clock);
            DMA.TickOAM(clock);
        }

        public void Pause()
        {
            IsPaused = true;
            Gpu.RenderToScreen();
            OnPause(this, null);
        }
        public void Resume()
        {
            IsPaused = false;
        }

        public async Task<bool> Terminate()
        {
            Ram.SaveExternalMemory(Cartridge.SaveFileName);
            Soundboard.Dispose();
            if (executionTask != null)
            {
                terminated = true;
                cancellationTokenSource.Cancel();
                await executionTask;
                executionTask.Dispose();
                executionTask = null;
            }
            return true;
        }


        public void SetButtonState(JoyButton button, bool pressed)
        {
            Joypad.SetButtonState(button, pressed);
        }
    }
}
