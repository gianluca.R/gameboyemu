﻿namespace GameBoyEmu.Emu.Enums
{
    public enum RamSize
    {
        SIZE_NONE = 0x00,
        SIZE_2KB = 0x01,
        SIZE_8KB = 0x02,
        SIZE_32KB = 0x03
    }
}
