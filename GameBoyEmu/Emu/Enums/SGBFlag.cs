﻿namespace GameBoyEmu.Emu.Enums
{
    public enum SGBFlag
    {
        SGB_DISABLED = 0x00,
        SGB_ENABLED = 0x03

        // Technically any value other than 0x03 will act as 0x00, but is undocumented
    }
}
