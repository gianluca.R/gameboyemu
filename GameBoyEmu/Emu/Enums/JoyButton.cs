﻿namespace GameBoyEmu.Emu.Enums
{
    public enum JoyButton
    {
        A = 1, B = 2,
        UP = 3, RIGHT = 4, DOWN = 5, LEFT = 6,
        START = 7, SELECT = 8
    }
}
