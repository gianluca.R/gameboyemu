﻿namespace GameBoyEmu.Emu.Enums
{
    public enum CGBFlag
    {
        NOT_SUPPORTED = 0x0,
        CGB_SUPPORT = 0x80,
        CGB_ONLY = 0xC0

        // Technically more values are available, but undocumented: should allow monochrome games to have some basic palette
    }
}
