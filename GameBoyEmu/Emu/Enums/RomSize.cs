﻿namespace GameBoyEmu.Emu.Enums
{
    public enum RomSize
    {
        BANKS_0 = 0x00,
        BANKS_4 = 0x01,
        BANKS_8 = 0x02,
        BANKS_16 = 0x03,
        BANKS_32 = 0x04,
        BANKS_64 = 0x05,
        BANKS_128 = 0x06,
        BANKS_256 = 0x07,
        // TODO: incomplete
    }
}
