﻿namespace GameBoyEmu.Emu.Enums
{
    public enum Accessor
    {
        A = 1, B = 2, C = 3, D = 4, E = 5, F = 6, H = 7, L = 8,
        AF = 100, BC = 101, DE = 102, HL = 103, SP = 104,
        n = 700, nn = 701, signedNPlusPC = 707, signedNPlusSP = 708,
        atNN = 9001, atSP = 9002, atHL = 9003, atNPlusIO = 9004, atHLI = 9905, atHLD = 9906, atCPlusIO = 9907, atBC = 9908, atDE = 9909,
        flagZ = 2000, flagNZ = 2001, flagN = 2010, flagNN = 2011, flagC = 2020, flagNC = 2021, flagH = 2030, flagNH = 2031

    }

    public static class AccessorExtension
    {
        public static string GetFriendlyName(this Accessor a)
        {
            switch (a)
            {
                case Accessor.signedNPlusPC:
                    return "PC+sn";
                case Accessor.atNN:
                    return "(nn)";
                case Accessor.atSP:
                    return "(SP)";
                case Accessor.atHL:
                    return "(HL)";
                case Accessor.atNPlusIO:
                    return "(FF00+n)";
                case Accessor.atHLI:
                    return "(HLI)";
                case Accessor.atHLD:
                    return "(HLD)";
                case Accessor.atCPlusIO:
                    return "(FF00+C)";
                case Accessor.atBC:
                    return "(BC)";
                case Accessor.atDE:
                    return "(DE)";

                case Accessor.flagZ:
                    return "Z";
                case Accessor.flagNZ:
                    return "NZ";

                case Accessor.flagC:
                    return "C";
                case Accessor.flagNC:
                    return "NC";

                case Accessor.flagN:
                    return "N";
                case Accessor.flagNN:
                    return "NN";

                case Accessor.flagH:
                    return "H";
                case Accessor.flagNH:
                    return "NH";
                default:
                    return a.ToString();
            }
        }
    }
}
