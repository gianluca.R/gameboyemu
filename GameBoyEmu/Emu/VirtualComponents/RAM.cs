﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Utils;
using GameBoyEmu.Emu.VirtualComponents.Auxiliary;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace GameBoyEmu.Emu.VirtualComponents
{
    /// <summary>
    /// GameBoy's virtual RAM. It works as an abstraction of the Game Boy's memory and interaction with IO Ports and others: in its current state it's not meant to execute any I/O logic 
    /// by itself, but simply to be manipulated by external agents.
    /// </summary>
    public class RAM
    {
        private GameBoy GB;
        private byte[] memory;
        private byte[] virtualCartridgeMemory; // We keep this separate from memory, so that memory banking is easier to apply
        private byte[][] wramBanks;
        private byte[][] extRam;
        private byte[][] vram;
        public int RomBank { get; set; } = 1; // Default value
        public int WramBank { get; set; } = 1; // Default
        public int ExtRamBank { get; set; } = 0; // Default value
        public int VRamBank { get; set; } = 0; // Default value
        private bool ExternalMemoryEnabled = false;
        private bool IsRomBankingMode = true;


        // For MBC3 ERAM/RTC
        private ExternalRamMode externalRamMode = ExternalRamMode.EXTERNAL_RAM;
        private int rtcRegister = 0;

        private RTC rtc = new RTC();

        public CartridgeType CartridgeMemoryType { get; private set; } = CartridgeType.ROM_ONLY; // Temp fix, in final version ROM_ONLY should be a good default value, we just haven't handled that case yet and this will work for testing

        public RAM(GameBoy gb)
        {
            GB = gb;
            memory = new byte[0xFFFF + 1]; // Extra 1 is for Interrupt Register

            Random r = new Random();
            r.NextBytes(memory); // Initial status of memory is undefined. To prevent any bias, we fill it with random bytes, as would happen on real hardware

            memory[0xFF00] = 0xCF; // INPUT
            memory[0xFF01] = 0x00;
            memory[0xFF02] = 0x7E;
            memory[0xFF03] = 0xFF;
            memory[0xFF04] = 0xAB;
            memory[0xFF05] = 0x00; // TIMA
            memory[0xFF06] = 0x00; // TMA
            memory[0xFF07] = 0xF8; // TAC
            memory[0xFF08] = 0xFF;
            memory[0xFF09] = 0xFF;
            memory[0xFF0A] = 0xFF;
            memory[0xFF0B] = 0xFF;
            memory[0xFF0C] = 0xFF;
            memory[0xFF0D] = 0xFF;
            memory[0xFF0E] = 0xFF;
            memory[0xFF0F] = 0xE1; // IF
            memory[0xFF10] = 0x80; // NR10
            memory[0xFF11] = 0xBF; // NR11
            memory[0xFF12] = 0xF3; // NR12
            memory[0xFF13] = 0xFF;
            memory[0xFF14] = 0xBF; // NR14
            memory[0xFF15] = 0xFF;
            memory[0xFF16] = 0x3F; // NR21
            memory[0xFF17] = 0x00; // NR22
            memory[0xFF18] = 0xFF;
            memory[0xFF19] = 0xBF; // NR24
            memory[0xFF1A] = 0x7F; // NR30
            memory[0xFF1B] = 0xFF; // NR31
            memory[0xFF1C] = 0x9F; // NR32
            memory[0xFF1E] = 0xBF; // NR33
            memory[0xFF20] = 0xFF; // NR41
            memory[0xFF21] = 0x00; // NR42
            memory[0xFF22] = 0x00; // NR43
            memory[0xFF23] = 0xBF; // NR30
            memory[0xFF24] = 0x77; // NR50
            memory[0xFF25] = 0xF3; // NR51
            memory[0xFF26] = 0xF1; // NR52 : 0xF1-GB, 0xF0-SGB; 
            memory[0xFF30] = 0xB8;
            memory[0xFF31] = 0xAE;
            memory[0xFF32] = 0x88;
            memory[0xFF33] = 0xB9;
            memory[0xFF34] = 0x22;
            memory[0xFF35] = 0xD9;
            memory[0xFF36] = 0x20;
            memory[0xFF37] = 0x8F;
            memory[0xFF38] = 0x01;
            memory[0xFF39] = 0xFD;
            memory[0xFF3A] = 0xEA;
            memory[0xFF3B] = 0x3D;
            memory[0xFF3C] = 0x85;
            memory[0xFF3D] = 0xFF;
            memory[0xFF3E] = 0x10;
            memory[0xFF3F] = 0x3B;
            memory[0xFF42] = 0x00; // SCY
            memory[0xFF43] = 0x00; // SCX
            memory[0xFF44] = 0x00; // LY
            memory[0xFF45] = 0x00; // LYC
            memory[0xFF47] = 0xFC; // BGP
            memory[0xFF48] = 0xFF; // OBP0
            memory[0xFF49] = 0xFF; // OBP1
            memory[0xFF4A] = 0x00; // WY
            memory[0xFF4B] = 0x00; // WX
            memory[0xFF55] = 0xFF; // VRAM DMA status
            memory[0xFFFF] = 0x00; // IE

            wramBanks = new byte[8][];
            for (int k = 0; k < 8; k++)
            {
                wramBanks[k] = new byte[0x1000];
            }
            vram = new byte[2][];
            vram[0] = new byte[0x2000];
            vram[1] = new byte[0x2000];

            virtualCartridgeMemory = new byte[0x100000];
        }

        public void DumpCartridge(Cartridge cartridge)
        {
            virtualCartridgeMemory = cartridge.ReadAll();
            Debug.Log("Dumped cartridge (" + virtualCartridgeMemory.Length + " bytes) in memory.");
        }

        public void EnableDynamic(ICartridgeHeader header)
        {
            switch (header.CartridgeType)
            {
                case CartridgeType.ROM_ONLY:
                    CartridgeMemoryType = CartridgeType.ROM_ONLY;
                    break;
                case CartridgeType.MBC1:
                case CartridgeType.MBC1_RAM:
                case CartridgeType.MBC1_RAM_BATTERY:
                    CartridgeMemoryType = CartridgeType.MBC1;
                    break;
                case CartridgeType.MBC3:
                case CartridgeType.MBC3_RAM:
                case CartridgeType.MBC3_RAM_BATTERY:
                case CartridgeType.MBC3_TIMER_RAM_BATTERY:
                case CartridgeType.MBC3_TIMER_BATTERY:
                    CartridgeMemoryType = CartridgeType.MBC3;
                    break;
                case CartridgeType.MBC5:
                case CartridgeType.MBC5_RAM:
                case CartridgeType.MBC5_RAM_BATTERY:
                case CartridgeType.MBC5_RUMBLE:
                case CartridgeType.MBC5_RUMBLE_RAM:
                case CartridgeType.MBC5_RUMBLE_RAM_BATTERY:
                    CartridgeMemoryType = CartridgeType.MBC5;
                    break;
                default:
                    throw new Exception("Unsupported cartridge memory banking.");
            }

            switch (header.ExtRamSize) // TODO: note that MBC2 has this set to none but will have 4 banks of 512 bytes always
            {
                case RamSize.SIZE_NONE: break;
                case RamSize.SIZE_2KB:
                    extRam = new byte[1][];
                    extRam[0] = new byte[0x800];
                    break;
                case RamSize.SIZE_8KB:
                    extRam = new byte[1][];
                    extRam[0] = new byte[0x2000];
                    break;
                case RamSize.SIZE_32KB:
                    extRam = new byte[4][];
                    for (int k = 0; k < 4; k++)
                    {
                        extRam[k] = new byte[0x2000];
                    }
                    break;
                default:
                    throw new Exception("Unsupported ERAM size.");
            }
        }

        /// <summary>
        /// Directly sets a byte in memory. Will not trigger any special action, will ignore banking and write only to memory areas that are not handled by special routines.
        /// </summary>
        public void SetByte(int pos, byte value)
        {
            this.memory[pos] = value;
        }
        public void WriteByte(int pos, byte value, bool bypassLimits)
        {
            if (!RamIOHelper.IsAccessAllowedDuringOAMDMA(GB, pos, bypassLimits))
            {
                return; // We simply do nothing, illegal access, TODO: make exception raising optional
            }

            if (pos <= FixedAddresses.WRITE_RAM_ENABLE.End)
            {
                ExternalMemoryEnabled = (value & 0x0A) == 0x0A;
            }
            else if (pos <= FixedAddresses.WRITE_ROM_BANK_NUMBER.End)
            {
                if (CartridgeMemoryType == CartridgeType.MBC3)
                {
                    if (value == 0) { value = 1; }
                    RomBank = value & 0x7F; // MBC3 writes 7 bits here
                }
                else if (CartridgeMemoryType == CartridgeType.MBC5)
                {
                    // MBC5 will treat 0 as 0!
                    if (pos < FixedAddresses.WRITE_ROM_BANK_NUMBER_HIGH_BIT.Start)
                    {
                        // Lower 8 bits of bank number
                        RomBank = (RomBank & 0x100) | value; // MBC5 writes the lower 8 bits of RomBank through this
                    }
                    else
                    {
                        // 9th bit
                        RomBank = (RomBank & 0xFF) | ((value & 0x1) << 8);
                    }
                }
                else if (CartridgeMemoryType == CartridgeType.MBC1)
                {
                    if (value == 0) { value = 1; }
                    RomBank = (RomBank & 0xE0) | (value & 0x1F);
                    RomBank = BankingUtils.AdjustMBC1RomBank(RomBank);
                }
            }
            else if (pos <= FixedAddresses.WRITE_RAM_BANK_NUMBER.End)
            {
                if (CartridgeMemoryType == CartridgeType.MBC3)
                {
                    if (value <= 3)
                    {
                        ExtRamBank = value;
                        externalRamMode = ExternalRamMode.EXTERNAL_RAM;
                    }
                    else
                    {
                        externalRamMode = ExternalRamMode.RTC;
                        rtcRegister = value - 0x08;
                    }
                }
                else if (CartridgeMemoryType == CartridgeType.MBC5)
                {
                    ExtRamBank = value & 0x0F;
                }
                else
                {
                    if (IsRomBankingMode)
                    {
                        RomBank |= (value & 0x03) << 5;
                        RomBank = BankingUtils.AdjustMBC1RomBank(RomBank);
                    }
                    else
                    {
                        ExtRamBank = value & 0x03;
                    }
                }
            }
            else if (pos <= FixedAddresses.WRITE_LATCH_CLOCK_DATA.End)
            {
                if (CartridgeMemoryType == CartridgeType.MBC3)
                {
                    rtc.Latch(value);
                }
                else if (CartridgeMemoryType == CartridgeType.MBC1)
                {
                    if (value == 0)
                    {
                        if (IsRomBankingMode == false)
                        {
                            // Since we're switching, we have to apply the value that previously was extRamBank to romBank
                            RomBank = (RomBank & 0x1F) | ((ExtRamBank & 0x03) << 5);
                            ExtRamBank = 0;
                        }
                        IsRomBankingMode = true;
                    }
                    else
                    {
                        if (IsRomBankingMode == true)
                        {
                            // Since we're switching, we have to apply the value that previously was bit 5-6 to extRam
                            ExtRamBank = RomBank >> 5;
                            RomBank &= 0x1F;
                            RomBank = BankingUtils.AdjustMBC1RomBank(RomBank);
                        }
                        IsRomBankingMode = false;
                    }
                }
                else
                {
                    throw new Exception("Writing forbidden by current MBC.");
                }
            }
            else if (pos <= FixedAddresses.VRAM.End)
            {
                // If access is denied, nothing happens
                if (GB.Gpu.Display.CanAccessVRAM || bypassLimits)
                {
                    vram[VRamBank][pos - FixedAddresses.VRAM.Start] = value;
                }
            }
            else if (pos <= FixedAddresses.EXT_RAM.End)
            {
                if (externalRamMode == ExternalRamMode.EXTERNAL_RAM)
                {
                    int relativePos = pos - FixedAddresses.EXT_RAM.Start;
                    if (ExternalMemoryEnabled &&
                        extRam != null &&
                        extRam.Length > ExtRamBank &&
                        relativePos < extRam[ExtRamBank].Length)
                    {
                        extRam[ExtRamBank][relativePos] = value;
                    }
                }
                else
                {
                    rtc.WriteRegister(rtcRegister, value);
                }
            }
            else if (pos <= FixedAddresses.WRAM0.End)
            {
                SetByte(pos, value);
            }
            else if (pos <= FixedAddresses.WRAM1.End)
            {
                wramBanks[WramBank][pos - FixedAddresses.WRAM1.Start] = value;
            }
            else if (pos <= FixedAddresses.ECHO.End)
            {
                int diff = FixedAddresses.ECHO.Start - FixedAddresses.WRAM0.Start;
                int realPos = pos - diff; // TODO: riscrivimi
                WriteByte(realPos, value, bypassLimits);
            }
            else if (pos <= FixedAddresses.OAM.End)
            {
                if (GB.Gpu.Display.CanAccessOAM || bypassLimits)
                {
                    SetByte(pos, value);
                }
            }
            else if (pos <= FixedAddresses.NOT_USABLE.End)
            {
                // Do nothing
            }
            else if (pos <= FixedAddresses.INTERRUPT_ENABLE_REGISTER) // IO and everything that comes after is handled by this helper
            {
                // Some IO ports have special behaviour once set
                RamIOHelper.WriteIORam(GB, pos, value, bypassLimits);
            }
            else
            {
                throw new Exception("Tried to access unmapped memory.");
            }
        }

        /// <summary>
        /// Directly reads a byte from memory. Will not trigger any special action, will ignore banking and read only from memory areas that are not handled by special routines.
        /// </summary>
        public byte GetByte(int pos)
        {
            return memory[pos];
        }
        public byte ReadByte(int pos, bool bypassLimits)
        {
            if (!RamIOHelper.IsAccessAllowedDuringOAMDMA(GB, pos, bypassLimits))
            {
                // As always, if memory isn't accessible any read will return 0xFF
                return 0xFF;
            }

            if (pos <= FixedAddresses.ROM_BANK_0.End)
            {
                return virtualCartridgeMemory[pos];
            }
            else if (pos <= FixedAddresses.ROM_BANK_N.End)
            {
                return virtualCartridgeMemory[pos + (RomBank - 1) * 0x4000];
            }
            else if (pos <= FixedAddresses.VRAM.End)
            {
                // If access is denied, return undefined
                if (GB.Gpu.Display.CanAccessVRAM || bypassLimits)
                {
                    return vram[VRamBank][pos - FixedAddresses.VRAM.Start];
                }
                else
                {
                    return 0xFF;
                }
            }
            else if (pos <= FixedAddresses.EXT_RAM.End)
            {
                if (externalRamMode == ExternalRamMode.EXTERNAL_RAM)
                {
                    int relativePos = pos - FixedAddresses.EXT_RAM.Start;
                    if (ExternalMemoryEnabled &&
                        extRam.Length > ExtRamBank &&
                        relativePos < extRam[ExtRamBank].Length)
                    {
                        return extRam[ExtRamBank][relativePos];
                    }
                    else
                    {
                        return 0xFF;
                    }
                }
                else //RTC
                {
                    return rtc.GetRegister(rtcRegister);
                }
            }
            else if (pos <= FixedAddresses.WRAM0.End)
            {
                return memory[pos];
            }
            else if (pos <= FixedAddresses.WRAM1.End)
            {
                return wramBanks[WramBank][pos - FixedAddresses.WRAM1.Start];
            }
            else if (pos <= FixedAddresses.ECHO.End)
            {
                int diff = FixedAddresses.ECHO.Start - FixedAddresses.WRAM0.Start;
                int realPos = pos - diff; // TODO: riscrivimi
                return ReadByte(realPos, bypassLimits);
            }
            else if (pos <= FixedAddresses.OAM.End)
            {
                // If access is denied, return undefined
                if (GB.Gpu.Display.CanAccessOAM || bypassLimits)
                {
                    return memory[pos];
                }
                else
                {
                    return 0xFF;
                }
            }
            else if (pos <= FixedAddresses.NOT_USABLE.End)
            {
                if (bypassLimits)
                {
                    return memory[pos];
                }
                else
                {
                    throw new Exception("Reading from disabled RAM not allowed.");
                }
            }
            else if (pos <= FixedAddresses.INTERRUPT_ENABLE_REGISTER) // Includes IO & HRAM
            {
                return RamIOHelper.ReadIORam(GB, pos, bypassLimits);
            }
            else
            {
                throw new Exception("Tried to access unmapped memory.");
            }
        }

        public byte[] ReadBlock(int pos, int len, bool bypassLimits)
        {
            if (len <= 0)
            {
                throw new Exception("Invalid block size.");
            }
            byte[] res = new byte[len];
            if (!RamIOHelper.IsAccessAllowedDuringOAMDMA(GB, pos, bypassLimits))
            {
                for (int k = 0; k < res.Length; k++) { res[k] = 0xFF; }
                return res; // TODO: make exception raising optional
            }

            if (pos <= FixedAddresses.ROM_BANK_0.End)
            {
                Array.Copy(this.virtualCartridgeMemory, pos, res, 0, len);
            }
            else if (pos <= FixedAddresses.ROM_BANK_N.End)
            {
                Array.Copy(this.virtualCartridgeMemory, pos + ((RomBank - 1) * 0x4000), res, 0, len);
            }
            else if (pos <= FixedAddresses.VRAM.End)
            {
                // If access is denied, return undefined
                if (GB.Gpu.Display.CanAccessVRAM || bypassLimits)
                {
                    Array.Copy(this.vram[VRamBank], pos - FixedAddresses.VRAM.Start, res, 0, len);
                }
            }
            else if (pos <= FixedAddresses.EXT_RAM.End)
            {
                int relativePos = pos - FixedAddresses.EXT_RAM.Start;
                if (ExternalMemoryEnabled &&
                    extRam.Length > ExtRamBank &&
                    relativePos < extRam[ExtRamBank].Length)
                {
                    Array.Copy(extRam[ExtRamBank], relativePos, res, 0, len);
                }
            }
            else if (pos <= FixedAddresses.WRAM0.End)
            {
                Array.Copy(this.memory, pos, res, 0, len);
            }
            else if (pos <= FixedAddresses.WRAM1.End)
            {
                Array.Copy(wramBanks[WramBank], pos - FixedAddresses.WRAM1.Start, res, 0, len);
            }
            else if (pos <= FixedAddresses.ECHO.End)
            {
                int diff = FixedAddresses.ECHO.Start - FixedAddresses.WRAM0.Start;
                int realPos = pos - diff; // TODO: riscrivimi
                return ReadBlock(realPos, len, bypassLimits);
            }
            else if (pos <= FixedAddresses.OAM.End)
            {
                // If access is denied, return undefined
                if (GB.Gpu.Display.CanAccessOAM || bypassLimits)
                {
                    Array.Copy(this.memory, pos, res, 0, len);
                }
            }
            else if (pos <= FixedAddresses.NOT_USABLE.End)
            {
                throw new Exception("Reading from disabled RAM not allowed.");
            }
            else if (pos <= FixedAddresses.INTERRUPT_ENABLE_REGISTER) // Includes IO & HRAM
            {
                Array.Copy(this.memory, pos, res, 0, len);
            }
            else
            {
                throw new Exception("Tried to access unmapped memory.");
            }

            return res;
        }

        public void WriteBlock(int pos, byte[] value, bool bypassLimits)
        {
            int len = value.Length;
            if (len <= 0)
            {
                return; //Again, we do nothing here. TODO: optional exceptions
            }
            if (!RamIOHelper.IsAccessAllowedDuringOAMDMA(GB, pos, bypassLimits))
            {
                return;
            }

            // TODO: all these won't check if data overflows in next region or not, which might cause problems but is faster

            if (pos < FixedAddresses.VRAM.Start) //Everything before VRAM shouldn't be written in block ever
            {
                throw new Exception("Block writing to ROM region not implemented.");
            }
            else if (pos <= FixedAddresses.VRAM.End)
            {
                // If access is denied, nothing happens
                if (GB.Gpu.Display.CanAccessVRAM || bypassLimits)
                {
                    Array.Copy(value, 0, vram[VRamBank], pos - FixedAddresses.VRAM.Start, len);
                }
            }
            else if (pos <= FixedAddresses.EXT_RAM.End)
            {
                int relativePos = pos - FixedAddresses.EXT_RAM.Start;
                if (ExternalMemoryEnabled &&
                    extRam.Length > ExtRamBank &&
                    relativePos < extRam[ExtRamBank].Length)
                {
                    Array.Copy(value, 0, wramBanks[ExtRamBank], relativePos, len);
                }
            }
            else if (pos <= FixedAddresses.WRAM0.End)
            {
                Array.Copy(value, 0, memory, pos, len);
            }
            else if (pos <= FixedAddresses.WRAM1.End)
            {
                Array.Copy(value, 0, wramBanks[WramBank], pos - FixedAddresses.WRAM1.Start, len);
            }
            else if (pos <= FixedAddresses.ECHO.End)
            {
                throw new Exception("Writing to ECHO not implemented.");
            }
            else if (pos <= FixedAddresses.OAM.End)
            {
                if (GB.Gpu.Display.CanAccessOAM || bypassLimits)
                {
                    Array.Copy(value, 0, memory, pos, len);
                }
            }
            else if (pos <= FixedAddresses.NOT_USABLE.End)
            {
                throw new Exception("Writing to disabled RAM not allowed.");
            }
            else if (pos <= FixedAddresses.IO_PORTS.End)
            {
                throw new Exception("Block writing in IO region not implemented.");
            }
            else if (pos <= FixedAddresses.INTERRUPT_ENABLE_REGISTER) // This includes HRAM, it's useless to make 2 conditions
            {
                Array.Copy(value, 0, memory, pos, len);
            }
            else
            {
                throw new Exception("Tried to access unmapped memory.");
            }
        }


        public UInt16 ReadShort(int pos, bool bypassLimits)
        {
            //byte[] data = ReadBlock(pos, 2, bypassLimits);
            //return (UInt16)((data[1] << 8) | data[0]);
            return (UInt16)((ReadByte(pos + 1, bypassLimits) << 8) | ReadByte(pos, bypassLimits));
        }
        public string ReadString(int start, int len, bool bypassLimits)
        {
            byte[] raw = this.ReadBlock(start, len, bypassLimits);
            return new ASCIIEncoding().GetString(raw.TakeWhile(b => b != 0).ToArray());
        }

        public void WriteShort(int pos, UInt16 value, bool bypassLimits)
        {
            WriteByte(pos, (byte)(value & 0x00FF), bypassLimits);
            pos++;
            if (pos > 0xFFFF) { pos = 0; } // This ensures we loop back to the beginning of our memory
            WriteByte(pos, (byte)((value & 0xFF00) >> 8), bypassLimits);
        }


        public byte[] ReadVRAMDirect(int bank, int pos, int len)
        {
            pos -= FixedAddresses.VRAM.Start;
            byte[] res = new byte[len];
            Array.Copy(vram[bank], pos, res, 0, len);
            return res;
        }

        public void SaveExternalMemory(string file)
        {
            if (extRam == null || extRam.Length == 0) { return; }
            if (File.Exists(file)) { File.Delete(file); }
            BinaryWriter bw = new BinaryWriter(new FileStream(file, FileMode.CreateNew, FileAccess.Write));

            bw.Write(extRam.Length);

            for (int k = 0; k < extRam.Length; k++)
            {
                bw.Write(extRam[k].Length);
                bw.Write(extRam[k], 0, extRam[k].Length);
            }

            bw.Close();
        }

        public void LoadExternalMemory(string file)
        {
            if (!File.Exists(file)) { return; }
            BinaryReader bw = new BinaryReader(new FileStream(file, FileMode.Open, FileAccess.Read));

            int bankCount = bw.ReadInt32();
            if (bankCount != extRam.Length)
            {
                Debug.Error("Save file '" + file + "' is corrupt.");
                return;
            }

            for (int k = 0; k < bankCount; k++)
            {
                int bankSize = bw.ReadInt32();
                if (bankSize != extRam[k].Length)
                {
                    Debug.Error("Save file '" + file + "' is corrupt.");
                    return;
                }
                extRam[k] = bw.ReadBytes(bankSize);
            }

            bw.Close();
        }
    }
}
