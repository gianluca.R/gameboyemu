﻿using GameBoyEmu.Emu.Utils;
using System;

namespace GameBoyEmu.Emu.VirtualComponents
{
    public class Registers
    {
        // TODO: move to full fledged class, make abstract and separate those with high/low from those full 16bit
        public class Register
        {
            public byte Right { get; set; } = 0;
            public byte Left { get; set; } = 0;

            public UInt16 Full
            {
                get
                {
                    return BitConverter.ToUInt16(new[] { this.Right, this.Left }, 0);
                }
                set
                {
                    this.Right = (byte)(value & 0x00FF);
                    this.Left = (byte)((value & 0xFF00) >> 8);
                }
            }

            public Register() { }
            public Register(UInt16 value)
            {
                this.Full = value;
            }
        }

        // As debugger for easier testing
        public Register AF = new Register(0x01B0);
        public Register BC = new Register(0x0013);
        public Register DE = new Register(0x00D8);
        public Register HL = new Register(0x014D);
        public Register SP = new Register(0xFFFE);
        public Register PC = new Register(0x0100);
        public bool IME = true;
        public bool Halted = false;

        /// <summary>
        /// Zero Flag: set if result of operation is 0, or values match in CMP
        /// </summary>
        public bool flagZ
        {
            get { return getFlag(7); }
            set { setFlag(7, value); }
        }
        /// <summary>
        /// Subtract flag: set if a subtraction was performed
        /// </summary>
        public bool flagN
        {
            get { return getFlag(6); }
            set { setFlag(6, value); }
        }
        /// <summary>
        /// Half Carry Flag: set if a carry occurred in the lower 4 bits: 
        ///     so if the operation, executed on the last 4 bits of both operators, generates a value such that bit 4 is set, this becomes true
        /// </summary>
        public bool flagH
        {
            get { return getFlag(5); }
            set { setFlag(5, value); }
        }
        /// <summary>
        /// Carry Flag: set if a carry occurred or A lessthan val in CMP
        /// </summary>
        public bool flagC
        {
            get { return getFlag(4); }
            set { setFlag(4, value); }
        }

        private bool getFlag(int bit)
        {
            return BitPosition.GetBit(AF.Right, bit);
        }
        private void setFlag(int bit, bool set)
        {
            AF.Right = BitPosition.SetBit(AF.Right, bit, set);
        }
    }
}
