﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Utils;
using System.Collections.Generic;

namespace GameBoyEmu.Emu.VirtualComponents
{
    public class Joypad
    {
        private GameBoy GB;

        private bool AccessButtons = false;
        private bool AccessPad = false;
        public byte JOYP
        {
            get
            {
                byte res = 0xC0;
                res = BitPosition.SetBit(res, 5, !AccessButtons);
                res = BitPosition.SetBit(res, 4, !AccessPad);
                if (AccessButtons) // buttons
                {
                    res = BitPosition.SetBit(res, 0, !buttonState[JoyButton.A]);
                    res = BitPosition.SetBit(res, 1, !buttonState[JoyButton.B]);
                    res = BitPosition.SetBit(res, 2, !buttonState[JoyButton.SELECT]);
                    res = BitPosition.SetBit(res, 3, !buttonState[JoyButton.START]);
                }
                else if (AccessPad) // directional keys
                {
                    res = BitPosition.SetBit(res, 0, !buttonState[JoyButton.RIGHT]);
                    res = BitPosition.SetBit(res, 1, !buttonState[JoyButton.LEFT]);
                    res = BitPosition.SetBit(res, 2, !buttonState[JoyButton.UP]);
                    res = BitPosition.SetBit(res, 3, !buttonState[JoyButton.DOWN]);
                }
                return res;
            }
            set {
                AccessButtons = !BitPosition.GetBit(value, 5);
                AccessPad = !BitPosition.GetBit(value, 4);
            }
        }

        public Joypad(GameBoy gb)
        {
            this.GB = gb;
        }

        private Dictionary<JoyButton, bool> buttonState = new Dictionary<JoyButton, bool>() {
            {JoyButton.A, false},
            {JoyButton.B, false},
            {JoyButton.UP, false},
            {JoyButton.RIGHT, false},
            {JoyButton.DOWN, false},
            {JoyButton.LEFT, false},
            {JoyButton.START, false},
            {JoyButton.SELECT, false},
        };

        public void SetButtonState(JoyButton button, bool pressed)
        {
            bool oldValue = buttonState[button];
            if (pressed == oldValue) { return; }
            buttonState[button] = pressed;

            // We fire interrupt only when button was pressed
            // Note that while this is correct, technically due to signal repeat in real hardware this means that
            // this interrupt will often fire also when a button is released, and might fire multiple times per key press
            // Regardless, it is often ignored, so there's no point in firing it more than needed by specs
            if (pressed && !oldValue)
            {
                GB.Interrupt.RequestInterrupt(InterruptService.JOYPAD_INTERRUPT);
            }
            // TODO: should we update register here?
        }
    }
}
