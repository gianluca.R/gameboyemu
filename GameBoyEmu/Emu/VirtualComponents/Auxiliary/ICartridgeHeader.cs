﻿using GameBoyEmu.Emu.Enums;
using System;

namespace GameBoyEmu.Emu.VirtualComponents.Auxiliary
{
    public interface ICartridgeHeader
    {
        byte[] Logo { get; }
        string Title { get; }
        string ManufacturerCode { get; }
        CGBFlag CGBFlag { get; }
        string NewLicenseeCode { get; }
        SGBFlag SGBFlag { get; }
        CartridgeType CartridgeType { get; }
        RomSize RomSize { get; }
        RamSize ExtRamSize { get; }
        bool JapanMarket { get; }
        byte OldLicenseeCode { get; }
        byte ROMVersion { get; }
        byte HeaderChecksum { get; }
        UInt16 GlobalChecksum { get; }
    }
}
