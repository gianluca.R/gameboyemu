﻿using GameBoyEmu.Emu.Utils;
using System;

namespace GameBoyEmu.Emu.VirtualComponents.Auxiliary
{
    public class Timer
    {
        public static readonly int[] TimerClockFrequencies = new[] { 4096, 262144, 65536, 16384 };

        private GameBoy GB;

        private UInt16 lastInternalClock = 0;
        private UInt16 InternalClock = 0;
        public byte DIV
        {
            get
            {
                return (byte)((InternalClock & 0xFF00) >> 8);
            }
            set
            {
                ResetInternalCounter();
            }
        }

        public byte TIMA = 0; // Timer counter
        public byte TMA = 0; // Timer modulo

        public bool IsTimerEnabled = false;
        public byte TimerMode = 0;

        private int ModeBitShift
        {
            get
            {
                switch (TimerMode)
                {
                    case 0: return 9;
                    case 1: return 3;
                    case 2: return 5;
                    case 3: return 7;
                }
                throw new Exception("Invalid mode.");
            }
        }

        public byte TAC // Timer control register
        {
            get
            {
                byte res = 0xF8;
                res = BitPosition.SetBit(res, 2, IsTimerEnabled);
                res |= (byte)(TimerMode & 0x3);
                return res;
            }
            set
            {
                bool newEnabled = BitPosition.GetBit(value, 2);
                byte newMode = (byte)(value & 0x3);

                if (IsTimerEnabled)
                {
                    if (!newEnabled)
                    {
                        if ((InternalClock & (CPU.Frequency / TimerClockFrequencies[TimerMode] / 2)) != 0)
                        {
                            IncreaseTimer();
                        }
                    }
                    else
                    {
                        if ((InternalClock & (CPU.Frequency / TimerClockFrequencies[TimerMode] / 2)) != 0 &&
                            (InternalClock & (CPU.Frequency / TimerClockFrequencies[newMode] / 2)) == 0)
                        {
                            IncreaseTimer();
                        }
                    }
                }

                IsTimerEnabled = newEnabled;
                TimerMode = newMode;
            }
        }

        public Timer(GameBoy gb)
        {
            GB = gb;
        }

        private void ResetInternalCounter()
        {
            lastInternalClock = InternalClock;
            InternalClock = 0;

            if (IsTimerEnabled)
            {
                if (BitPosition.GetBit(lastInternalClock, ModeBitShift))
                {
                    IncreaseTimer();
                }
            }
        }

        public void Tick(int clock)
        {
            for (int k = 0; k < clock; k++)
            {
                lastInternalClock = InternalClock;
                InternalClock++;
                ProcessDivDelta();
            }
        }

        private void ProcessDivDelta()
        {
            if (InternalClock == 1024)
            {
                int x = 0;
            }
            if (IsTimerEnabled)
            {
                if (!BitPosition.GetBit(InternalClock, ModeBitShift) && BitPosition.GetBit(lastInternalClock, ModeBitShift))
                {
                    IncreaseTimer();
                }
            }
        }

        private void IncreaseTimer()
        {
            TIMA++;
            if (TIMA == 0) // We overflowed
            {
                TIMA = TMA;
                GB.Interrupt.RequestInterrupt(InterruptService.TIMER_INTERRUPT);
            }
        }
    }
}
