﻿using GameBoyEmu.Emu.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBoyEmu.Emu.VirtualComponents.Auxiliary
{
    public class Display
    {
        private GameBoy GB;

        private bool _isDisplayEnabled = true;
        public bool IsDisplayEnabled
        {
            get { return _isDisplayEnabled; }
            set
            {
                _isDisplayEnabled = value;
                if (!value)
                {
                    GB.Ram.SetByte(FixedAddresses.LCD_LY_REGISTER, 0); // If I set it through other methods, it risks triggering an interrupt! I want to bypass everything in this specific scenario
                    GB.Gpu.Display.DisplayMode = 1;
                }
            }
        }
        public bool UseWindowAlternateMap;
        public bool IsWindowEnabled;
        public bool UseAlternateTileData = true;
        public bool UseBGAlternateMap;
        public bool BigSprites;
        public bool AreSpritesEnabled;
        public bool IsBGEnabled = true;

        public byte LCDControlRegister
        {
            get
            {
                byte res = 0xFF;
                res = BitPosition.SetBit(res, 7, IsDisplayEnabled);
                res = BitPosition.SetBit(res, 6, UseWindowAlternateMap);
                res = BitPosition.SetBit(res, 5, IsWindowEnabled);
                res = BitPosition.SetBit(res, 4, UseAlternateTileData);
                res = BitPosition.SetBit(res, 3, UseBGAlternateMap);
                res = BitPosition.SetBit(res, 2, BigSprites);
                res = BitPosition.SetBit(res, 1, AreSpritesEnabled);
                res = BitPosition.SetBit(res, 0, IsBGEnabled);
                return res;
            }
            set
            {
                IsDisplayEnabled = BitPosition.GetBit(value, 7);
                UseWindowAlternateMap = BitPosition.GetBit(value, 6);
                IsWindowEnabled = BitPosition.GetBit(value, 5);
                UseAlternateTileData = BitPosition.GetBit(value, 4);
                UseBGAlternateMap = BitPosition.GetBit(value, 3);
                BigSprites = BitPosition.GetBit(value, 2);
                AreSpritesEnabled = BitPosition.GetBit(value, 1);
                IsBGEnabled = BitPosition.GetBit(value, 0);
            }
        }

        public bool IsCoincidenceInterruptEnabled;
        public bool IsOAMInterruptEnabled;
        public bool IsVBlankInterruptEnabled;
        public bool IsHBlankInterruptEnabled;
        public bool CoincidenceFlag = true;
        public byte DisplayMode = 1;

        public byte LCDStatusRegister
        {
            get
            {
                byte res = 0xFC;
                res = BitPosition.SetBit(res, 6, IsCoincidenceInterruptEnabled);
                res = BitPosition.SetBit(res, 5, IsOAMInterruptEnabled);
                res = BitPosition.SetBit(res, 4, IsVBlankInterruptEnabled);
                res = BitPosition.SetBit(res, 3, IsHBlankInterruptEnabled);
                res = BitPosition.SetBit(res, 2, CoincidenceFlag);
                res |= (byte)(DisplayMode & 0x3);
                return res;
            }
            set
            {
                IsCoincidenceInterruptEnabled = BitPosition.GetBit(value, 6);
                IsOAMInterruptEnabled = BitPosition.GetBit(value, 5);
                IsVBlankInterruptEnabled = BitPosition.GetBit(value, 4);
                IsHBlankInterruptEnabled = BitPosition.GetBit(value, 3);
            }
        }

        public bool CanAccessVRAM
        {
            get
            {
                return DisplayMode == 0 || DisplayMode == 1 || DisplayMode == 2;
            }
        }
        public bool CanAccessOAM
        {
            get
            {
                return DisplayMode == 0 || DisplayMode == 1;
            }
        }

        private byte _ly = 0;
        public byte LY
        {
            get { return _ly; }
            set
            {
                _ly = value;
                TriggerLYCoincidenceInterruptIfEnabled();
            }
        }
        private byte _lyc = 0;
        public byte LYC
        {
            get { return _lyc; }
            set
            {
                _lyc = value;
                TriggerLYCoincidenceInterruptIfEnabled();
            }
        }

        public byte WX = 0;
        public byte WY = 0;
        public byte SCX = 0;
        public byte SCY = 0;

        public Display(GameBoy gb)
        {
            this.GB = gb;
        }

        public void TriggerLYCoincidenceInterruptIfEnabled()
        {
            if (GB.Gpu.Display.IsCoincidenceInterruptEnabled)
            {
                bool coincidence = GB.Gpu.Display.LY == GB.Gpu.Display.LYC;
                GB.Gpu.Display.CoincidenceFlag = coincidence;
                if (coincidence)
                {
                    GB.Interrupt.RequestInterrupt(InterruptService.STAT_INTERRUPT);
                }
            }
        }

        public void TriggerOAMInterruptIfEnabled()
        {
            if (GB.Gpu.Display.IsOAMInterruptEnabled)
            {
                GB.Interrupt.RequestInterrupt(InterruptService.STAT_INTERRUPT);
            }
        }

        public void TriggerVBlankInterruptIfEnabled()
        {
            if (GB.Gpu.Display.IsVBlankInterruptEnabled)
            {
                GB.Interrupt.RequestInterrupt(InterruptService.STAT_INTERRUPT);
            }
        }

        public void TriggerHBlankInterruptIfEnabled()
        {
            if (GB.Gpu.Display.IsHBlankInterruptEnabled)
            {
                GB.Interrupt.RequestInterrupt(InterruptService.STAT_INTERRUPT);
            }
        }

    }
}
