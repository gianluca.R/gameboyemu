﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.Utils;
using System;

namespace GameBoyEmu.Emu.VirtualComponents.Auxiliary
{
    public class CartridgeHeader : ICartridgeHeader
    {
        public byte[] Logo { get; private set; }
        public string Title { get; private set; }
        public string ManufacturerCode { get; private set; }
        public CGBFlag CGBFlag { get; private set; }
        public string NewLicenseeCode { get; private set; }
        public SGBFlag SGBFlag { get; private set; }
        public CartridgeType CartridgeType { get; private set; }
        public RomSize RomSize { get; private set; }
        public RamSize ExtRamSize { get; private set; }
        public bool JapanMarket { get; private set; }
        public byte OldLicenseeCode { get; private set; }
        public byte ROMVersion { get; private set; }
        public byte HeaderChecksum { get; private set; }
        public UInt16 GlobalChecksum { get; private set; }

        public CartridgeHeader(RAM ram)
        {
            Logo = ram.ReadBlock(0x104, 0x2F, true);
            CGBFlag = (CGBFlag)ram.ReadByte(0x143, true);
            SGBFlag = (SGBFlag)ram.ReadByte(0x146, true);
            CartridgeType = (CartridgeType)ram.ReadByte(0x147, true);
            RomSize = (RomSize)ram.ReadByte(0x148, true);
            ExtRamSize = (RamSize)ram.ReadByte(0x149, true);
            JapanMarket = ram.ReadByte(0x14A, true) == 0x00;
            OldLicenseeCode = ram.ReadByte(0x14B, true);
            ROMVersion = ram.ReadByte(0x14C, true);
            HeaderChecksum = ram.ReadByte(0x14D, true);
            GlobalChecksum = ram.ReadShort(0x14E, true); // never gets checked

            Title = ram.ReadString(0x134, 0xF, true).Trim(); //size variable, CGB is shorter, 11 to 15 chars rather than 16, but we ignore it for now
            ManufacturerCode = ram.ReadString(0x13F, 0x3, true); //only in new cartridges, in older is part of title

            if (OldLicenseeCode == 0x33) // Special value indicates a newer cartridge that uses the new licensee code
            {
                NewLicenseeCode = ram.ReadString(0x144, 0x2, true); //new games only
            }

            // Now we check the header checksum
            byte[] headerBytes = ram.ReadBlock(0x134, 0x18, true);
            int calculatedHeaderChecksum = 0xFFFF;
            foreach (byte i in headerBytes)
            {
                calculatedHeaderChecksum -= i + 1;
            }

            if (HeaderChecksum != (calculatedHeaderChecksum & 0xFF))
            {
                Debug.Log("Invalid header checksum. Expected: " + HeaderChecksum + ", calculated: " + (calculatedHeaderChecksum & 0xFF));
            }

            Debug.Log("Loaded headers for " + Title + ".");
        }
    }
}
