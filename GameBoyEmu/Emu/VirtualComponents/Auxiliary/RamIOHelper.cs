﻿using GameBoyEmu.Emu.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBoyEmu.Emu.VirtualComponents.Auxiliary
{
    public class RamIOHelper
    {
        public static byte ReadIORam(GameBoy gb, int pos, bool bypassLimits)
        {
            switch (pos)
            {
                #region Display

                case FixedAddresses.LCD_CONTROL_REGISTER:
                    return gb.Gpu.Display.LCDControlRegister;
                case FixedAddresses.LCDC_STATUS_REGISTER:
                    return gb.Gpu.Display.LCDStatusRegister;
                case FixedAddresses.LCD_LY_REGISTER:
                    return gb.Gpu.Display.LY;
                case FixedAddresses.LCD_LY_COMPARE_REGISTER:
                    return gb.Gpu.Display.LYC;
                case FixedAddresses.WINDOW_X_REGISTER:
                    return gb.Gpu.Display.WX;
                case FixedAddresses.WINDOW_Y_REGISTER:
                    return gb.Gpu.Display.WY;
                case FixedAddresses.SCROLL_X:
                    return gb.Gpu.Display.SCX;
                case FixedAddresses.SCROLL_Y:
                    return gb.Gpu.Display.SCY;

                #endregion

                #region Sound

                case FixedAddresses.SOUND_CONTROL_REGISTER:
                    return gb.Soundboard.GetSoundControlRegister();

                #endregion

                #region Palettes

                case FixedAddresses.BACKGROUND_PALETTE_DATA:
                    return gb.PaletteSystem.ReadBGPaletteData();
                case FixedAddresses.SPRITE_PALETTE_DATA:
                    return gb.PaletteSystem.ReadSpritePaletteData();

                #endregion

                #region Timers

                case FixedAddresses.DIV_REGISTER:
                    return gb.Timer.DIV;
                case FixedAddresses.TIMER_COUNTER_REGISTER:
                    return gb.Timer.TIMA;
                case FixedAddresses.TIMER_MODULO_REGISTER:
                    return gb.Timer.TMA;
                case FixedAddresses.TIMER_CONTROL_REGISTER:
                    return gb.Timer.TAC;

                #endregion

                #region Interrupts

                case FixedAddresses.INTERRUPT_ENABLE_REGISTER:
                    return gb.Interrupt.IE;
                case FixedAddresses.INTERRUPT_FLAG_REGISTER:
                    return gb.Interrupt.IF;

                #endregion

                case FixedAddresses.JOYPAD_REGISTER:
                    return gb.Joypad.JOYP;

                default:
                    return gb.Ram.GetByte(pos);
            }
        }
        public static void WriteIORam(GameBoy gb, int pos, byte value, bool bypassLimits)
        {
            switch (pos)
            {
                #region Display

                case FixedAddresses.LCD_CONTROL_REGISTER:
                    gb.Gpu.Display.LCDControlRegister = value;
                    break;
                case FixedAddresses.LCDC_STATUS_REGISTER:
                    gb.Gpu.Display.LCDStatusRegister = value;
                    break;
                case FixedAddresses.LCD_LY_REGISTER:
                    // TODO:make exceptions optional on illegal access
                    throw new Exception("LY is read only.");
                case FixedAddresses.LCD_LY_COMPARE_REGISTER:
                    gb.Gpu.Display.LYC = value;
                    break;
                case FixedAddresses.WINDOW_X_REGISTER:
                    gb.Gpu.Display.WX = value;
                    break;
                case FixedAddresses.WINDOW_Y_REGISTER:
                    gb.Gpu.Display.WY = value;
                    break;
                case FixedAddresses.SCROLL_X:
                    gb.Gpu.Display.SCX = value;
                    break;
                case FixedAddresses.SCROLL_Y:
                    gb.Gpu.Display.SCY = value;
                    break;

                #endregion

                #region Timers

                case FixedAddresses.DIV_REGISTER:
                    gb.Timer.DIV = value;
                    break;
                case FixedAddresses.TIMER_COUNTER_REGISTER:
                    gb.Timer.TIMA = value;
                    break;
                case FixedAddresses.TIMER_MODULO_REGISTER:
                    gb.Timer.TMA = value;
                    break;
                case FixedAddresses.TIMER_CONTROL_REGISTER:
                    gb.Timer.TAC = value;
                    break;

                #endregion

                #region DMA

                case FixedAddresses.OAM_DMA_TRANSFER_REGISTER:
                    gb.Ram.SetByte(pos, value);
                    gb.DMA.StartOAMTransfer(value);
                    break;
                case FixedAddresses.VRAM_DMA_START_AND_LENGTH:
                    if (!bypassLimits)
                    {
                        value = gb.DMA.ExecuteVramDMA(value);
                    }
                    gb.Ram.SetByte(pos, value);
                    break;

                #endregion

                #region Banks

                case FixedAddresses.WRAM_BANK_SELECT_REGISTER:
                    // Only 3 lowest bits are used
                    if (value == 0) { value = 1; }
                    gb.Ram.WramBank = value & 0x7;
                    gb.Ram.SetByte(pos, (byte)gb.Ram.WramBank);
                    break;
                case FixedAddresses.VRAM_BANK_REGISTER:
                    gb.Ram.VRamBank = value & 0x1;
                    gb.Ram.SetByte(pos, (byte)gb.Ram.VRamBank);
                    break;

                #endregion

                #region Palettes

                case FixedAddresses.BGP_REGISTER:
                    gb.Ram.SetByte(pos, gb.PaletteSystem.ProcessBGP(pos, value));
                    break;
                case FixedAddresses.OBP0_REGISTER:
                    gb.Ram.SetByte(pos, gb.PaletteSystem.ProcessOBP0(pos, value));
                    break;
                case FixedAddresses.OBP1_REGISTER:
                    gb.Ram.SetByte(pos, gb.PaletteSystem.ProcessOBP1(pos, value));
                    break;
                case FixedAddresses.BACKGROUND_PALETTE_INDEX:
                    gb.Ram.SetByte(pos, gb.PaletteSystem.ProcessBGPaletteIndex(pos, value));
                    break;
                case FixedAddresses.BACKGROUND_PALETTE_DATA:
                    gb.Ram.SetByte(pos, gb.PaletteSystem.ProcessBGPaletteData(pos, value));
                    break;
                case FixedAddresses.SPRITE_PALETTE_INDEX:
                    gb.Ram.SetByte(pos, gb.PaletteSystem.ProcessSpritePaletteIndex(pos, value));
                    break;
                case FixedAddresses.SPRITE_PALETTE_DATA:
                    gb.Ram.SetByte(pos, gb.PaletteSystem.ProcessSpritePaletteData(pos, value));
                    break;

                #endregion

                #region Interrupts

                case FixedAddresses.INTERRUPT_ENABLE_REGISTER:
                    gb.Interrupt.IE = value;
                    break;
                case FixedAddresses.INTERRUPT_FLAG_REGISTER:
                    gb.Interrupt.IF = value;
                    break;

                #endregion

                case FixedAddresses.JOYPAD_REGISTER:
                    gb.Joypad.JOYP = value;
                    break;

                case FixedAddresses.GBC_DOUBLE_SPEED_REGISTER:
                    if (BitPosition.GetBit(value, 0))
                    {
                        // TODO: speed should switch after next STOP instruction
                        CPU.DoubleSpeed = !CPU.DoubleSpeed;
                        Debug.Log("Double Speed: " + CPU.DoubleSpeed);
                        value = BitPosition.SetBit(value, 7, CPU.DoubleSpeed);
                    }
                    value = BitPosition.SetBit(value, 0, false);
                    gb.Ram.SetByte(pos, value);
                    break;


                default:
                    if (FixedAddresses.AUDIO_REGISTERS.ContainsKey((UInt16)pos))
                    {
                        gb.Ram.SetByte(pos, gb.Soundboard.ProcessRegister(pos, value));
                    }
                    else if (pos >= FixedAddresses.SOUND_C3_WAVE_START && pos <= FixedAddresses.SOUND_C3_WAVE_END)
                    {
                        gb.Ram.SetByte(pos, gb.Soundboard.ProcessWaveData(pos, value));
                    }
                    else
                    {
                        gb.Ram.SetByte(pos, value);
                    }
                    break;
            }
        }

        public static bool IsAccessAllowedDuringOAMDMA(GameBoy gb, int pos, bool bypassLimits)
        {
            return !gb.DMA.IsOAMInProgress || bypassLimits || FixedAddresses.HRAM.Contains(pos) || FixedAddresses.IO_PORTS.Contains(pos);
        }
    }
}
