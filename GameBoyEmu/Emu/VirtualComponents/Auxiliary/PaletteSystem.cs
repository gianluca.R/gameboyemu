﻿using GameBoyEmu.Emu.Rendering;
using GameBoyEmu.Emu.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBoyEmu.Emu.VirtualComponents.Auxiliary
{
    public class PaletteSystem
    {
        public PaletteSystem()
        {
            // We don't want them to start as null, so we initialize them as they would be on a GB.
            BGP = ConvertGBPalette(0);
            OBP0 = ConvertGBPalette(0);
            OBP1 = ConvertGBPalette(0);
        }

        #region GB

        private Palette BGP;
        private Palette OBP0;
        private Palette OBP1;

        public byte ProcessBGP(int pos, byte value)
        {
            BGP = ConvertGBPalette(value);
            return value;
        }
        public byte ProcessOBP0(int pos, byte value)
        {
            OBP0 = ConvertGBPalette(value);
            return value;
        }
        public byte ProcessOBP1(int pos, byte value)
        {
            OBP1 = ConvertGBPalette(value);
            return value;
        }
        public Palette[][] RetrieveGBPalettes()
        {
            return new Palette[][] {
                new[] { BGP },
                new[] { OBP0, OBP1 }
            };
        }

        private Palette ConvertGBPalette(byte rawPalette)
        {
            Color[] colors = new Color[4];

            for (int k = 0; k < 4; k++)
            {
                byte lum = (byte)(3 - ((rawPalette & (0x3 << k * 2)) >> (k * 2)));
                lum *= 70;
                colors[k] = new Color(lum, lum, lum);
            }

            return new Palette(colors);
        }

        #endregion

        #region GBC

        int bgIndex;
        int spriteIndex;
        bool bgAutoincrement;
        bool spriteAutoincrement;
        byte[] rawBG = new byte[64];
        byte[] rawSprite = new byte[64];

        public byte ReadBGPaletteData()
        {
            return rawBG[bgIndex];
        }
        public byte ReadSpritePaletteData()
        {
            return rawSprite[spriteIndex];
        }

        public byte ProcessBGPaletteIndex(int pos, byte value)
        {
            bgIndex = value & 0x3F;
            bgAutoincrement = BitPosition.GetBit(value, 7);
            return value;
        }
        public byte ProcessSpritePaletteIndex(int pos, byte value)
        {
            spriteIndex = value & 0x3F;
            spriteAutoincrement = BitPosition.GetBit(value, 7);
            return value;
        }
        public byte ProcessBGPaletteData(int pos, byte value)
        {
            rawBG[bgIndex] = value;
            if (bgAutoincrement)
            {
                bgIndex++;
                if (bgIndex >= rawBG.Length)
                {
                    bgIndex = 0;
                }
            }
            return value;
        }
        public byte ProcessSpritePaletteData(int pos, byte value)
        {
            rawSprite[spriteIndex] = value;
            if (spriteAutoincrement)
            {
                spriteIndex++;
                if (spriteIndex >= rawSprite.Length)
                {
                    spriteIndex = 0;
                }
            }
            return value;
        }

        public Palette[][] RetrieveGBCPalettes()
        {
            Palette[][] res = new Palette[2][];
            res[0] = new Palette[8]; // BGP0-7
            res[1] = new Palette[8]; // OBP0-7

            for (int k = 0; k < 8; k++)
            {
                byte[] bgCols = new byte[8];
                byte[] spriteCols = new byte[8];
                Array.Copy(rawBG, k * 8, bgCols, 0, 8);
                Array.Copy(rawSprite, k * 8, spriteCols, 0, 8);
                res[0][k] = ConvertGBCPalette(bgCols);
                res[1][k] = ConvertGBCPalette(spriteCols);
            }

            return res;
        }

        private Palette ConvertGBCPalette(byte[] rawPalette)
        {
            Color[] colors = new Color[4];

            for (int k = 0; k < 4; k++)
            {
                UInt16 rawCol = (UInt16)((rawPalette[k * 2 + 1] << 8) | rawPalette[k * 2]);
                byte red = (byte)(rawCol & 0x1F);
                byte green = (byte)((rawCol & 0x3E0) >> 5);
                byte blue = (byte)((rawCol & 0x7C00) >> 10);
                red *= 8;
                green *= 8;
                blue *= 8;
                colors[k] = new Color(red, green, blue);
            }

            return new Palette(colors);
        }

        #endregion
    }
}
