﻿using GameBoyEmu.Emu.Utils;
using System;

namespace GameBoyEmu.Emu.VirtualComponents.Auxiliary
{
    public class RTC
    {
        private bool latchClockState = false;
        private bool haltTimer = false;

        // 0:08h, 4: 0Ch
        //08h RTC S Seconds   0-59 (0-3Bh)
        //09h RTC M Minutes   0-59 (0-3Bh)
        //0Ah RTC H Hours     0-23 (0-17h)
        //0Bh RTC DL Lower 8 bits of Day Counter(0-FFh)
        //0Ch RTC DH Upper 1 bit of Day Counter, Carry Bit, Halt Flag
        //      Bit 0  Most significant bit of Day Counter(Bit 8)
        //      Bit 6  Halt(0=Active, 1=Stop Timer)
        //      Bit 7  Day Counter Carry Bit(1=Counter Overflow)
        private byte[] RTCRegisters = new byte[5];

        public void Latch(byte value)
        {
            bool state = value != 0;
            if (state && !latchClockState)
            {
                DumpTimeToRegisters();
            }
            latchClockState = state;
        }

        private void DumpTimeToRegisters()
        {
            DateTime time = DateTime.Now;
            RTCRegisters[0] = (byte)time.Second;
            RTCRegisters[1] = (byte)time.Minute;
            RTCRegisters[2] = (byte)time.Hour;
            RTCRegisters[3] = (byte)(time.DayOfYear & 0xFF);

            byte lastRegister = 0;
            lastRegister = BitPosition.SetBit(lastRegister, 6, haltTimer);
            lastRegister = BitPosition.SetBit(lastRegister, 0, (time.DayOfYear >> 8 & 0x1) == 0x1);
            // No carry, because it didn't happen in this scenario: useful only when calculating dates

            RTCRegisters[4] = lastRegister;
        }

        /// <summary>
        /// Returns the requested RTC register. id ranges from 0 to 4 and maps to 0x8-0xC.
        /// </summary>
        public byte GetRegister(int id)
        {
            return RTCRegisters[id];
        }

        public void WriteRegister(int id, byte value)
        {
            RTCRegisters[id] = value;
        }
    }
}
