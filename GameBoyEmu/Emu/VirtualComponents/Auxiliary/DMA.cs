﻿using GameBoyEmu.Emu.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameBoyEmu.Emu.VirtualComponents.Auxiliary
{
    public class DMA
    {
        private GameBoy GB;

        public bool OAMActive { get; private set; } = false;
        private UInt16 OAMSource;
        private byte OAMLength;
        private byte OAMOffset;
        private bool OAMIsFirstTick = false;
        int OAMInternalClock;

        public bool IsOAMInProgress
        {
            get
            {
                return OAMActive && !OAMIsFirstTick && OAMInternalClock >= 0;
            }
        }

        public DMA(GameBoy gb)
        {
            GB = gb;
        }

        //OAM works like this: it is started 1 cycle after it is requested, excluding the cycle starting the request itself.
        public void StartOAMTransfer(byte value)
        {
            if (value > 0xF1)
            {
                return;
            }
            UInt16 srcAddress = (UInt16)(value * 0x100);
            OAMActive = true;
            OAMSource = srcAddress;
            OAMLength = 0xA0;
            OAMOffset = 0;
            OAMIsFirstTick = true;
            OAMInternalClock = -4; // This introduces an additional delay of one cycle.
        }

        public void TickOAM(int clock)
        {
            if (!OAMActive) { return; }
            if (OAMIsFirstTick)
            {
                OAMIsFirstTick = false; // This means this is getting called right after starting the transfer (same cycle). It's supposed to start on next cycle.
                return;
            }
            OAMInternalClock += clock;
            int len = OAMInternalClock / 3;
            if (len > OAMLength - OAMOffset) { len = OAMLength - OAMOffset; }
            if (len<=0) { return; }

            byte[] buffer = GB.Ram.ReadBlock(OAMSource + OAMOffset, len, true);
            GB.Ram.WriteBlock(FixedAddresses.OAM.Start + OAMOffset, buffer, true);

            OAMOffset += (byte)len;
            if (OAMOffset >= OAMLength)
            {
                OAMActive = false;
                return;
            }
            OAMInternalClock -= len * 3;
        }


        public byte ExecuteVramDMA(byte startLength)
        {
            byte oldStatus = GB.Ram.ReadByte(FixedAddresses.VRAM_DMA_START_AND_LENGTH, true);
            bool onHBlank = BitPosition.GetBit(startLength, 7);

            if (!BitPosition.GetBit(oldStatus, 7) && !onHBlank)
            {
                //This means a transfer was running and we wrote 0 to bit 7, which means to stop!
                return GB.Gpu.StopVramDMA();
            }

            byte srcHigh = GB.Ram.ReadByte(FixedAddresses.VRAM_DMA_SOURCE_HIGH, true);
            byte srcLow = GB.Ram.ReadByte(FixedAddresses.VRAM_DMA_SOURCE_LOW, true);
            byte dstHigh = GB.Ram.ReadByte(FixedAddresses.VRAM_DMA_DESTINATION_HIGH, true);
            byte dstLow = GB.Ram.ReadByte(FixedAddresses.VRAM_DMA_DESTINATION_LOW, true);

            // TODO: if onHBlank, delegate to GPU through event
            UInt16 src = (UInt16)((srcHigh << 8) | (srcLow & 0xF0));
            UInt16 dst = (UInt16)(((dstHigh & 0x1F) << 8) | (dstLow & 0xF0));
            dst += 0x8000;
            int len = ((startLength & 0x7F) + 1) * 0x10;

            if (onHBlank)
            {
                GB.Gpu.QueueVramDMA(src, dst, len);
                return 0x00;
            }
            else
            {
                byte[] data = GB.Ram.ReadBlock(src, len, true);
                GB.Ram.WriteBlock(dst, data, true);
                return 0xFF;
            }
        }
    }
}
