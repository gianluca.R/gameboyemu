﻿using System.IO;

namespace GameBoyEmu.Emu.VirtualComponents.Auxiliary
{
    public class Cartridge
    {
        public string FileName;
        public string SaveFileName
        {
            get
            {
                return Path.GetDirectoryName(FileName) + "\\" + Path.GetFileNameWithoutExtension(FileName) + ".sav";
            }
        }
        private FileStream fileStream;
        private BinaryReader reader;

        public Cartridge(string file)
        {
            this.FileName = file;
            this.fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read);
            this.reader = new BinaryReader(this.fileStream);
        }

        public byte[] ReadAll()
        {
            //Now, this isn't a problem as a GB rom is much smaller than what an int can represent, no data is lost on cast
            byte[] res = reader.ReadBytes((int)fileStream.Length);
            reader.BaseStream.Position = 0;
            return res;
        }
    }
}
