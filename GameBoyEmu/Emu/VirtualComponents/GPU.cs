﻿using GameBoyEmu.Emu.Rendering;
using GameBoyEmu.Emu.Utils;
using GameBoyEmu.Emu.VirtualComponents.Auxiliary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace GameBoyEmu.Emu.VirtualComponents
{
    public class GPU
    {
        private GameBoy GB;
        private Renderer renderer;
        private Action<BitmapSource, BitmapSource> renderMethod;

        public const int HBLANK_CLOCK_COST = 85; // 85 up to 208
        public const int VBLANK_CLOCK_COST = 456;
        public const int OAM_CLOCK_COST = 80; 
        public const int VRAM_CLOCK_COST = 168;// 168 up to 291

        public const bool RenderingEnabled = true;
        public Display Display;

        // DMA
        private bool VramDMAEnabled = false;
        private UInt16 VramDMASource;
        private UInt16 VramDMADestination;
        private int VramDMALength;

        public int VideoClock = 0;
        public int FrameLimit { get; set; } = 63;
        private Stopwatch lastFrame = new Stopwatch();
        private Stopwatch last100Frames = new Stopwatch();
        private int frameCount = 0;
        public double FPS { get; private set; }

        public delegate VRAMSnapshot VRAMReadHandler();
        public event VRAMReadHandler OnReadVRAM;

        public GPU(GameBoy gb, Action<BitmapSource, BitmapSource> renderMethod, bool gbcMode)
        {
            this.GB = gb;
            this.renderer = new Renderer(gb);
            this.renderMethod = renderMethod;
            this.renderer.GBCMode = gbcMode;
            this.Display = new Display(gb);
        }

        public void ExecuteGPU(int clockStep)
        {
            // If display is disabled, we do nothing!
            if (!Display.IsDisplayEnabled)
            {
                return;
            }
            VideoClock += clockStep;

            switch (Display.DisplayMode)
            {
                case 0:
                    HBlank(false);
                    break;
                case 1:
                    VBlank(false);
                    break;
                case 2:
                    OAMRead(false);
                    break;
                case 3:
                    VRAMRead(false);
                    break;
            }
        }

        public void TriggerHBlank()
        {
            Display.DisplayMode = 0;
            if (Display.LY < 144)
            {
                RenderLine(Display.LY);
            }
            Display.TriggerHBlankInterruptIfEnabled();
        }


        public void TriggerVBlank()
        {
            Display.DisplayMode = 1;
            GB.Interrupt.RequestInterrupt(InterruptService.VBLANK_INTERRUPT);

            RenderToScreen();

            if (FrameLimit > 0)
            {
                if (!lastFrame.IsRunning)
                {
                    lastFrame.Start();
                }
                else
                {
                    while (lastFrame.ElapsedMilliseconds < 1000.0 / FrameLimit) { }
                    lastFrame.Restart();
                }
            }


            if (!last100Frames.IsRunning)
            {
                last100Frames.Start();
            }
            else if (frameCount >= 100)
            {
                double elapsed = last100Frames.ElapsedMilliseconds;
                FPS = Math.Round(1000.0 / (elapsed / frameCount), 2);
                last100Frames.Restart();
                frameCount = 0;
            }
            frameCount++;

            Display.TriggerVBlankInterruptIfEnabled();
        }

        public void TriggerOAM()
        {
            Display.DisplayMode = 2;
            Display.TriggerOAMInterruptIfEnabled();

            byte[] oam = GB.Ram.ReadBlock(FixedAddresses.OAM.Start, FixedAddresses.OAM.Length, true);
            renderer.updateOAM(oam);
        }

        public void TriggerVRAM()
        {
            Display.DisplayMode = 3;

            renderer.updateVRAM(OnReadVRAM());
        }

        private void HBlank(bool skip)
        {
            if (VideoClock >= HBLANK_CLOCK_COST || skip)
            {
                if (!skip) { VideoClock -= HBLANK_CLOCK_COST; }
                Display.LY++;
                if (Display.LY < 144)
                {
                    StepVramDMA();
                    TriggerOAM();
                }
                else
                {
                    TriggerVBlank();
                }
            }
        }

        private void VBlank(bool skip)
        {
            if (VideoClock >= VBLANK_CLOCK_COST || skip)
            {
                if (!skip) { VideoClock -= VBLANK_CLOCK_COST; }
                Display.LY++;
                if (Display.LY > 153)
                {
                    renderer.ResetFrame();
                    Display.LY = 0;
                    TriggerOAM();
                }
            }
        }

        private void OAMRead(bool skip)
        {
            if (VideoClock >= OAM_CLOCK_COST || skip)
            {
                if (!skip) { VideoClock -= OAM_CLOCK_COST; }
                TriggerVRAM();
            }
        }

        private void VRAMRead(bool skip)
        {
            if (VideoClock >= VRAM_CLOCK_COST || skip)
            {
                if (!skip) { VideoClock -= VRAM_CLOCK_COST; }
                TriggerHBlank();
            }
        }

        public void QueueVramDMA(UInt16 src, UInt16 dst, int len)
        {
            VramDMAEnabled = true;
            VramDMASource = src;
            VramDMADestination = dst;
            VramDMALength = len;
        }
        public byte StopVramDMA()
        {
            VramDMAEnabled = false;
            byte status = (byte)((VramDMALength > 0) ? (VramDMALength / 0x10) - 1 : 0);
            status = BitPosition.SetBit(status, 7, !VramDMAEnabled);
            return status;
        }
        private void StepVramDMA()
        {
            if (!VramDMAEnabled) { return; }

            byte[] data = GB.Ram.ReadBlock(VramDMASource, 0x10, true);
            GB.Ram.WriteBlock(VramDMADestination, data, true);
            VramDMALength -= 0x10;
            VramDMASource += 0x10;
            VramDMADestination += 0x10;
            if (VramDMALength <= 0) { VramDMAEnabled = false; }
            byte status = (byte)((VramDMALength > 0) ? (VramDMALength / 0x10) - 1 : 0);
            status = BitPosition.SetBit(status, 7, !VramDMAEnabled);
            GB.Ram.WriteByte(FixedAddresses.VRAM_DMA_START_AND_LENGTH, status, true);
        }

        #region Rendering

        public void RenderLine(int line)
        {
            if (RenderingEnabled)
            {
                new Task(() => { renderer.RenderScanline(line); }).Start();
            }
        }
        public void RenderToScreen()
        {
            if (RenderingEnabled)
            {
                try
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        renderMethod(renderer.PrintBuffer(), null);
                    });
                }
                catch { }
            }
        }

        public BitmapSource GetTileMapImage(int tileDataId, int paletteId)
        {
            int paletteCategory = 0;
            if (paletteId > 7)
            {
                paletteId -= 8;
                paletteCategory = 1;
            }
            return renderer.RenderTileMap(GB.Ram, tileDataId, paletteCategory, paletteId);
        }
        public BitmapSource GetBGImage(int bgMapId, int tileDataId)
        {
            return renderer.RenderBGMap(bgMapId, tileDataId);
        }

        #endregion
    }
}

