﻿using GameBoyEmu.Emu.Instructions;
using GameBoyEmu.Emu.Optimization;
using GameBoyEmu.Emu.Structs;
using GameBoyEmu.Emu.Utils;
using GameBoyEmu.Emu.VirtualComponents.Auxiliary;
using System;
using System.Collections.Generic;
using System.Linq;
using static GameBoyEmu.Emu.Optimization.LoopSkipper;

namespace GameBoyEmu.Emu.VirtualComponents
{
    public class CPU
    {
        public const int Frequency = 4194304; // GB Hz
        public static bool DoubleSpeed = false; // TODO: turn me non static
        public double SpeedMultiplier { get { return DoubleSpeed ? 2.0 : 1.0; } }

        private readonly RAM ram;
        private readonly GPU gpu;
        private readonly Registers registers;
        private readonly Timer timer;
        private readonly InstructionSet instructionSet = new InstructionSet();
        private readonly LoopSkipper loopSkipperOptimizer = new LoopSkipper();

        // History keeping
        private System.Diagnostics.Stopwatch last100KInstructions = new System.Diagnostics.Stopwatch();
        private int instructionCount = 0;

        public double CPS { get; private set; }

        public CPU(RAM ram, Registers registers, GPU gpu, Timer timer, UInt16 entryPoint)
        {
            this.ram = ram;
            this.registers = registers;
            this.gpu = gpu;
            this.timer = timer;
            this.registers.PC.Full = entryPoint;
        }

        public int ExecuteCPU()
        {
            if (registers.Halted)
            {
                // If we're halted we won't do anything here. Now we're waiting for an interrupt to make us jump somewhere and reset this flag.
                timer.Tick(Frequency / 1048576);
                return Frequency / 1048576;
            }
            UInt16 startPos = registers.PC.Full;

            bool isExtended = false;
            byte opCode = InstructionAccessors.ReadByteAndIncreasePC(ram, registers, ExecutionParameters.DEFAULT);
            Dictionary<byte, InstructionDescriptor> _instructionSet = instructionSet.instructionSet;

            if (opCode == InstructionSet.ExtendedOpCode)
            {
                // This means we're using instruction from the extended set, let's work with that then
                opCode = InstructionAccessors.ReadByteAndIncreasePC(ram, registers, ExecutionParameters.DEFAULT); // We read our opCode again, this time it's the actual extended instruction identifier
                _instructionSet = instructionSet.extendedInstructionSet;
                isExtended = true;
            }
            if (!_instructionSet.ContainsKey(opCode))
            {
                throw new Exception("Unknown " + (isExtended ? "extended" : "") + " opCode " + opCode.ToString("x") + " encountered.");
            }
            InstructionDescriptor curInstruction = _instructionSet[opCode];


            InstructionOutput output = curInstruction.action?.Invoke(ram, registers, ExecutionParameters.DEFAULT);
            timer.Tick(curInstruction.clock);

            if (!last100KInstructions.IsRunning)
            {
                last100KInstructions.Start();
            }
            else if (instructionCount >= 100000)
            {
                double elapsed = last100KInstructions.ElapsedMilliseconds;
                CPS = Math.Round(1000.0 / (elapsed / instructionCount), 2);
                last100KInstructions.Restart();
                instructionCount = 0;
            }
            instructionCount++;

            return curInstruction.clock;
        }
    }
}
