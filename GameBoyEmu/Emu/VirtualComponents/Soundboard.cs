﻿using GameBoyEmu.Emu.Audio;
using GameBoyEmu.Emu.Audio.SampleProviders;
using GameBoyEmu.Emu.Utils;
using NAudio.Wave;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace GameBoyEmu.Emu.VirtualComponents
{
    // TODO: split me in classes with less responsibility
    public class Soundboard
    {
        bool SoundEnabled = false;
        ChannelData[] Channels;

        // Channel generators
        SweepSignalGenerator Channel1;
        SweepSignalGenerator Channel2;
        CustomWaveGenerator Channel3;
        NoiseSignalGenerator Channel4;

        WaveOut Device;
        AudioMixer ChannelMixer;

        public Soundboard()
        {
            Channels = new ChannelData[4];

            Channels[0].ReferenceFrequency = 131072.0;
            Channel1 = new SweepSignalGenerator(SweepSignalGenerator.WaveForm.Square);
            Channels[0].VolumeEnvelope = new VolumeEnvelopeProvider(Channel1);

            Channels[1].ReferenceFrequency = 131072.0;
            Channel2 = new SweepSignalGenerator(SweepSignalGenerator.WaveForm.Square);
            Channels[1].VolumeEnvelope = new VolumeEnvelopeProvider(Channel2);

            Channel3 = new CustomWaveGenerator(32);
            Channels[2].ReferenceFrequency = 65536.0;

            Channel4 = new NoiseSignalGenerator();
            Channels[3].VolumeEnvelope = new VolumeEnvelopeProvider(Channel4);

            ChannelMixer = new AudioMixer(new ISampleProvider[] {
                Channels[0].VolumeEnvelope,
                Channels[1].VolumeEnvelope,
                Channel3,
                Channels[3].VolumeEnvelope
            });

            Channels[0].Enabled = true;
            Channels[1].Enabled = true;
            Channels[2].Enabled = true;
            Channels[3].Enabled = true;

            Device = new WaveOut();
            Device.DesiredLatency = 50;
            Device.NumberOfBuffers = 2;
            Device.Init(ChannelMixer);
            Device.Play();
            Device.Volume = 0.3f;
        }

        public void Dispose()
        {
            for (int k = 0; k < Channels.Length; k++)
            {
                if (Device != null)
                {
                    Device.Dispose();
                }
            }
        }

        public byte GetSoundControlRegister()
        {
            byte res = 0;
            res = BitPosition.SetBit(res, 7, SoundEnabled);
            for (int k = 0; k < 4; k++)
            {
                if (Channels[k].PlayOnlyOnce && Channels[k].IsOn)
                {
                    double elapsed = DateTime.Now.Subtract(Channels[k].TimeStarted).TotalSeconds;
                    if (elapsed >= Channels[k].SoundLength)
                    {
                        Channels[k].IsOn = false;
                    }
                }
                res = BitPosition.SetBit(res, k, Channels[k].IsOn);
            }
            return res;
        }

        public byte ProcessRegister(int registerAddress, byte value)
        {
            switch (registerAddress)
            {
                case FixedAddresses.SOUND_C1_SWEEP_REGISTER:
                    return ProcessSweepRegister(0, value);
                case FixedAddresses.SOUND_C1_DUTY_AND_LENGHT:
                    return ProcessDutyAndLength(0, value);
                case FixedAddresses.SOUND_C1_VOLUME_ENVELOPE:
                    return ProcessVolumeEnvelope(0, value);
                case FixedAddresses.SOUND_C1_FREQUENCY_LOW:
                    return ProcessFrequencyLow(0, value);
                case FixedAddresses.SOUND_C1_FREQUENCY_HIGH_AND_CONTROL:
                    return ProcessFrequencyHighAndControl(0, value);

                case FixedAddresses.SOUND_C2_DUTY_AND_LENGHT:
                    return ProcessDutyAndLength(1, value);
                case FixedAddresses.SOUND_C2_VOLUME_ENVELOPE:
                    return ProcessVolumeEnvelope(1, value);
                case FixedAddresses.SOUND_C2_FREQUENCY_LOW:
                    return ProcessFrequencyLow(1, value);
                case FixedAddresses.SOUND_C2_FREQUENCY_HIGH_AND_CONTROL:
                    return ProcessFrequencyHighAndControl(1, value);

                case FixedAddresses.SOUND_C3_ON_OFF:
                    return ProcessOnOff(2, value);
                case FixedAddresses.SOUND_C3_LENGHT:
                    return ProcessLengthOnly(2, true, value);
                case FixedAddresses.SOUND_C3_VOLUME:
                    return ProcessSimpleVolume(2, value);
                case FixedAddresses.SOUND_C3_FREQUENCY_LOW:
                    return ProcessFrequencyLow(2, value);
                case FixedAddresses.SOUND_C3_FREQUENCY_HIGH_AND_CONTROL:
                    return ProcessFrequencyHighAndControl(2, value);

                case FixedAddresses.SOUND_C4_LENGHT:
                    return ProcessLengthOnly(3, false, value);
                case FixedAddresses.SOUND_C4_VOLUME_ENVELOPE:
                    return ProcessVolumeEnvelope(3, value);
                case FixedAddresses.SOUND_C4_POLYNOMIAL_COUNTER:
                    return ProcessPolynomialCounter(3, value);
                case FixedAddresses.SOUND_C4_CONTROL:
                    return ProcessControlOnly(3, value);

                case FixedAddresses.SOUND_CHANNEL_CONTROL_REGISTER:
                    return ProcessSoundChannelControlRegister(value);
                case FixedAddresses.SOUND_OUTPUT_REGISTER:
                    return ProcessSoundOutputRegister(value);
                case FixedAddresses.SOUND_CONTROL_REGISTER:
                    return ProcessSoundControlRegister(value);
            }
            return value;
        }

        public byte ProcessWaveData(int pos, byte value)
        {
            pos -= FixedAddresses.SOUND_C3_WAVE_START;

            // Each byte written has to be converted in 2 wave table entries, 4 bits each
            pos *= 2; // relative position in the wave table buffer
            Channel3.WaveTable[pos] = (byte)((value & 0xF0) >> 4);
            Channel3.WaveTable[pos + 1] = (byte)(value & 0xF);
            return value;
        }

        byte ProcessSoundChannelControlRegister(byte value)
        {
            int rightChannel = (value & 0x70) >> 4;
            int leftChannel = value & 0x7;
            ChannelMixer.LeftVolume = leftChannel / 7f;
            ChannelMixer.RightVolume = rightChannel / 7f;
            return value;
        }

        byte ProcessSoundOutputRegister(byte value)
        {
            ChannelMixer.SourceToLeft[0] = BitPosition.GetBit(value, 0);
            ChannelMixer.SourceToLeft[1] = BitPosition.GetBit(value, 1);
            ChannelMixer.SourceToLeft[2] = BitPosition.GetBit(value, 2);
            ChannelMixer.SourceToLeft[3] = BitPosition.GetBit(value, 3);
            ChannelMixer.SourceToRight[0] = BitPosition.GetBit(value, 4);
            ChannelMixer.SourceToRight[1] = BitPosition.GetBit(value, 5);
            ChannelMixer.SourceToRight[2] = BitPosition.GetBit(value, 6);
            ChannelMixer.SourceToRight[3] = BitPosition.GetBit(value, 7);
            return value;
        }

        byte ProcessSoundControlRegister(byte value)
        {
            bool newSoundEnabledValue = BitPosition.GetBit(value, 7);
            if (SoundEnabled && !newSoundEnabledValue)
            {
                ResetAllSound();
            }
            else if (!SoundEnabled && newSoundEnabledValue)
            {
                if (Device.PlaybackState != PlaybackState.Stopped)
                {
                    Device.Stop();
                }

                Device.Init(ChannelMixer);
                Device.Play();
            }
            SoundEnabled = newSoundEnabledValue;
            return 0; // No need to return this, as it will be overridden by GetSoundControlRegister()
        }

        void ResetAllSound()
        {
            for (int k = 0; k < Channels.Length; k++)
            {
                Channels[k].FrequencyHigh = 0;
                Channels[k].FrequencyLow = 0;
                Channels[k].SoundLength = 0;
                Channels[k].IsOn = false;
            }
            Channel1.Reset();
            Channel2.Reset();
            Channel3.Reset();
            Channel4.Reset();
            if (Device.PlaybackState != PlaybackState.Stopped)
            {
                Device.Stop();
            }
        }

        byte ProcessOnOff(int channel, byte value)
        {
            if (channel == 2)
            {
                if (BitPosition.GetBit(value, 7))
                {
                    GetChannel(channel).Play();
                }
                else
                {
                    GetChannel(channel).Pause();
                }
                return (byte)(value & 0x80);
            }
            else
            {
                throw new Exception("On/off not supported on channel " + channel);
            }
        }

        byte ProcessSimpleVolume(int channel, byte value)
        {
            if (channel == 2)
            {
                int volume = (value & 0x60) >> 5;
                if (volume > 0)
                {
                    Channel3.Volume = (100 >> (volume - 1)) / 100.0;
                }
                else
                {
                    Channel3.Volume = 0;
                }
                return (byte)(value & 0x60);
            }
            else
            {
                throw new Exception("Simple volume not supported on channel " + channel);
            }
        }

        byte ProcessSweepRegister(int channel, byte value)
        {
            int sweepTime = (value & 0x70) >> 4;
            if (channel == 0)
            {
                Channel1.SweepIncrease = !BitPosition.GetBit(value, 3);
                Channel1.SweepShiftCount = value & 0x7;
                Channel1.SweepTime = sweepTime / 128.0;
            }
            else
            {
                throw new Exception("Sweep not supported on channel " + channel);
            }
            return value;
        }

        byte ProcessDutyAndLength(int channel, byte value)
        {
            SweepSignalGenerator ch;
            if (channel == 0) { ch = Channel1; }
            else if (channel == 1) { ch = Channel2; }
            else
            {
                throw new Exception("Duty not supported on channel " + channel);
            }
            int duty = (value & 0xC0) >> 6;
            switch (duty)
            {
                case 0:
                    ch.WaveDuty = 0.125;
                    break;
                case 1:
                    ch.WaveDuty = 0.25;
                    break;
                case 2:
                    ch.WaveDuty = 0.5;
                    break;
                case 3:
                    ch.WaveDuty = 0.75;
                    break;
            }
            ProcessLengthOnly(channel, false, value);
            return (byte)(value & 0xC0); // only duty can be read, no need to save it in ram
        }

        byte ProcessLengthOnly(int channel, bool extended, byte value)
        {
            if (extended)
            {
                Channels[channel].SoundLength = (256.0 - value) * (1.0 / 256.0);
                return value;
            }
            else
            {
                int len = value & 0x3F;
                Channels[channel].SoundLength = (64.0 - len) * (1.0 / 256.0);
                return (byte)(value & 0x3F);
            }
        }

        byte ProcessVolumeEnvelope(int channel, byte value)
        {
            int volume = (value & 0xF0) >> 4;
            Channels[channel].VolumeEnvelope.VolumeEnvelopeIncrease = BitPosition.GetBit(value, 3);
            Channels[channel].VolumeEnvelope.VolumeEnvelopeCount = value & 0x7;
            Channels[channel].VolumeEnvelope.Volume = volume / 15f;
            return value;
        }

        // TODO: also ugly, improve
        byte ProcessFrequencyLow(int channel, byte value)
        {
            Channels[channel].FrequencyLow = value;
            UpdateChannelFrequency(channel);
            return 0x00;
        }

        byte ProcessFrequencyHighAndControl(int channel, byte value)
        {
            Channels[channel].FrequencyHigh = (byte)(value & 0x7);
            UpdateChannelFrequency(channel);
            ProcessControlOnly(channel, value);
            return (byte)(value & BitPosition.BIT_6);
        }

        byte ProcessControlOnly(int channel, byte value)
        {
            bool restart = BitPosition.GetBit(value, 7);
            Channels[channel].PlayOnlyOnce = BitPosition.GetBit(value, 6);
            if (restart)
            {
                RestartChannel(channel);
            }
            return (byte)(value & BitPosition.BIT_6);
        }

        byte ProcessPolynomialCounter(int channel, byte value)
        {
            int frequencyShift = (value & 0xF0) >> 4;
            int divisor = value & 0x7;
            if (channel == 3)
            {
                Channel4.NoiseTimerDivisor = (divisor == 0) ? 8 : divisor * 16;
                Channel4.SmallNoiseStep = BitPosition.GetBit(value, 3);
                Channel4.ClockShift = frequencyShift;
            }
            else
            {
                throw new Exception("Polynomial counter not supported on channel " + channel);
            }
            return value;
        }

        void RestartChannel(int channel)
        {
            Channels[channel].IsOn = true;
            Channels[channel].VolumeEnvelope?.Reset();
            ISampleProvider wave = Channels[channel].VolumeEnvelope != null ? Channels[channel].VolumeEnvelope : GetChannel(channel);

            if (Channels[channel].PlayOnlyOnce)
            {
                wave = wave.Take(TimeSpan.FromSeconds(Channels[channel].SoundLength));
                Channels[channel].TimeStarted = DateTime.Now;
            }
            ChannelMixer.Sources[channel] = wave;


            var ch = GetChannel(channel);
            ch.Stop();
            if (Channels[channel].Enabled)
            {
                ch.Play();
            }
        }

        void UpdateChannelFrequency(int channel)
        {
            if (channel == 0)
            {
                Channel1.InitialFrequency = Channels[channel].Frequency;
                Channel1.Frequency = Channels[channel].Frequency;
            }
            else if (channel == 1)
            {
                Channel2.InitialFrequency = Channels[channel].Frequency;
                Channel2.Frequency = Channels[channel].Frequency;
            }
            else if (channel == 2)
            {
                Channel3.Frequency = Channels[channel].Frequency;
            }
            else
            {
                throw new Exception("Frequency not supported on channel " + channel);
            }
        }
        IExtendedSampleProvider GetChannel(int channel)
        {
            if (channel == 0)
            {
                return Channel1;
            }
            else if (channel == 1)
            {
                return Channel2;
            }
            else if (channel == 2)
            {
                return Channel3;
            }
            else
            {
                return Channel4;
            }
        }

    }


}
