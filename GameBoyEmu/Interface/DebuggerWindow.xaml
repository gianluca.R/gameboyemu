﻿<Window x:Class="GameBoyEmu.Interface.DebuggerWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:GameBoyEmu.Interface"
        mc:Ignorable="d"
        Title="DebuggerWindow" Height="450" Width="800" Closing="Window_Closing">

    <Window.CommandBindings>
        <CommandBinding Command="local:DebuggerCommands.Restart" Executed="CommandRestart_Executed" CanExecute="CommandRestart_CanExecute" />
        <CommandBinding Command="local:DebuggerCommands.Pause" Executed="CommandPause_Executed" CanExecute="CommandPause_CanExecute" />
        <CommandBinding Command="local:DebuggerCommands.Resume" Executed="CommandResume_Executed" CanExecute="CommandResume_CanExecute" />
        <CommandBinding Command="local:DebuggerCommands.StepInto" Executed="CommandStepInto_Executed" CanExecute="CommandStepInto_CanExecute" />
        <CommandBinding Command="local:DebuggerCommands.StepOver" Executed="CommandStepOver_Executed" CanExecute="CommandStepOver_CanExecute" />
        <CommandBinding Command="local:DebuggerCommands.ToggleBreakpoint" Executed="CommandToggleBreakpoint_Executed" CanExecute="CommandToggleBreakpoint_CanExecute" />
    </Window.CommandBindings>
    <Window.Resources>
        <local:MultiValueEqualityConverter x:Key="multiValueEqualityConverter"></local:MultiValueEqualityConverter>
        <local:ElementContainedInListConverter x:Key="elementContainedInListConverter"></local:ElementContainedInListConverter>
    </Window.Resources>

    <Grid>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="*"/>
            <ColumnDefinition Width="190"/>
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
            <RowDefinition Height="23"/>
            <RowDefinition Height="30"/>
            <RowDefinition Height="55*"/>
            <RowDefinition Height="128*"/>
        </Grid.RowDefinitions>
        <Menu  Height="23" VerticalAlignment="Top" Grid.Row="0" Grid.ColumnSpan="2">
            <MenuItem Header="File"/>
        </Menu>

        <ToolBarPanel Grid.Row="1" Grid.ColumnSpan="2">
            <ToolBar HorizontalAlignment="Left" Width="300">
                <Button Content="Restart" Command="local:DebuggerCommands.Restart"/>
                <Button Content="Pause" Command="local:DebuggerCommands.Pause"/>
                <Button Content="Resume" Command="local:DebuggerCommands.Resume"/>
                <Button Content="Step Into" Command="local:DebuggerCommands.StepInto"/>
                <Button Content="Step Over" Command="local:DebuggerCommands.StepOver"/>
                <Button Content="Toggle Breakpoint" Command="local:DebuggerCommands.ToggleBreakpoint"/>
            </ToolBar>
        </ToolBarPanel>

        <ListBox x:Name="listInstructions" Grid.Row="2" Grid.RowSpan="2">
            <ListBox.Resources>
                <Style TargetType="ListBoxItem">
                    <Setter Property="Template">
                        <Setter.Value>
                            <ControlTemplate TargetType="{x:Type ListBoxItem}">
                                <Border x:Name="Bd"
                            BorderBrush="{TemplateBinding BorderBrush}"
                            BorderThickness="{TemplateBinding BorderThickness}"
                            Background="{TemplateBinding Background}"
                            Padding="{TemplateBinding Padding}"
                            SnapsToDevicePixels="true">
                                    <ContentPresenter HorizontalAlignment="{TemplateBinding HorizontalContentAlignment}"
                                            SnapsToDevicePixels="{TemplateBinding SnapsToDevicePixels}"
                                            VerticalAlignment="{TemplateBinding VerticalContentAlignment}" />
                                </Border>
                                <ControlTemplate.Triggers>
                                    <MultiTrigger>
                                        <MultiTrigger.Conditions>
                                            <Condition Property="Selector.IsSelectionActive"
                                            Value="False" />
                                            <Condition Property="IsSelected"
                                            Value="True" />
                                        </MultiTrigger.Conditions>
                                        <Setter Property="BorderBrush" TargetName="Bd" Value="DarkBlue" />
                                        <Setter Property="Background" TargetName="Bd" Value="Transparent" />
                                    </MultiTrigger>
                                    <MultiTrigger>
                                        <MultiTrigger.Conditions>
                                            <Condition Property="Selector.IsSelectionActive"
                                            Value="True" />
                                            <Condition Property="IsSelected"
                                            Value="True" />
                                        </MultiTrigger.Conditions>
                                        <Setter Property="BorderBrush" TargetName="Bd" Value="DarkBlue" />
                                        <Setter Property="Background" TargetName="Bd" Value="Transparent" />
                                    </MultiTrigger>
                                </ControlTemplate.Triggers>
                            </ControlTemplate>
                        </Setter.Value>
                    </Setter>

                    <Style.Triggers>
                        <DataTrigger Value="True">
                            <DataTrigger.Binding>
                                <MultiBinding Converter="{StaticResource multiValueEqualityConverter}">
                                    <Binding Path="RawAddress" Mode="OneWay" />
                                    <Binding RelativeSource="{RelativeSource Mode=FindAncestor, AncestorType=local:DebuggerWindow}" Path="CurrentPC" Mode="OneWay"/>
                                </MultiBinding>
                            </DataTrigger.Binding>
                            <Setter Property="Foreground" Value="Red"/>
                            <Setter Property="FontWeight" Value="Bold"/>
                        </DataTrigger>
                        <DataTrigger Value="True">
                            <DataTrigger.Binding>
                                <MultiBinding Converter="{StaticResource elementContainedInListConverter}">
                                    <Binding Path="RawAddress" Mode="OneWay" />
                                    <Binding RelativeSource="{RelativeSource Mode=FindAncestor, AncestorType=local:DebuggerWindow}" Path="GB.Breakpoints" Mode="OneWay"/>
                                </MultiBinding>
                            </DataTrigger.Binding>
                            <Setter Property="Background" Value="DarkRed"/>
                            <Setter Property="Foreground" Value="White"/>
                            <Setter Property="Foreground" Value="Red"/>
                        </DataTrigger>
                    </Style.Triggers>
                </Style>
            </ListBox.Resources>
            <ListBox.ItemTemplate>
                <DataTemplate>
                    <Grid Margin="0,2">
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="50" />
                            <ColumnDefinition Width="70" />
                            <ColumnDefinition Width="120" />
                            <ColumnDefinition Width="150" />
                            <ColumnDefinition Width="150" />
                            <ColumnDefinition Width="*" />
                        </Grid.ColumnDefinitions>
                        <TextBlock Text="{Binding Address}" />
                        <TextBlock Grid.Column="1" Text="{Binding Hex}" />
                        <TextBlock Grid.Column="2" Text="{Binding Translated}" />
                        <TextBlock Grid.Column="3" Text="{Binding DataRead}" />
                        <TextBlock Grid.Column="4" Text="{Binding DataWrite}" />
                        <TextBlock Grid.Column="5" Text="{Binding Comment}" />
                    </Grid>
                </DataTemplate>
            </ListBox.ItemTemplate>
            <ListBox.ItemsPanel>
                <ItemsPanelTemplate>
                    <VirtualizingStackPanel />
                </ItemsPanelTemplate>
            </ListBox.ItemsPanel>
        </ListBox>
        <GroupBox Grid.Column="1" Header="Registers" Height="95" Margin="3,3,3,0" Grid.Row="2" VerticalAlignment="Top">
            <Grid>
                <Grid.RowDefinitions>
                    <RowDefinition Height="1*"/>
                    <RowDefinition Height="1*"/>
                    <RowDefinition Height="1*"/>
                </Grid.RowDefinitions>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="1*"/>
                    <ColumnDefinition Width="1*"/>
                    <ColumnDefinition Width="1*"/>
                    <ColumnDefinition Width="1*"/>
                </Grid.ColumnDefinitions>
                <Label Content="AF" Grid.Row="0" Grid.Column="0" HorizontalAlignment="Right"/>
                <TextBox x:Name="txtRegisterAF" Grid.Row="0" Grid.Column="1" Margin="3" TextWrapping="Wrap"/>

                <Label Content="PC" Grid.Row="0" Grid.Column="2" HorizontalAlignment="Right" Grid.RowSpan="3"/>
                <TextBox x:Name="txtRegisterPC" Grid.Row="0" Grid.Column="3" Margin="3" TextWrapping="Wrap"/>

                <Label Content="BC" Grid.Row="1" Grid.Column="0" HorizontalAlignment="Right" Grid.RowSpan="3"/>
                <TextBox x:Name="txtRegisterBC" Grid.Row="1" Grid.Column="1" Margin="3" TextWrapping="Wrap"/>

                <Label Content="DE" Grid.Row="1" Grid.Column="2" HorizontalAlignment="Right" Grid.RowSpan="3"/>
                <TextBox x:Name="txtRegisterDE" Grid.Row="1" Grid.Column="3" Margin="3" TextWrapping="Wrap"/>

                <Label Content="HL" Grid.Row="2" Grid.Column="0" HorizontalAlignment="Right" Grid.RowSpan="3"/>
                <TextBox x:Name="txtRegisterHL" Grid.Row="2" Grid.Column="1" Margin="3" TextWrapping="Wrap"/>

                <Label Content="SP" Grid.Row="2" Grid.Column="2" HorizontalAlignment="Right" Grid.RowSpan="3"/>
                <TextBox x:Name="txtRegisterSP" Grid.Row="2" Grid.Column="3" Margin="3" TextWrapping="Wrap"/>

            </Grid>
        </GroupBox>

        <GroupBox Grid.Column="1" Header="Misc" Height="95" Margin="3,3,3,0" Grid.Row="3" VerticalAlignment="Top">
            <Grid>
                <Grid.RowDefinitions>
                    <RowDefinition Height="1*"/>
                    <RowDefinition Height="1*"/>
                    <RowDefinition Height="1*"/>
                </Grid.RowDefinitions>
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="1*"/>
                    <ColumnDefinition Width="1*"/>
                    <ColumnDefinition Width="1*"/>
                    <ColumnDefinition Width="1*"/>
                </Grid.ColumnDefinitions>
                <Label Content="IF" Grid.Row="0" Grid.Column="0" HorizontalAlignment="Right"/>
                <TextBox x:Name="txtIF" Grid.Row="0" Grid.Column="1" Margin="3" TextWrapping="Wrap"/>

                <Label Content="IE" Grid.Row="0" Grid.Column="2" HorizontalAlignment="Right" Grid.RowSpan="3"/>
                <TextBox x:Name="txtIE" Grid.Row="0" Grid.Column="3" Margin="3" TextWrapping="Wrap"/>
                
                <Label Content="IME" Grid.Row="1" Grid.Column="0" HorizontalAlignment="Right"/>
                <TextBox x:Name="txtIME" Grid.Row="1" Grid.Column="1" Margin="3" TextWrapping="Wrap"/>

                <Label Content="ROM" Grid.Row="1" Grid.Column="2" HorizontalAlignment="Right" Grid.RowSpan="3"/>
                <TextBox x:Name="txtRomBank" Grid.Row="1" Grid.Column="3" Margin="3" TextWrapping="Wrap"/>


            </Grid>
        </GroupBox>

    </Grid>
</Window>
