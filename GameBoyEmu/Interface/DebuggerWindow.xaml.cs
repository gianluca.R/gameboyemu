﻿using GameBoyEmu.Emu.Debugger;
using GameBoyEmu.Emu.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace GameBoyEmu.Interface
{
    /// <summary>
    /// Interaction logic for DebuggerWindow.xaml
    /// </summary>
    public partial class DebuggerWindow : Window, INotifyPropertyChanged
    {
        private GameBoy _gb;
        public GameBoy GB
        {
            get { return _gb; }
            set
            {
                _gb = value;
                if (value != null)
                {
                    _gb.OnPause += GB_OnPause;
                    ReloadASM(0);
                }
            }
        }

        private UInt16? _currentPC = null;
        public UInt16? CurrentPC
        {
            get { return _currentPC; }
            set
            {
                _currentPC = value;
                OnPropertyChanged("CurrentPC");
            }
        }
        private SortedList<UInt16, ParsedInstruction> parsedInstructionList = new SortedList<UInt16, ParsedInstruction>();

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public DebuggerWindow()
        {
            InitializeComponent();

            ReloadASM(0);
            SetRegistersEditable(false);
        }

        void ReloadASM(UInt16 line)
        {
            if (GB != null && Visibility == Visibility.Visible)
            {
                parsedInstructionList = CodeParser.ParseLines(GB.Ram, GB.Registers, line);
                listInstructions.ItemsSource = parsedInstructionList.Values;
            }
        }

        void GoToInstructionAt(UInt16 pos)
        {
            if (pos < 0 || pos > 0xFFFF) { return; }
            if (!parsedInstructionList.ContainsKey(pos))
            {
                ReloadASM(pos);
            }
            var currentLine = parsedInstructionList[pos];

            listInstructions.ItemsSource = null;
            listInstructions.ItemsSource = parsedInstructionList.Values;

            listInstructions.SelectedItem = currentLine;
            listInstructions.ScrollIntoView(currentLine);
        }

        private void GB_OnPause(object sender, EventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                SetRegistersEditable(true);
                UpdateTextFromRegisters();
                ReloadASM(GB.Registers.PC.Full);
                CurrentPC = GB.Registers.PC.Full;
                GoToInstructionAt(GB.Registers.PC.Full);
            });
        }

        #region Data binding
        // Which isn't really data binding, it's just too little to make it worthwhile right now, especially since we don't have a real model behind
        private void SetRegistersEditable(bool enabled)
        {
            txtRegisterAF.IsEnabled = enabled;
            txtRegisterBC.IsEnabled = enabled;
            txtRegisterDE.IsEnabled = enabled;
            txtRegisterHL.IsEnabled = enabled;
            txtRegisterSP.IsEnabled = enabled;
            txtRegisterPC.IsEnabled = enabled;
            txtIF.IsEnabled = enabled;
            txtIE.IsEnabled = enabled;
            txtIME.IsEnabled = enabled;
            txtRomBank.IsEnabled = false;
        }
        private void UpdateTextFromRegisters()
        {
            txtRegisterAF.Text = HexUtils.NumberToHex(GB.Registers.AF.Full, 4);
            txtRegisterBC.Text = HexUtils.NumberToHex(GB.Registers.BC.Full, 4);
            txtRegisterDE.Text = HexUtils.NumberToHex(GB.Registers.DE.Full, 4);
            txtRegisterHL.Text = HexUtils.NumberToHex(GB.Registers.HL.Full, 4);
            txtRegisterSP.Text = HexUtils.NumberToHex(GB.Registers.SP.Full, 4);
            txtRegisterPC.Text = HexUtils.NumberToHex(GB.Registers.PC.Full, 4);
            txtIF.Text = HexUtils.NumberToHex(GB.Ram.ReadByte(FixedAddresses.INTERRUPT_FLAG_REGISTER, true), 2);
            txtIE.Text = HexUtils.NumberToHex(GB.Ram.ReadByte(FixedAddresses.INTERRUPT_ENABLE_REGISTER, true), 2);
            txtIME.Text = GB.Registers.IME ? "1" : "0";
            txtRomBank.Text = GB.Ram.RomBank.ToString();
        }
        private void UpdateRegistersFromText()
        {
            // TODO: handle each one separately without making this uglier than it is
            try
            {
                GB.Registers.AF.Full = (UInt16)int.Parse(txtRegisterAF.Text, NumberStyles.HexNumber);
                GB.Registers.BC.Full = (UInt16)int.Parse(txtRegisterBC.Text, NumberStyles.HexNumber);
                GB.Registers.DE.Full = (UInt16)int.Parse(txtRegisterDE.Text, NumberStyles.HexNumber);
                GB.Registers.HL.Full = (UInt16)int.Parse(txtRegisterHL.Text, NumberStyles.HexNumber);
                GB.Registers.SP.Full = (UInt16)int.Parse(txtRegisterSP.Text, NumberStyles.HexNumber);
                GB.Registers.PC.Full = (UInt16)int.Parse(txtRegisterPC.Text, NumberStyles.HexNumber);
                GB.Ram.WriteByte(FixedAddresses.INTERRUPT_FLAG_REGISTER, (byte)int.Parse(txtIF.Text, NumberStyles.HexNumber), true);
                GB.Ram.WriteByte(FixedAddresses.INTERRUPT_ENABLE_REGISTER, (byte)int.Parse(txtIE.Text, NumberStyles.HexNumber), true);
                GB.Registers.IME = txtIME.Text == "1" ? true : false;
            }
            catch { }
        }
        #endregion

        #region Commands 

        private async void CommandRestart_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            await GB.Restart();
            if (GB.IsPaused)
            {
                GB_OnPause(null, null);
            }
        }

        private void CommandRestart_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = GB != null;
        }

        private void CommandPause_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            GB.Pause();
        }

        private void CommandPause_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = GB != null && !GB.IsPaused;
        }

        private void CommandResume_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            UpdateRegistersFromText();
            GB.Resume();
            CurrentPC = null;
            SetRegistersEditable(false);
        }

        private void CommandResume_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = GB != null && GB.IsPaused;
        }

        private void CommandStepInto_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            UpdateRegistersFromText();
            UInt16 startingPos = GB.Registers.PC.Full;
            while (GB.Registers.PC.Full == startingPos)
            {
                GB.LoopStep();
            }
            ReloadASM(GB.Registers.PC.Full);
            CurrentPC = GB.Registers.PC.Full;
            GoToInstructionAt(GB.Registers.PC.Full);
            GB.Gpu.RenderToScreen();
            UpdateTextFromRegisters();
        }

        private void CommandStepOver_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = GB != null && GB.IsPaused;
        }

        private void CommandStepOver_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            UpdateRegistersFromText();

            UInt16 startingPos = GB.Registers.PC.Full;
            while (GB.Registers.PC.Full == startingPos)
            {
                GB.LoopStep();
            }
            GoToInstructionAt(GB.Registers.PC.Full);
            GB.Gpu.RenderToScreen();
            UpdateTextFromRegisters();
        }

        private void CommandStepInto_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = GB != null && GB.IsPaused;
        }

        private void CommandToggleBreakpoint_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            UInt16 pos = ((ParsedInstruction)listInstructions.SelectedItem).RawAddress;
            if (GB.Breakpoints.Contains(pos))
            {
                GB.Breakpoints.Remove(pos);
            }
            else
            {
                GB.Breakpoints.Add(pos);
            }
            OnPropertyChanged("GB");
        }

        private void CommandToggleBreakpoint_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = GB != null && listInstructions.SelectedItem != null;
        }

        #endregion

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }
    }

    public static class DebuggerCommands
    {
        public static readonly RoutedUICommand Restart = new RoutedUICommand
        (
            "Restart",
            "Restart",
            typeof(DebuggerWindow),
            new InputGestureCollection()
            {
                new KeyGesture(Key.F1)
            }
        );
        public static readonly RoutedUICommand Pause = new RoutedUICommand
        (
            "Pause",
            "Pause",
            typeof(DebuggerWindow),
            new InputGestureCollection()
            {
                new KeyGesture(Key.F6)
            }
        );
        public static readonly RoutedUICommand Resume = new RoutedUICommand
        (
            "Resume",
            "Resume",
            typeof(DebuggerWindow),
            new InputGestureCollection()
            {
                new KeyGesture(Key.F7)
            }
        );
        public static readonly RoutedUICommand StepInto = new RoutedUICommand
        (
            "Step Into",
            "Step Into",
            typeof(DebuggerWindow),
            new InputGestureCollection()
            {
                new KeyGesture(Key.F8)
            }
        );
        public static readonly RoutedUICommand StepOver = new RoutedUICommand
        (
            "Step Over",
            "Step Over",
            typeof(DebuggerWindow),
            new InputGestureCollection()
            {
                new KeyGesture(Key.F9)
            }
        );
        public static readonly RoutedUICommand ToggleBreakpoint = new RoutedUICommand
        (
            "Toggle Breakpoint",
            "Toggle Breakpoint",
            typeof(DebuggerWindow),
            new InputGestureCollection()
            {
                new KeyGesture(Key.F4)
            }
        );
    }

    public class MultiValueEqualityConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return values?.All(o => o?.Equals(values[0]) == true) == true || values?.All(o => o == null) == true;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class ElementContainedInListConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length != 2)
            {
                throw new ArgumentException("ElementContainedInListConverter needs exactly 2 inputs: a list and a value");
            }
            IList list;
            Object value;
            if (values[0] is IList)
            {
                list = (IList)values[0];
                value = values[1];
            }
            else
            {
                list = (IList)values[1];
                value = values[0];
            }

            return list.Contains(value);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
