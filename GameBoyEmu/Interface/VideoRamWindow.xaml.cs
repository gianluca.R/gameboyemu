﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace GameBoyEmu.Interface
{
    /// <summary>
    /// Interaction logic for VideoRamWindow.xaml
    /// </summary>
    public partial class VideoRamWindow : Window
    {
        public GameBoy GB { get; set; }

        private int bgMap = 0;
        private int tileDataId = 0;
        private int tileMapBank = 0;
        private int tileMapPalette = 0;

        public VideoRamWindow()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            this.Visibility = Visibility.Hidden;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DispatcherTimer refreshTimer = new DispatcherTimer();
            refreshTimer.Interval = new TimeSpan(0, 0, 0, 0, 50);
            refreshTimer.Tick += RefreshTimer_Tick;
            refreshTimer.Start();
        }

        private void RefreshTimer_Tick(object sender, EventArgs e)
        {
            if (Visibility == Visibility.Visible && GB != null)
            {
                imgTileMap.Source = GB.Gpu.GetTileMapImage(tileMapBank, tileMapPalette);
                imgBG.Source = GB.Gpu.GetBGImage(bgMap, tileDataId);
            }
        }

        private void radioBGMap0_Checked(object sender, RoutedEventArgs e)
        {
            bgMap = 0;
        }

        private void radioBGMap1_Checked(object sender, RoutedEventArgs e)
        {
            bgMap = 1;
        }

        private void radioTileMapBank0_Checked(object sender, RoutedEventArgs e)
        {
            tileMapBank = 0;
        }

        private void radioTileMapBank1_Checked(object sender, RoutedEventArgs e)
        {
            tileMapBank = 1;
        }

        private void txtTilePalette_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            int.TryParse(txtTilePalette.Text, out tileMapPalette);
            if (tileMapPalette > 13) { tileMapPalette = 13; }
            if (tileMapPalette < 0) { tileMapPalette = 0; }
        }

        private void radioTileData0_Checked(object sender, RoutedEventArgs e)
        {
            tileDataId = 0;
        }

        private void radioTileData1_Checked(object sender, RoutedEventArgs e)
        {
            tileDataId = 1;
        }
    }
}
