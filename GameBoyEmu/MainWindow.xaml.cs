﻿using GameBoyEmu.Emu.Enums;
using GameBoyEmu.Emu.VirtualComponents;
using GameBoyEmu.Emu.VirtualComponents.Auxiliary;
using GameBoyEmu.Interface;
using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace GameBoyEmu
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        GameBoy gb;
        DispatcherTimer clk;
        DebuggerWindow windowDebugger = new DebuggerWindow();
        VideoRamWindow windowVideoRam = new VideoRamWindow();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.UseLayoutRounding = false;
            RenderOptions.SetBitmapScalingMode(imgRender, BitmapScalingMode.NearestNeighbor);
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            JoyButton? button;
            if (getButton(e.Key, out button))
            {
                gb.SetButtonState(button.Value, true);
            }
        }

        private void Window_KeyUp(object sender, KeyEventArgs e)
        {
            JoyButton? button;
            if (getButton(e.Key, out button))
            {
                gb.SetButtonState(button.Value, false);
            }
        }

        bool getButton(Key key, out JoyButton? button)
        {
            button = null;
            switch (key)
            {
                case Key.W:
                    button = JoyButton.UP;
                    return true;

                case Key.A:
                    button = JoyButton.LEFT;
                    return true;

                case Key.S:
                    button = JoyButton.DOWN;
                    return true;

                case Key.D:
                    button = JoyButton.RIGHT;
                    return true;

                case Key.L:
                    button = JoyButton.A;
                    return true;

                case Key.K:
                    button = JoyButton.B;
                    return true;

                case Key.Enter:
                    button = JoyButton.START;
                    return true;

                case Key.Space:
                    button = JoyButton.SELECT;
                    return true;
            }
            return false;
        }

        private void MenuItemOpenROM_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "All supported ROMs (*.gb,*.gbc)|*.gb;*.gbc|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                StartEmulator(openFileDialog.FileName);
            }
        }

        private void StartEmulator(string romFile)
        {
            if (gb != null)
            {
                gb.Terminate();
            }
            if (clk != null)
            {
                clk.Stop();
            }

            //Start emu here for now
            Cartridge cartridge = new Cartridge(romFile);
            gb = new GameBoy((screen, tileMap) =>
            {
                imgRender.Source = screen;
            });
            gb.LoadCartridge(cartridge);

            clk = new DispatcherTimer(new TimeSpan(0, 0, 0, 0, 100), DispatcherPriority.Normal,
                (a, b) =>
                {
                    this.Title = gb.FPS + " fps, " + Math.Round(gb.CPS / 1000.0, 4) + " Kcps";
                },
                this.Dispatcher);
        }

        private void MenuItemDebugger_Click(object sender, RoutedEventArgs e)
        {
            windowDebugger.Show();
            windowDebugger.GB = gb;
        }
        private void MenuItemVideoRam_Click(object sender, RoutedEventArgs e)
        {
            windowVideoRam.Show();
            windowVideoRam.GB = gb;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (gb != null)
            {
                gb.Terminate();
            }
            windowDebugger.Close();
            windowVideoRam.Close();
            Application.Current.Shutdown();
        }
    }
}
