A work in progress Game Boy emulator written in C#.


Resources used:  
http://bgb.bircd.org/pandocs.htm - Good resource with GameBoy architecture and specs  
http://marc.rawer.de/Gameboy/Docs/GBCPUman.pdf - CPU instruction manual  
https://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html - OpCode map, especially useful for the extended instruction set